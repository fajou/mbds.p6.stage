Automatisation des livraison avec GPS tracker des voitures et Mode de paiement rapproche.

Ce projet consiste a developper un systeme de livraison des produits clientele 
avec GPS tracker et automatisation des livraisons. La livraison est assiste par un systeme de payement mobile.

Conception de l'application
    - IHM
    - UML
    - BDD
   
Vente en ligne
    - Liste des produits avec filtre par categories
    - Gestion de pannier du client
    - Processuce de livraison et mode de paiments
    - Generation et envoie code de livraison (Si mode de confiramation par code).
    - Notification des livraison en retard ou non recu.
Systeme de livraison avec Geolocalisation
    - Groupement des produits en fonction destination, poids, voiture pour creer une livraison livraison.
    - Trackeur de la geolocalisation des voitures de livraison.
    - Prediction date de livraison et date de retour des livreure (En temps reel).
        * Envoie des email et notifications de l'arrive de livraison au client.
    - Collection des donnees itinerraie des livreurs en temps reel.
    - Prediction possible des prochaine livraison en temps reel :
        * En fonction preparation des package et date de retoure des livreure et des voiture disponible.
    - Calcule de l'intervalle des temps fais pour un livraison.
    - Suivi et rapport de conduits des voitures de livraison avec notification par email. 
    - Groupements des produits pour le prochain livraison :
        * En fonctions des produits et des voitures disponible.
    - Confirmation des livraison recu.
    - Statistiques
        - Livraison, Voiture et Chauffeure
        - Statistique de payements mobile
        - Prediction et suggestion des bonnes dates de livraison.       
    - Configuration marge des livraisons.
            - Vitesse max,min voiture
            - Poids max,min livrable des produits
            - Interval de temps d'une livraison a une autre.
Application mobile:
    - Liste des clients a livrer.
        - Trie par rapport a la destination.
        - Details et liste des produits a livrer.
    - Confirmation des produits livree par NFC (Communication a champ raproche) 
        * Cas des achats deja payees
    - Possible de confirmer par code en cas d'abscence de NFC d'un client 
        * Le client recoie un code de confirmation de livraison.
        * Saisie du code client.
    - Confirmation avec paiement des produits livree - (NFC avec mode de payement: Mvola, Airtel Money, Orange Money)
Phase de teste
    - Teste des fonctionnalites
    - Simulation des mode de payements
    - Simulation d'une voitures tracker par GPS.