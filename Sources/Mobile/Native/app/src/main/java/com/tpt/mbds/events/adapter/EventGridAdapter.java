package com.tpt.mbds.events.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.adapter.holder.EventGridHolder;
import com.tpt.mbds.events.databinding.CardGridEventBinding;
import com.tpt.mbds.events.models.Event;

import java.util.List;

public class EventGridAdapter extends RecyclerView.Adapter<EventGridHolder> {
    private List<Event> events;

    public EventGridAdapter(List<Event> providers) {
        this.events = providers;
    }

    @NonNull
    @Override
    public EventGridHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater       inflater     = LayoutInflater.from(parent.getContext());
        CardGridEventBinding eventBinding = DataBindingUtil.inflate(inflater, R.layout.card_grid_event, parent, false);
        return new EventGridHolder(eventBinding, eventBinding.eventGridCard);
    }

    @Override
    public void onBindViewHolder(EventGridHolder eventHolder, int position) {
        Event event = events.get(position);
        eventHolder.getEventBinding().setEvent(event);
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}