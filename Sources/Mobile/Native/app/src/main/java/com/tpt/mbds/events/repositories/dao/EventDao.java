package com.tpt.mbds.events.repositories.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tpt.mbds.events.models.Event;

import java.util.List;

@Dao
public interface EventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Event... events);

    @Query("SELECT * FROM event")
    LiveData<List<Event>> getAll();

    @Query("SELECT * FROM event where id =:eventId")
    LiveData<Event> getById(long eventId);

    @Query("SELECT * FROM event WHERE id IN(:ids)")
    LiveData<List<Event>> findByIds(Long[] ids);
}
