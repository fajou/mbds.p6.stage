package com.tpt.mbds.events.repositories.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.tpt.mbds.events.models.Billet;

import java.util.List;

@Dao
public interface BilletDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAll(Billet... billets);

    @Query("SELECT * FROM billet")
    LiveData<List<Billet>> getAll();

    @Query("SELECT * FROM billet where qrCode=:code")
    LiveData<Billet> getByCode(String code);
}
