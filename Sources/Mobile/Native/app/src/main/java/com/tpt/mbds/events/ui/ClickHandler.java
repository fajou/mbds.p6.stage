package com.tpt.mbds.events.ui;

import android.content.Intent;
import android.view.View;
import com.tpt.mbds.events.InvitationActivity;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.models.Event;

public class ClickHandler {

    public void onClickInvite(View view, Event event) {
        Intent intent = new Intent(view.getContext(), InvitationActivity.class);
        intent.putExtra(Utils.ID, event.getId());
        view.getContext().startActivity(intent);
    }
}
