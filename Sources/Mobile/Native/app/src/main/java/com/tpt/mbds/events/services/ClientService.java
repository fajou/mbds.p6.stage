package com.tpt.mbds.events.services;


import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.OperateurCode;

import java.util.ArrayList;
import java.util.List;

public class ClientService {
    public static List<Billet> billets = new ArrayList<>();

    public OperateurCode modifierMontant(OperateurCode o, double montant) {
        return o;
    }

    public List<Billet> listeBillet(OperateurCode operateurCode) {
        return billets;
    }

    public void ajouterBillet(Billet billet, OperateurCode operateurCode) {
        billets.add(billet);
    }

    public OperateurCode getClient() {
        return new OperateurCode();
    }
}
