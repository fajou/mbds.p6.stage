package com.tpt.mbds.events.ui.compte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.*;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.tpt.mbds.events.MainActivity;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.constants.PathApi;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.databinding.ActivityLoginBinding;
import com.tpt.mbds.events.models.FbLogin;
import com.tpt.mbds.events.models.UserLogin;
import com.tpt.mbds.events.services.GsonRequest;
import com.tpt.mbds.events.services.responses.LoginResponse;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.tpt.mbds.events.constants.Utils.GSON;
import static com.tpt.mbds.events.constants.Utils.USER_CONNECTED;

public class LoginActivity extends AppCompatActivity {
    private static final ExecutorService     EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    private              UserCompteViewModel viewModel;
    private              SharedPreferences   shared;
    private              CallbackManager     callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setUser(new UserLogin("valeryharisoanitolotra@gmail.com", "123456"));
        viewModel = new ViewModelProvider(this).get(UserCompteViewModel.class);
        callbackManager = CallbackManager.Factory.create();
        /*For custom button : LoginManager.getInstance().logInWithReadPermissions(this, Collections.singletonList("public_profile"));*/
        initButtonFbLogin();
    }

    private void progres(boolean show) {
        ProgressBar bar = findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void initButtonFbLogin() {
        LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setPermissions("public_profile");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                fbRegister(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });
    }

    private void fbRegister(AccessToken accessToken) {
        progres(true);
        EXECUTOR_SERVICE.execute(() -> {
            GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
                FbLogin login = FbLogin.getFbLogin(object.toString());
                login.setUserToken(accessToken.getToken());
                System.out.println(GSON.toJson(login));
                RequestQueue queue = Volley.newRequestQueue(this);
                GsonRequest<LoginResponse> gsonRequest = new GsonRequest<>(Request.Method.POST, PathApi.FB_LOGIN, null, GSON.toJson(login), LoginResponse.class,
                        this::onResponse, Throwable::printStackTrace);
                gsonRequest.setContentType("application/json");
                queue.add(gsonRequest);
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,birthday,email,gender,first_name,last_name");
            request.setParameters(parameters);
            request.executeAsync();
        });
    }

    public void goToHome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void onClickLogin(View view) {
        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);

        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
            progres(true);
            String          userName = username.getText().toString();
            String          pasword  = password.getText().toString();
            final UserLogin login    = new UserLogin(userName, pasword);
            EXECUTOR_SERVICE.execute(() -> {
                RequestQueue queue = Volley.newRequestQueue(this);
                GsonRequest<LoginResponse> request = new GsonRequest<>(Request.Method.POST, PathApi.LOGIN, null, GSON.toJson(login), LoginResponse.class,
                        this::onResponse, Throwable::printStackTrace);
                request.setContentType("application/json");
                queue.add(request);
            });
        } else {
            Toast.makeText(this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onResponse(LoginResponse response) {
        if (!response.isError()) {
            shared = getSharedPreferences(Utils.SHARED_SESSION, MODE_PRIVATE);
            shared.edit().putBoolean(USER_CONNECTED, true).apply();
            viewModel.insert(response.getData());
            progres(false);
            goToHome();
        } else Toast.makeText(this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
    }
}
