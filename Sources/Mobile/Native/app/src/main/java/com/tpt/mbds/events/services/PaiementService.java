package com.tpt.mbds.events.services;

import com.tpt.mbds.events.constants.Operateur;
import com.tpt.mbds.events.models.OperateurCode;
import com.tpt.mbds.events.models.QrCodePaimentInfo;

public class PaiementService {
    public static OperateurCode CODE = new OperateurCode(1, "*111*1*4*1*%s*%s#", "*111*1*4*1*%s*%s#", "*111*1*4*1*%s*%s#");

    public static String codeGsm(QrCodePaimentInfo paimentInfo, Operateur opt) {
        if (opt == Operateur.TELMA)
            return String.format(CODE.getCodeTelma(), paimentInfo.getTelma(), paimentInfo.getPrixTotal());
        if (opt == Operateur.ORANGE)
            return String.format(CODE.getCodeOrange(), paimentInfo.getOrange(), paimentInfo.getPrixTotal());
        if (opt == Operateur.AIRTEL)
            return String.format(CODE.getCodeAirtel(), paimentInfo.getAirtel(), paimentInfo.getPrixTotal());
        return "";
    }
}
