package com.tpt.mbds.events.models;

public class QrCodeBilletInfo {
    private long   eventId;
    private long   userId;
    private long   myEventId;
    private double prixtotal;
    private String code;

    public long getEventId() {
        return eventId;
    }

    public long getUserId() {
        return userId;
    }

    public long getMyEventId() {
        return myEventId;
    }

    public double getPrixtotal() {
        return prixtotal;
    }

    public String getCode() {
        return code;
    }
}
