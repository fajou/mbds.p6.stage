package com.tpt.mbds.events.services.responses;

import com.tpt.mbds.events.models.UserCompte;

public class LoginResponse {
    private boolean    error;
    private UserCompte data;
    private String     message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public UserCompte getData() {
        return data;
    }

    public void setData(UserCompte data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }
}
