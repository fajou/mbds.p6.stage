package com.tpt.mbds.events.services;

import com.android.volley.*;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;
import com.tpt.mbds.events.constants.Url;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static com.tpt.mbds.events.constants.Utils.GSON;

public class GsonRequest<T> extends Request<T> {
    private final Map<String, String>  headers;
    private final String               body;
    private       String               contentType;
    private       Class<T>             clazz;
    private final Response.Listener<T> listener;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param path    URL of the request to make
     * @param headers Map of request headers
     * @param body    Map of body
     */
    public GsonRequest(int method, String path, Map<String, String> headers, String body, Class<T> clazz, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, Url.path(path), errorListener);
        this.headers = headers;
        this.listener = listener;
        this.body = body;
        this.clazz = clazz;
        System.out.println(getUrl());
        this.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 30000;
            }

            @Override
            public void retry(VolleyError error) {

            }
        });
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (body != null && !body.equals(""))
            return body.getBytes(StandardCharsets.UTF_8);
        return super.getBody();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return contentType == null ? super.getBodyContentType() : contentType;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            System.out.println(json);
            return Response.success(GSON.fromJson(json, clazz), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
