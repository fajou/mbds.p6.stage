package com.tpt.mbds.events.repositories;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.tpt.mbds.events.models.UserCompte;
import com.tpt.mbds.events.repositories.dao.UserCompteDao;

public class UserRepository {
    private UserCompteDao        compteDao;
    private LiveData<UserCompte> userCompte;
    private AppDatabase          _db;

    public UserRepository(Application application) {
        _db = AppDatabase.getDatabase(application);
    }

    public void initUserCompteDao() {
        compteDao = _db.userCompteDao();
        userCompte = compteDao.get();
    }

    public LiveData<UserCompte> getUserCompte() {
        return userCompte;
    }

    public void inserUserCompte(final UserCompte compte) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            compteDao.save(compte);
        });
    }
}