package com.tpt.mbds.events.repositories.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import com.tpt.mbds.events.models.UserCompte;

@Dao
public interface UserCompteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(UserCompte compte);

    @Delete
    void delete(UserCompte compte);

    @Query("select * from user_compte limit 1")
    LiveData<UserCompte> get();
}
