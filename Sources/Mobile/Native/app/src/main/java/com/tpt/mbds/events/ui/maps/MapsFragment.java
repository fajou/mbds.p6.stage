package com.tpt.mbds.events.ui.maps;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tpt.mbds.events.MainActivity;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.models.LocalPosition;
import com.tpt.mbds.events.services.GsonRequest;
import com.tpt.mbds.events.services.responses.EventResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import static com.tpt.mbds.events.constants.PathApi.EVENT_APROX;
import static com.tpt.mbds.events.constants.Utils.GSON;

public class MapsFragment extends Fragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, LocationListener {
    public static        List<Event>     EVENTS_APROX;
    private static final int             REQUEST_LOCATION = 1001;
    private              LocationManager locationManager;
    private              GoogleMap       mMap;
    private              MarkerOptions   CURRENT_LOCATION;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkPermision();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @BindingAdapter({"mapLatLng"})
    public static void setMapLatLng(FragmentContainerView c, String latLng) {
    }

    private void checkPermision() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
    }

    private void setCurrentMarker() {
        locationManager = (LocationManager) requireContext().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria     = new Criteria();
        String   bestProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider == null)
            return;

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        Location location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            double lat    = location.getLatitude();
            double lng    = location.getLongitude();
            LatLng latLng = new LatLng(lat, lng);
            CURRENT_LOCATION = new MarkerOptions().position(latLng).title("Moi");
            mMap.addMarker(CURRENT_LOCATION).showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            onLocationChanged(location);
        }
    }

    private void setEventsAprox() {
        for (Event event : EVENTS_APROX) {
            LatLng position = GSON.fromJson(event.getLocalisation(), LocalPosition.class).getLatLng();
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .visible(true)
                    .title(event.getTitle()));
            marker.showInfoWindow();
            marker.setTag(event.getId());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        EVENTS_APROX = EVENTS_APROX != null ? EVENTS_APROX : new ArrayList<>();
        mMap = googleMap;
        setEventsAprox();
        mMap.setOnMarkerClickListener(this);
        setCurrentMarker();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Toast.makeText(requireContext(), marker.getTitle(), Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        double latitude  = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng    = new LatLng(latitude, longitude);
        CURRENT_LOCATION.position(latLng);
        reitriveData(new LocalPosition(latitude, longitude));
    }

    public void reitriveData(LocalPosition position) {
        Executors.newSingleThreadExecutor().execute(() -> {
            RequestQueue        queue   = Volley.newRequestQueue(requireContext());
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Utils.bearer(MainActivity.ACCESS_TOKEN));
            GsonRequest<EventResponse> request = new GsonRequest<>(Request.Method.POST, EVENT_APROX, headers, GSON.toJson(position), EventResponse.class,
                    response -> {
                        if (!response.isError()) {
                            MapsFragment.EVENTS_APROX = response.getData();
                            setEventsAprox();
                        }
                    }, Throwable::printStackTrace);
            queue.add(request);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear();
            setCurrentMarker();
        }
    }
}
