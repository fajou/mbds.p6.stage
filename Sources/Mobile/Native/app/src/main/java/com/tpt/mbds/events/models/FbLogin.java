package com.tpt.mbds.events.models;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

public class FbLogin {
    @SerializedName(value = "last_name")
    private       String  lastName;
    @SerializedName(value = "first_name")
    private       String  firstname;
    private       String  nom;
    private       String  prenom;
    private       String  email;
    private       int     jour;
    private       int     mois;
    private       int     annee;
    @SerializedName("gender")
    private       String  sexe;
    private       String  userToken;
    private final boolean isFBConnect = true;
    private       Date    birthday;

    public static FbLogin getFbLogin(String fbInfo) {
        FbLogin  login    = new GsonBuilder().setDateFormat("MM/dd/yyyy").create().fromJson(fbInfo, FbLogin.class);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(login.birthday) ;
        login.jour = calendar.get(Calendar.DAY_OF_MONTH);
        login.mois = calendar.get(Calendar.MONTH);
        login.annee = calendar.get(Calendar.YEAR);
        login.nom = login.lastName;
        login.prenom = login.firstname;
        return login;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
