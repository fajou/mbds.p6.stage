package com.tpt.mbds.events.adapter.holder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.databinding.CardGridEventBinding;

public class EventGridHolder extends RecyclerView.ViewHolder {
    private CardGridEventBinding eventBinding;

    public EventGridHolder(CardGridEventBinding eventBinding, @NonNull View itemView) {
        super(itemView);
        this.eventBinding = eventBinding;
    }

    public CardGridEventBinding getEventBinding() {
        return eventBinding;
    }
}