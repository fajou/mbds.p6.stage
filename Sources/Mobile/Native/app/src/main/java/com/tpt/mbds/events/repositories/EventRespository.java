package com.tpt.mbds.events.repositories;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.repositories.dao.EventDao;

import java.util.List;

public class EventRespository {
    private EventDao        eventDao;
    private AppDatabase     _db;
    private LiveData<List<Event>> events;

    public EventRespository(Application application) {
        _db = AppDatabase.getDatabase(application);
        eventDao = _db.eventDao();
        events = eventDao.getAll();
    }

    public void inserts(Event... events) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            eventDao.insertAll(events);
        });
    }

    public LiveData<List<Event>> getEvents() {
        return events;
    }

    public LiveData<Event> getEvent(long eventId) {
        return eventDao.getById(eventId);
    }

    public LiveData<List<Event>> getEventByIds(Long[] ids) {
        return eventDao.findByIds(ids);
    }
}
