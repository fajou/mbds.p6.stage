package com.tpt.mbds.events.services.responses;

import java.util.List;

public class MyEventResponse {
    private boolean    error;
    private List<Long> data;
    private String     message;

    public boolean isError() {
        return error;
    }

    public List<Long> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
