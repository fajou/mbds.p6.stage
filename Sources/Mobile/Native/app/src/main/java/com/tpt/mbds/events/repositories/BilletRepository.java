package com.tpt.mbds.events.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.repositories.dao.BilletDao;

import java.util.List;

public class BilletRepository {
    private BilletDao              billetDao;
    private AppDatabase            _db;
    private LiveData<List<Billet>> billets;

    public BilletRepository(Application application) {
        _db = AppDatabase.getDatabase(application);
        billetDao = _db.billetDao();
        billets = billetDao.getAll();
    }

    public void inserts(Billet... billets) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            billetDao.insertAll(billets);
        });
    }

    public LiveData<List<Billet>> getBillets() {
        return billets;
    }

    public LiveData<Billet> get(String code) {
        return billetDao.getByCode(code);
    }
}
