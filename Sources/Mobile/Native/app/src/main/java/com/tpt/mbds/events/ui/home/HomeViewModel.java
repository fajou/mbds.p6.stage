package com.tpt.mbds.events.ui.home;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.repositories.BilletRepository;
import com.tpt.mbds.events.repositories.EventRespository;

import java.util.List;

public class HomeViewModel extends AndroidViewModel {
    private EventRespository      eventRespository;
    private BilletRepository      billetRepository;
    private LiveData<List<Event>> events;

    public HomeViewModel(Application application) {
        super(application);
        eventRespository = new EventRespository(application);
        billetRepository = new BilletRepository(application);
        events = eventRespository.getEvents();
    }

    public LiveData<List<Event>> getEvents() {
        return events;
    }

    public void inserts(Event... events) {
        eventRespository.inserts(events);
    }

    public void inserts(Billet... billets) {
        billetRepository.inserts(billets);
    }

    public LiveData<Event> getEvent(long eventId) {
        return eventRespository.getEvent(eventId);
    }

    public LiveData<Billet> getBillet(String code) {
        return billetRepository.get(code);
    }

    public LiveData<List<Event>> getEventByIds(Long... ids) {
        return eventRespository.getEventByIds(ids);
    }
}