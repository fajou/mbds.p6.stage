package com.tpt.mbds.events.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.adapter.CustomDataBindingAdapter;
import com.tpt.mbds.events.adapter.EventAdapter;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    private HomeViewModel       homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        createListeEvenement(root);
        homeViewModel.getEvents().observe(getViewLifecycleOwner(), events -> CustomDataBindingAdapter.createList(getContext(), root, new EventAdapter(events)));
        return root;
    }

    private void createListeEvenement(View view) {
        CustomDataBindingAdapter.createList(getContext(), view, new EventAdapter(new ArrayList<>()));
    }
}
