package com.tpt.mbds.events.adapter.holder;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.databinding.CardEventBinding;

public class EventHolder extends RecyclerView.ViewHolder {
    private CardEventBinding eventBinding;

    public EventHolder(CardEventBinding binding, View view) {
        super(view);
        eventBinding = binding;
    }

    public CardEventBinding getEventBinding() {
        return eventBinding;
    }
}