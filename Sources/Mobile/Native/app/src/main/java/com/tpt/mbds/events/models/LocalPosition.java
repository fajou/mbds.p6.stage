package com.tpt.mbds.events.models;

import com.google.android.gms.maps.model.LatLng;

import static com.tpt.mbds.events.constants.Utils.GSON;

public class LocalPosition {
    private double lat;
    private double lng;

    public LocalPosition(double latitude, double longitude) {
        lat = latitude;
        lng = longitude;
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public static LocalPosition toLocalPosition(String position) {
        if (position == null)
            return null;
        return GSON.fromJson(position, LocalPosition.class);
    }
}
