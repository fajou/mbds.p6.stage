package com.tpt.mbds.events.adapter.holder;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.databinding.CardInvitationBinding;

public class InvitationHolder extends RecyclerView.ViewHolder {
    CardInvitationBinding invitationBinding;

    public InvitationHolder(CardInvitationBinding invitationBinding, @NonNull View itemView) {
        super(itemView);
        this.invitationBinding = invitationBinding;
    }

    public CardInvitationBinding getInvitationBinding() {
        return invitationBinding;
    }
}
