package com.tpt.mbds.events.repositories;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.models.OperateurCode;
import com.tpt.mbds.events.models.UserCompte;
import com.tpt.mbds.events.repositories.dao.BilletDao;
import com.tpt.mbds.events.repositories.dao.EventDao;
import com.tpt.mbds.events.repositories.dao.PaimentDao;
import com.tpt.mbds.events.repositories.dao.UserCompteDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {OperateurCode.class, UserCompte.class, Event.class, Billet.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PaimentDao paimentDao();

    public abstract UserCompteDao userCompteDao();

    public abstract EventDao eventDao();

    public abstract BilletDao billetDao();

    private static volatile AppDatabase     INSTANCE;
    private static final    int             NUMBER_OF_THREADS     = 4;
    static final            ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "pub_event_db")
                            .addCallback(databaseInitCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static RoomDatabase.Callback databaseInitCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            databaseWriteExecutor.execute(() -> {
                OperateurCode code = new OperateurCode(1, "*111*1*4*1*%s*%s#", "*111*1*4*1*%s*%s#", "*111*1*4*1*%s*%s#");
                INSTANCE.paimentDao().insert(code);
            });
        }
    };
}