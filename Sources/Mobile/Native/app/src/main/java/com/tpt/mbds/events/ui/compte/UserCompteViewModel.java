package com.tpt.mbds.events.ui.compte;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.tpt.mbds.events.models.UserCompte;
import com.tpt.mbds.events.repositories.UserRepository;

public class UserCompteViewModel extends AndroidViewModel {
    private UserRepository       repositories;
    private LiveData<UserCompte> compte;

    public UserCompteViewModel(@NonNull Application application) {
        super(application);
        repositories = new UserRepository(application);
        repositories.initUserCompteDao();
        compte = repositories.getUserCompte();
    }

    public LiveData<UserCompte> getCompte() {
        return compte;
    }

    public void insert(UserCompte compte) {
        repositories.inserUserCompte(compte);
    }
}
