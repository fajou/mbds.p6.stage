package com.tpt.mbds.events.constants;

public class Url {
    private static final String BASE_URL     = "https://mbds-p6-tpt-events.herokuapp.com/api";
    private static final String BASE_URL_GAE = "https://pub-events.df.r.appspot.com/api";

    public static String path(String path) {
        return BASE_URL_GAE + path;
    }
}
