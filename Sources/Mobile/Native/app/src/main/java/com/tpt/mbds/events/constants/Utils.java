package com.tpt.mbds.events.constants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Utils {
    public static final String SHARED_SESSION         = "com.tpt.mbds.events.session";
    public static final Gson   GSON                   = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
    public static final String USER_CONNECTED         = "IS_USER_CONNECTE";
    public static final String HIDE_SCANNER_HOWTO     = "HIDE_SCANNER_HOWTO";
    public static final String ID                     = "ID_";
    public static final String QR_CODE_EXTRA          = "com.tpt.mbds.events.QR_CODE";
    public static final String CALL_USSD_FOR_PAIMENT  = "com.tpt.mbds.events.CALL_USSD";
    public static final int    REQUEST_CODE           = 1;
    public static final int    REQUEST_QR_CODE_PAIE   = 1425;
    public static final int    REQUEST_QR_CODE_TICKET = 1426;
    public static final int    REQUEST_FOR_CALL       = 1525;
    public static final String EXTRA_REPLY            = "com.tpt.mbds.events.REPLY";
    public static final String OPT_MOBILE             = "mobile";

    public static String bearer(String token) {
        String bearer = String.format("%s %s", "Bearer", token);
        System.out.println(bearer);
        return bearer;
    }

    public static boolean hasValue(Object o) {
        return o != null;
    }
}
