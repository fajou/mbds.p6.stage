package com.tpt.mbds.events.repositories;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.tpt.mbds.events.models.OperateurCode;
import com.tpt.mbds.events.repositories.dao.PaimentDao;

public class PaimentRepository {
    private PaimentDao              paimentDao;
    private AppDatabase             _db;
    private LiveData<OperateurCode> code;

    public PaimentRepository(Application application) {
        _db = AppDatabase.getDatabase(application);
        paimentDao = _db.paimentDao();
        code = paimentDao.get();
    }

    public void insert(final OperateurCode operateurCode) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            paimentDao.insert(operateurCode);
        });
    }

    public LiveData<OperateurCode> getCode() {
        return code;
    }
}
