package com.tpt.mbds.events.models;

public class MediaCloud {
    private String secure_url;
    private String public_id;
    private int    height;
    private int    width;

    public String getSecure_url() {
        return secure_url;
    }

    public String getPublic_id() {
        return public_id;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setSecure_url(String secure_url) {
        this.secure_url = secure_url;
    }
}
