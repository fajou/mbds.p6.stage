package com.tpt.mbds.events;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tpt.mbds.events.adapter.CustomDataBindingAdapter;
import com.tpt.mbds.events.adapter.InvitationAdapter;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.databinding.ContentInvitationBinding;
import com.tpt.mbds.events.ui.home.HomeViewModel;

import java.util.ArrayList;

import static com.tpt.mbds.events.constants.Utils.EXTRA_REPLY;
import static com.tpt.mbds.events.constants.Utils.REQUEST_CODE;

public class InvitationActivity extends AppCompatActivity {
    private             HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            /*Intent intent = new Intent(InvitationActivity.this, InviteUserActivity.class);
            startActivityForResult(intent, REQUEST_CODE);*/
        });

        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        InvitationAdapter adapter = new InvitationAdapter(new ArrayList<>());
        CustomDataBindingAdapter.createList(getBaseContext(), findViewById(R.id.event_invitation), adapter);
        /*homeViewModel.getUsers().observe(this, adapter::setUsers);*/

        ContentInvitationBinding content = ContentInvitationBinding.bind(findViewById(R.id.content_invitation_layout));
        homeViewModel.getEvent(getIntent().getLongExtra(Utils.ID, 1)).observe(this, content::setEvent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            String result = data.getStringExtra(EXTRA_REPLY);
            /*homeViewModel.insert(new Gson().fromJson(result, OperateurCode.class));*/
        } else {
            Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
        }
    }
}
