package com.tpt.mbds.events.services.responses;

import com.tpt.mbds.events.models.Billet;

import java.util.List;

public class BilletResposne {
    private boolean      error;
    private List<Billet> data;
    private String       message;

    public boolean isError() {
        return error;
    }

    public List<Billet> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
