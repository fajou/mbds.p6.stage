package com.tpt.mbds.events.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import com.tpt.mbds.events.converter.JsonConverters;

@Entity(tableName = "user_compte")
@TypeConverters(value = JsonConverters.class)
public class UserCompte {
    @PrimaryKey
    private long       id;
    private long       version;
    private String     userName;
    private String     lastLocalisation;
    private String     facebookToken;
    private String     mobilesTokenId;
    private String     codePays;
    private long       points;
    private long       argents;
    private UserInfo   userInfo;
    private MediaCloud mediaProfile;

    public String getUserName() {
        return userName;
    }

    public String getLastLocalisation() {
        return lastLocalisation;
    }

    public String getFacebookToken() {
        return facebookToken;
    }

    public String getMobilesTokenId() {
        return mobilesTokenId;
    }

    public String getCodePays() {
        return codePays;
    }

    public long getPoints() {
        return points;
    }

    public long getArgents() {
        return argents;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setLastLocalisation(String lastLocalisation) {
        this.lastLocalisation = lastLocalisation;
    }

    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    public void setMobilesTokenId(String mobilesTokenId) {
        this.mobilesTokenId = mobilesTokenId;
    }

    public void setCodePays(String codePays) {
        this.codePays = codePays;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public void setArgents(long argents) {
        this.argents = argents;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public MediaCloud getMediaProfile() {
        return mediaProfile;
    }

    public void setMediaProfile(MediaCloud mediaProfile) {
        this.mediaProfile = mediaProfile;
    }

}
