package com.tpt.mbds.events.services.responses;

import com.tpt.mbds.events.models.Event;

import java.util.List;

public class EventResponse {
    private boolean     error;
    private List<Event> data;
    private String      message;

    public boolean isError() {
        return error;
    }

    public List<Event> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
