package com.tpt.mbds.events.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.ResponsiveUrl;
import com.google.android.material.chip.Chip;
import com.squareup.picasso.Picasso;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.models.TypeBillet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.text.format.DateFormat.getDateFormat;

public class CustomDataBindingAdapter {

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String url) {
        Picasso.get().load(url).into(view);
    }

    @BindingAdapter({"responsiveImg"})
    public static void setResponsiveImg(ImageView view, String publicId) {
        MediaManager.get().responsiveUrl(view, publicId, ResponsiveUrl.Preset.FIT, url -> {
            String link = url.secure(true).generate();
            Picasso.get().load(link).into(view);
        });
    }

    @SuppressLint("SimpleDateFormat")
    @BindingAdapter({"startDate"})
    public static void setStartDate(Chip chip, Date date) {
        String format = new SimpleDateFormat("dd/MM/yyyy à HH:mm").format(date);
        chip.setText(format);
    }

    @BindingAdapter({"priceUnit"})
    public static void setPriceUnit(Chip chip, List<TypeBillet> billets) {
        if (Utils.hasValue(billets) && billets.size() > 0) {
            TypeBillet billet = billets.get(0);
            String     pu     = String.format("%s MGA", billet.getPriceU());
            chip.setText(pu);
        } else chip.setText(R.string.type_free_default);
    }
    @BindingAdapter({"priceMin"})
    public static void setPriceMin(TextView view, List<TypeBillet> billets) {
        if (Utils.hasValue(billets) && billets.size() > 0) {
            TypeBillet billet = billets.get(0);
            String     pu     = String.format("A partir de %s MGA", billet.getPriceU());
            view.setText(pu);
        } else view.setText(R.string.type_free_default);
    }

    @BindingAdapter({"street"})
    public static void setSreet(Chip chip, String address) {
        if (Utils.hasValue(address)) {
            String[] values = address.split(",");
            chip.setText(values[0].trim());
        } else chip.setVisibility(View.GONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @BindingAdapter({"html"})
    public static void setHtml(TextView view, String html) {
        if (Utils.hasValue(html))
            view.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT));
    }

    public static void createList(Context context, View view, RecyclerView.Adapter adapter) {
        RecyclerView recycler = view.findViewById(R.id.recycler_view);
        recycler.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recycler.setLayoutManager(layoutManager);
        recycler.setAdapter(adapter);
    }
}
