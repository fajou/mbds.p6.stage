package com.tpt.mbds.events.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "opt_code")
public class OperateurCode {
    @PrimaryKey
    public int id;

    @ColumnInfo(name = "code_telma")
    public String codeTelma;
    @ColumnInfo(name = "code_airtel")
    public String codeAirtel;
    @ColumnInfo(name = "code_orange")
    public String codeOrange;

    public OperateurCode() { }

    @Ignore
    public OperateurCode(int id, String codeTelma, String codeAirtel, String codeOrange) {
        this.id = id;
        this.codeTelma = codeTelma;
        this.codeAirtel = codeAirtel;
        this.codeOrange = codeOrange;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodeTelma() {
        return codeTelma;
    }

    public void setCodeTelma(String codeTelma) {
        this.codeTelma = codeTelma;
    }

    public String getCodeAirtel() {
        return codeAirtel;
    }

    public void setCodeAirtel(String codeAirtel) {
        this.codeAirtel = codeAirtel;
    }

    public String getCodeOrange() {
        return codeOrange;
    }

    public void setCodeOrange(String codeOrange) {
        this.codeOrange = codeOrange;
    }
}