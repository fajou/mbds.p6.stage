package com.tpt.mbds.events.models;

import java.util.UUID;

public class QrCodePaimentInfo {
    private UUID   currentId;
    private long   eventId;
    private long   myEventId;
    private double prixTotal;
    private String code;
    private String telma;
    private String orange;
    private String airtel;

    public UUID getCurrentId() {
        return currentId;
    }

    public long getEventId() {
        return eventId;
    }

    public long getMyEventId() {
        return myEventId;
    }

    public String getCode() {
        return code;
    }

    public double getPrixTotal() {
        return prixTotal;
    }

    public String getTelma() {
        return telma;
    }

    public String getOrange() {
        return orange;
    }

    public String getAirtel() {
        return airtel;
    }
}
