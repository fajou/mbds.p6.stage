package com.tpt.mbds.events.models;

import android.annotation.SuppressLint;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.tpt.mbds.events.converter.JsonConverters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
@SuppressLint("SimpleDateFormat")
@TypeConverters(value = JsonConverters.class)
public class Event {
    @PrimaryKey
    private long             id;
    private String           hours;
    private String           title;
    private String           street;
    private Date             startDate;
    private Date             endDate;
    private String           localisation;
    private String           adress;
    private String           description;
    @TypeConverters(value = JsonConverters.class)
    private UserCompte       oraganisateur;
    @TypeConverters(value = JsonConverters.class)
    private SubCategorie     subCategorie;
    @TypeConverters(value = JsonConverters.class)
    private MediaCloud       video;
    @TypeConverters(value = JsonConverters.class)
    private List<TypeBillet> typeBillets;
    @TypeConverters(value = JsonConverters.class)
    private MediaCloud       defaultMedia;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }


    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TypeBillet> getTypeBillets() {
        return typeBillets;
    }

    public void setTypeBillets(List<TypeBillet> typeBillets) {
        this.typeBillets = typeBillets;
    }

    public UserCompte getOraganisateur() {
        return oraganisateur;
    }

    public void setOraganisateur(UserCompte oraganisateur) {
        this.oraganisateur = oraganisateur;
    }

    public SubCategorie getSubCategorie() {
        return subCategorie;
    }

    public void setSubCategorie(SubCategorie subCategorie) {
        this.subCategorie = subCategorie;
    }

    public MediaCloud getVideo() {
        return video;
    }

    public void setVideo(MediaCloud video) {
        this.video = video;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHours() {

        return startDate == null ? "à 18:00" : "à " + new SimpleDateFormat("HH:mm").format(startDate);
    }

    public MediaCloud getDefaultMedia() {
        return defaultMedia;
    }

    public void setDefaultMedia(MediaCloud mediaCloud) {
        this.defaultMedia = mediaCloud;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setStart(String start) {

    }
}
