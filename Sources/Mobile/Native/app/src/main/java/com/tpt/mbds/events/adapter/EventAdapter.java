package com.tpt.mbds.events.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.adapter.holder.EventHolder;
import com.tpt.mbds.events.databinding.CardEventBinding;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.ui.ClickHandler;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventHolder> {
    private List<Event> events;

    public EventAdapter(List<Event> providers) {
        this.events = providers;
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater   inflater     = LayoutInflater.from(parent.getContext());
        CardEventBinding eventBinding = DataBindingUtil.inflate(inflater, R.layout.card_event, parent, false);
        return new EventHolder(eventBinding, eventBinding.cardEvent);
    }

    @Override
    public void onBindViewHolder(EventHolder eventHolder, int position) {
        Event event = events.get(position);
        eventHolder.getEventBinding().setEvent(event);
        eventHolder.getEventBinding().setHandler(new ClickHandler());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }
}