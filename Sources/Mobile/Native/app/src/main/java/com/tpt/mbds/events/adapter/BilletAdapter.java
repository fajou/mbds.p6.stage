package com.tpt.mbds.events.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.Event;

import java.util.List;

public class BilletAdapter extends RecyclerView.Adapter<BilletAdapter.BilletHolder> {
    private int          ressourceItem;
    private List<Billet> datasources;
    private Context      context;

    public BilletAdapter(int ressourceItem, Context context, List<Billet> providers) {
        this.datasources = providers;
        this.context = context;
        this.ressourceItem = ressourceItem;
    }

    public static class BilletHolder extends RecyclerView.ViewHolder {
        private View view;

        public BilletHolder(View v) {
            super(v);
            view = v;
        }

        public View getView() {
            return view;
        }
    }

    @NonNull
    @Override
    public BilletHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(ressourceItem, parent, false);
        return new BilletHolder(v);
    }

    @Override
    public void onBindViewHolder(BilletHolder holder, int position) {
        Billet billet   = datasources.get(position);
    }

    @Override
    public int getItemCount() {
        return datasources.size();
    }
}