package com.tpt.mbds.events;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import com.tpt.mbds.events.constants.Navs;
import com.tpt.mbds.events.constants.Operateur;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.databinding.NavHeaderMainBinding;
import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.models.QrCodeBilletInfo;
import com.tpt.mbds.events.models.QrCodePaimentInfo;
import com.tpt.mbds.events.models.UserCompte;
import com.tpt.mbds.events.services.GsonRequest;
import com.tpt.mbds.events.services.PaiementService;
import com.tpt.mbds.events.services.responses.BilletResposne;
import com.tpt.mbds.events.services.responses.EventResponse;
import com.tpt.mbds.events.ui.compte.UserCompteViewModel;
import com.tpt.mbds.events.ui.home.HomeViewModel;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.tpt.mbds.events.constants.PathApi.EVENT_LOCAL;
import static com.tpt.mbds.events.constants.PathApi.GET_BILLETS;
import static com.tpt.mbds.events.constants.Utils.GSON;
import static com.tpt.mbds.events.constants.Utils.HIDE_SCANNER_HOWTO;
import static com.tpt.mbds.events.constants.Utils.QR_CODE_EXTRA;
import static com.tpt.mbds.events.constants.Utils.REQUEST_FOR_CALL;
import static com.tpt.mbds.events.constants.Utils.REQUEST_QR_CODE_PAIE;
import static com.tpt.mbds.events.constants.Utils.REQUEST_QR_CODE_TICKET;

public class MainActivity extends AppCompatActivity {
    private       AppBarConfiguration mAppBarConfiguration;
    private       UserCompteViewModel compteViewModel;
    private       HomeViewModel       homeViewModel;
    private       ViewModelProvider   viewModelProvider;
    private final ExecutorService     EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);
    private       SharedPreferences   shared;
    public static String              ACCESS_TOKEN;
    public static long                USER_COMTE_ID    = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setNavitation();
        shared = getSharedPreferences(Utils.SHARED_SESSION, MODE_PRIVATE);
        viewModelProvider = new ViewModelProvider(this);
        compteViewModel = viewModelProvider.get(UserCompteViewModel.class);
        homeViewModel = viewModelProvider.get(HomeViewModel.class);
        compteViewModel.getCompte().observe(this, this::reitriveData);
    }

    private void progres(boolean show) {
        ProgressBar bar = findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setNavitation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout   drawer = findViewById(R.id.drawer_layout);
        NavigationView nav    = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(Navs.NAVS_ID).setOpenableLayout(drawer).build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(nav, navController);

        View headerView = nav.getHeaderView(0);
        compteViewModel = new ViewModelProvider(this).get(UserCompteViewModel.class);
        NavHeaderMainBinding binding = NavHeaderMainBinding.bind(headerView);
        compteViewModel.getCompte().observe(this, binding::setUser);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    public void reitriveData(UserCompte compte) {
        USER_COMTE_ID = compte.getId();
        ACCESS_TOKEN = compte.getMobilesTokenId();
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", Utils.bearer(compte.getMobilesTokenId()));
        RequestQueue queue = Volley.newRequestQueue(this);
        progres(true);
        EXECUTOR_SERVICE.execute(() -> {
            GsonRequest<EventResponse> request = new GsonRequest<>(Request.Method.GET, EVENT_LOCAL, headers, null, EventResponse.class,
                    response -> {
                        if (!response.isError()) {
                            homeViewModel.inserts(response.getData().toArray(new Event[0]));
                        }
                        progres(false);
                    }, Throwable::printStackTrace);
            queue.add(request);
        });

        EXECUTOR_SERVICE.execute(() -> {
            GsonRequest<BilletResposne> billets = new GsonRequest<>(Request.Method.GET, GET_BILLETS, headers, null, BilletResposne.class,
                    response -> {
                        if (!response.isError()) {
                            homeViewModel.inserts(response.getData().toArray(new Billet[0]));
                        }
                    }, Throwable::printStackTrace);
            queue.add(billets);
        });
    }

    public void onClickBtnOK(View view) {
        View parent = findViewById(R.id.id_how_to_scan);
        if (parent != null) {
            CheckBox box = findViewById(R.id.id_never_show);
            shared.edit().putBoolean(HIDE_SCANNER_HOWTO, box.isChecked()).apply();
            parent.setVisibility(View.GONE);
        }
    }

    public void onClickPayTicket(View view) {
        Intent intent = new Intent(MainActivity.this, QRCodeScanner.class);
        startActivityForResult(intent, REQUEST_QR_CODE_PAIE);
    }

    public void onClickScanTicket(View view) {
        Intent intent = new Intent(MainActivity.this, QRCodeScanner.class);
        startActivityForResult(intent, REQUEST_QR_CODE_TICKET);
    }

    private void payerTickets(QrCodePaimentInfo paiment) {
        RadioGroup radioGroup = findViewById(R.id.radio_container);
        int        checkedId  = radioGroup.getCheckedRadioButtonId();
        String     operation;
        switch (checkedId) {
            case R.id.airtel_choix:
                operation = PaiementService.codeGsm(paiment, Operateur.AIRTEL);
                break;
            case R.id.orange_choix:
                operation = PaiementService.codeGsm(paiment, Operateur.ORANGE);
                break;
            default:
                operation = PaiementService.codeGsm(paiment, Operateur.TELMA);
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(String.format("tel:%s", Uri.encode(operation))));
        startActivityForResult(callIntent, REQUEST_FOR_CALL);
        /*doJob(operation);*/
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                case REQUEST_QR_CODE_PAIE:
                    if (resultCode == RESULT_OK) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                            String            result  = data.getStringExtra(QR_CODE_EXTRA);
                            QrCodePaimentInfo paiment = GSON.fromJson(result, QrCodePaimentInfo.class);
                            payerTickets(paiment);
                        } else Toast.makeText(this, R.string.erreur_call_dined, Toast.LENGTH_LONG).show();
                    } else Toast.makeText(this, R.string.qr_not_scanned_or_invalid, Toast.LENGTH_LONG).show();
                    break;
                case REQUEST_QR_CODE_TICKET:
                    if (resultCode == RESULT_OK) {
                        String           result    = data.getStringExtra(QR_CODE_EXTRA);
                        QrCodeBilletInfo eventInfo = GSON.fromJson(result, QrCodeBilletInfo.class);
                        Billet           billet    = homeViewModel.getBillet(eventInfo.getCode()).getValue();
                        setScannStatus(billet);
                    }
                    break;
                case REQUEST_FOR_CALL:
                    System.out.println("END USSD...........");
                    break;
                default:
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setScannStatus(Billet billet) {
        ImageView view = findViewById(R.id.ic_valide_ok);
        if (billet == null || billet.isChecked()) {
            view.setImageResource(R.drawable.ic_baseline_indeterminate_check_box_24);
        } else {
            view.setImageResource(R.drawable.ic_baseline_check_circle_24);
        }

        view.setVisibility(View.VISIBLE);
        view.animate().alpha(0f).setDuration(2000).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
            }
        });

        if (billet != null) {
            EXECUTOR_SERVICE.execute(() -> {
                billet.setChecked(true);
                homeViewModel.inserts(billet);
            });
        }
    }
}
