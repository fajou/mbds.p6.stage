package com.tpt.mbds.events.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.adapter.holder.InvitationHolder;
import com.tpt.mbds.events.databinding.CardInvitationBinding;
import com.tpt.mbds.events.models.UserInfo;

import java.util.List;

public class InvitationAdapter extends RecyclerView.Adapter<InvitationHolder> {
    private List<UserInfo> users;

    public InvitationAdapter(List<UserInfo> operateurCodes) {
        setUsers(operateurCodes);
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InvitationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater        inflater          = LayoutInflater.from(parent.getContext());
        CardInvitationBinding invitationBinding = DataBindingUtil.inflate(inflater, R.layout.card_invitation, parent, false);
        return new InvitationHolder(invitationBinding, invitationBinding.cardInvitation);
    }

    @Override
    public void onBindViewHolder(@NonNull InvitationHolder holder, int position) {
        UserInfo operateurCode = users.get(position);
        holder.getInvitationBinding().setUser(operateurCode);
    }

    @Override
    public int getItemCount() {
        return users != null ? users.size() : 0;
    }
}
