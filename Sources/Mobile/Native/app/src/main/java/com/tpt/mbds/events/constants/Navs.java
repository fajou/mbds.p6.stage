package com.tpt.mbds.events.constants;

import com.tpt.mbds.events.R;

public final class Navs {
    public static final int[] NAVS_ID = {
            R.id.nav_home,
            R.id.nav_proximite,
            R.id.nav_scan_topay,
            R.id.nav_mes_tickets
    };
}
