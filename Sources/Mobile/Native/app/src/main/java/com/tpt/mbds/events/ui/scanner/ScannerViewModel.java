package com.tpt.mbds.events.ui.scanner;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.tpt.mbds.events.models.Billet;
import com.tpt.mbds.events.models.OperateurCode;
import com.tpt.mbds.events.repositories.BilletRepository;
import com.tpt.mbds.events.repositories.PaimentRepository;

public class ScannerViewModel extends AndroidViewModel {
    private final PaimentRepository       repository;
    private       BilletRepository        billetRepository;
    private       LiveData<OperateurCode> code;

    public ScannerViewModel(Application application) {
        super(application);
        this.repository = new PaimentRepository(application);
        this.billetRepository = new BilletRepository(application);
        code = repository.getCode();
    }

    public LiveData<OperateurCode> getCode() {
        return code;
    }

    public void insert(OperateurCode code) {
        this.repository.insert(code);
    }

    public void inserts(Billet... billets) {
        billetRepository.inserts(billets);
    }
}
