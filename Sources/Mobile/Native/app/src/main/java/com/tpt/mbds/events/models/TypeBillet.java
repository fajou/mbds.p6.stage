package com.tpt.mbds.events.models;

public class TypeBillet {
    private long   id;
    private int    numbers;
    private int    numberTotalSales;
    private double priceU;
    private double priceTotal;
    private double priceTotalSales;
    private String libelle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public int getNumberTotalSales() {
        return numberTotalSales;
    }

    public void setNumberTotalSales(int numberTotalSales) {
        this.numberTotalSales = numberTotalSales;
    }

    public double getPriceU() {
        return priceU;
    }

    public void setPriceU(double priceU) {
        this.priceU = priceU;
    }

    public double getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    public double getPriceTotalSales() {
        return priceTotalSales;
    }

    public void setPriceTotalSales(double priceTotalSales) {
        this.priceTotalSales = priceTotalSales;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
