package com.tpt.mbds.events.converter;

import androidx.room.TypeConverter;
import com.google.gson.reflect.TypeToken;
import com.tpt.mbds.events.models.*;

import java.time.Duration;
import java.util.Date;
import java.util.List;

import static com.tpt.mbds.events.constants.Utils.GSON;
import static com.tpt.mbds.events.constants.Utils.hasValue;

public class JsonConverters {
    @TypeConverter
    public UserInfo toOject(String value) {
        return value == null ? null : GSON.fromJson(value, UserInfo.class);
    }

    @TypeConverter
    public String toJson(UserInfo info) {
        return info == null ? null : GSON.toJson(info);
    }

    @TypeConverter
    public MediaCloud toCloud(String value) {
        return value == null ? null : GSON.fromJson(value, MediaCloud.class);
    }

    @TypeConverter
    public String toJson(MediaCloud cloud) {
        return cloud == null ? null : GSON.toJson(cloud);
    }

    @TypeConverter
    public List<TypeBillet> getTypeBillets(String value) {
        if (hasValue(value)) {
            return GSON.fromJson(value, new TypeToken<List<TypeBillet>>() {
            }.getType());
        }
        return null;
    }

    @TypeConverter
    public String setTypeBillets(List<TypeBillet> typeBillets) {
        return hasValue(typeBillets) ? GSON.toJson(typeBillets) : null;
    }

    @TypeConverter
    public SubCategorie getSubCategorie(String value) {
        return hasValue(value) ? GSON.fromJson(value, SubCategorie.class) : null;
    }

    @TypeConverter
    public String setSubCategorie(SubCategorie subCategorie) {
        return hasValue(subCategorie) ? GSON.toJson(subCategorie) : null;
    }

    @TypeConverter
    public Date getDate(String value) {
        return hasValue(value) ? GSON.fromJson(value, Date.class) : null;
    }

    @TypeConverter
    public String setDate(Date value) {
        return hasValue(value) ? GSON.toJson(value) : null;
    }

    @TypeConverter
    public UserCompte getUserCompte(String value) {
        return hasValue(value) ? GSON.fromJson(value, UserCompte.class) : null;
    }

    @TypeConverter
    public String setUserCompte(UserCompte compte) {
        return hasValue(compte) ? GSON.toJson(compte) : null;
    }
}