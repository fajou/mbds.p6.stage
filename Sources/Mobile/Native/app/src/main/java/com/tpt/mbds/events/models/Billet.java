package com.tpt.mbds.events.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.tpt.mbds.events.converter.JsonConverters;

import java.util.Date;

@Entity
@TypeConverters(value = JsonConverters.class)
public class Billet {
    @PrimaryKey
    private long    id;
    private double  priceTotal;
    private double  priceU;
    private String  qrCode;
    private int     numbers;
    private Date    dateLimit;
    private boolean checked;

    public long getId() {
        return id;
    }

    public double getPriceTotal() {
        return priceTotal;
    }

    public double getPriceU() {
        return priceU;
    }

    public String getQrCode() {
        return qrCode;
    }

    public int getNumbers() {
        return numbers;
    }

    public Date getDateLimit() {
        return dateLimit;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    public void setPriceU(double priceU) {
        this.priceU = priceU;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public void setDateLimit(Date dateLimit) {
        this.dateLimit = dateLimit;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
