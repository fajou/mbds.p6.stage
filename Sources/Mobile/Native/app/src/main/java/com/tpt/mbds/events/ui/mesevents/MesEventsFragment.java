package com.tpt.mbds.events.ui.mesevents;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.tpt.mbds.events.R;
import com.tpt.mbds.events.adapter.EventGridAdapter;
import com.tpt.mbds.events.adapter.GridItemDecoration;
import com.tpt.mbds.events.constants.Utils;
import com.tpt.mbds.events.models.Event;
import com.tpt.mbds.events.services.GsonRequest;
import com.tpt.mbds.events.services.responses.MyEventResponse;
import com.tpt.mbds.events.ui.home.HomeViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;

import static com.tpt.mbds.events.MainActivity.ACCESS_TOKEN;
import static com.tpt.mbds.events.MainActivity.USER_COMTE_ID;
import static com.tpt.mbds.events.constants.PathApi.GET_MES_EVENTS;

@SuppressLint("SimpleDateFormat")
public class MesEventsFragment extends Fragment {
    private        HomeViewModel     homeViewModel;
    private        ViewModelProvider viewModelProvider;
    private static MyEventResponse   response;
    private        Date              currDate = new Date();
    private        Date              tomwDate = new Date();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View         root     = inflater.inflate(R.layout.fragment_mesevents, container, false);
        RecyclerView recycler = root.findViewById(R.id.mes_event_dispo_grid);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 1, GridLayoutManager.VERTICAL, false));
        int largePadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.shr_product_grid_spacing_small);
        recycler.addItemDecoration(new GridItemDecoration(largePadding, smallPadding));

        viewModelProvider = new ViewModelProvider(this);
        homeViewModel = viewModelProvider.get(HomeViewModel.class);
        onCalendarSelect(root.findViewById(R.id.calendar_events));
        reitriveDataRoom(root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        retrieveData();
        setTitleMesEvents();
    }

    private void onCalendarSelect(CalendarView calendarView) {
        calendarView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
            currDate = calendar.getTime();
            setTitleMesEvents();
            reitriveDataRoom(requireView());
        });
    }

    private void setTitleMesEvents() {
        TextView text = requireView().findViewById(R.id.id_date_events);
        text.setText(String.format("Evenements disponible le %s", new SimpleDateFormat("dd/MM/yyyy").format(currDate)));
    }

    private void progres(boolean show) {
        ProgressBar bar = requireView().findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void createGridEvenements(View view, List<Event> events) {
        List<Event> byDates = new ArrayList<>();
        String      curr    = new SimpleDateFormat("dd/MM/yyyy").format(currDate);
        for (Event event : events) {
            String start = new SimpleDateFormat("dd/MM/yyyy").format(event.getStartDate());
            if (curr.equals(start)) {
                byDates.add(event);
            }
        }

        RecyclerView     recycler = view.findViewById(R.id.mes_event_dispo_grid);
        EventGridAdapter adapter  = new EventGridAdapter(byDates);
        recycler.setAdapter(adapter);
    }

    private void reitriveDataRoom(View view) {
        if (response == null)
            return;
        Long[] ids = response.getData().toArray(new Long[0]);
        homeViewModel.getEventByIds(ids).observe(requireActivity(), (value) -> createGridEvenements(view, value));
    }

    private void retrieveData() {
        Executors.newSingleThreadExecutor().execute(() -> {
            progres(true);
            Map<String, String> headers = new HashMap<>();
            headers.put("Authorization", Utils.bearer(ACCESS_TOKEN));
            RequestQueue queue = Volley.newRequestQueue(requireContext());
            Executors.newSingleThreadExecutor().execute(() -> {
                GsonRequest<MyEventResponse> billets = new GsonRequest<>(Request.Method.GET, GET_MES_EVENTS + "/" + USER_COMTE_ID, headers, null, MyEventResponse.class,
                        response -> {
                            if (!response.isError()) {
                                MesEventsFragment.response = response;
                                reitriveDataRoom(requireView());
                            }
                            progres(false);
                        }, Throwable::printStackTrace);
                queue.add(billets);
            });
        });
    }
}