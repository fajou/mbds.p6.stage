package com.gps.tracker.livraison.adapter.items;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.gps.tracker.livraison.databinding.CardItemBinding;

public class ItemHolder extends RecyclerView.ViewHolder {
    private CardItemBinding itemBinding;

    public ItemHolder(CardItemBinding binding, View view) {
        super(view);
        itemBinding = binding;
    }

    public CardItemBinding getItemBinding() {
        return itemBinding;
    }
}