package com.gps.tracker.livraison;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.cloudinary.android.MediaManager;
import com.facebook.AccessToken;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.ui.compte.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity {
    private SharedPreferences shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorSplashScreen_Start));
        }
        setContentView(R.layout.activity_splash_screen);
        checkLoginStatus();
        View view = findViewById(R.id.logo);
        Utils.setLogo(view);
    }

    private void checkLoginStatus() {
        Handler handler = new Handler();
        shared = getSharedPreferences(Utils.SHARED_SESSION, MODE_PRIVATE);
        boolean     isConnected = shared.getBoolean(Utils.USER_CONNECTED, false);
        setMediaManager();

        if (isConnected) {
            handler.postDelayed(() -> {
                Intent intent = new Intent(this, MainClientActivity.class);
                startActivity(intent);
                finish();
            }, 500);
        } else {
            handler.postDelayed(() -> {
                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }, 2000);
        }
    }

    private void setMediaManager() {
        try {
            MediaManager.init(this);
        } catch (IllegalStateException ignored) {
        }
    }
}
