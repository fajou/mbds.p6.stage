package com.gps.tracker.livraison.constants;

public enum Operateur {
    TELMA,
    AIRTEL,
    ORANGE
}
