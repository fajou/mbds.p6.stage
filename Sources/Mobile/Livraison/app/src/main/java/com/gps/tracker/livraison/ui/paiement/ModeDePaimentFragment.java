package com.gps.tracker.livraison.ui.paiement;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils.SimpleStringSplitter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.services.UssdService;

import static android.content.Context.MODE_PRIVATE;
import static android.provider.Settings.Secure.ACCESSIBILITY_ENABLED;
import static android.provider.Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES;
import static android.provider.Settings.Secure.getInt;
import static com.gps.tracker.livraison.constants.Utils.HIDE_SCANNER_HOWTO;

public class ModeDePaimentFragment extends Fragment {
    private              ModePaimentViewModel modePaimentViewModel;
    private static final int                  REQUEST_PERMISSIONS = 201;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mode_paiement, container, false);
        setHowToScan(view);
        /*actualiseTickets();*/
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        modePaimentViewModel = new ViewModelProvider(this).get(ModePaimentViewModel.class);
        /*setRequestPermissions();*/
    }

    public void setHowToScan(View view) {
        View    howTo     = view.findViewById(R.id.id_how_to_scan);
        boolean showHowTo = view.getContext().getSharedPreferences(Utils.SHARED_SESSION, MODE_PRIVATE).getBoolean(HIDE_SCANNER_HOWTO, false);
        howTo.setVisibility(showHowTo ? View.GONE : View.VISIBLE);
    }

    private boolean isAccessibilitySettingsOn(Context mContext) throws Settings.SettingNotFoundException {
        int                  accessibilityEnabled = getInt(mContext.getApplicationContext().getContentResolver(), ACCESSIBILITY_ENABLED);
        final String         service              = String.format("%s/%s", mContext.getPackageName(), UssdService.class.getCanonicalName());
        SimpleStringSplitter mStringColonSplitter = new SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(mContext.getApplicationContext().getContentResolver(), ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext()) {
                    String accessibilityService = mStringColonSplitter.next();
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void setRequestPermissions() {
        try {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.BIND_ACCESSIBILITY_SERVICE}, REQUEST_PERMISSIONS);
            if (!isAccessibilitySettingsOn(requireContext())) {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }
}
