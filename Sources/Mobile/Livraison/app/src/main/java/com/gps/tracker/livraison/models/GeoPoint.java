package com.gps.tracker.livraison.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

public class GeoPoint {
    private double lat;
    private double lng;

    public GeoPoint() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public GeoPoint(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public static GeoPoint toGeoPoint(String str) {
        if (str == null || str.trim().equals("")) {
            return null;
        }
        return new Gson().fromJson(str, GeoPoint.class);
    }

    public LatLng getLatLng() {
        return new LatLng(lat, lng);
    }
}