package com.gps.tracker.livraison.models;

public class UserInfo {
    private long    id;
    private String  avatar;
    private String  city;
    private String  email;
    private String  first_name;
    private String  last_name;
    private boolean cousier;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFullName() {
        return String.format("%s %s", first_name, last_name);
    }

    public static UserInfo getClient() {
        UserInfo info = new UserInfo();
        info.avatar = "https://s3.amazonaws.com/uifaces/faces/twitter/janpalounek/128.jpg";
        info.email = "Emmalee94@yahoo.com";
        info.first_name = "Gleichner";
        info.last_name = "Emmalee";
        info.city = "Madagascar";
        info.id = 290;
        info.cousier = false;
        return info;
    }

    public static UserInfo getCousier() {
        UserInfo info = new UserInfo();
        info.avatar = "https://s3.amazonaws.com/uifaces/faces/twitter/peter576/128.jpg";
        info.email = "Katheryn.Willms@gmail.com";
        info.first_name = "Katheryn";
        info.last_name = "Willms";
        info.city = "Madagascar";
        info.id = 292;
        info.cousier = true;
        return info;
    }

    public boolean isCousier() {
        return cousier;
    }

    public void setCousier(boolean cousier) {
        this.cousier = cousier;
    }
}
