package com.gps.tracker.livraison.constants;

public class PathApi {
    public static final String LOGIN       = "/login";
    public static final String EVENT_LOCAL = "/events";
    public static final String GET_BILLETS = "/get-billets";
    public static final String GET_MES_EVENTS = "/get-mes-billets";
    public static final String EVENT_APROX = "/events-aproximite";
    public static final String FB_LOGIN    = "/fb-login";
}
