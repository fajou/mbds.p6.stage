package com.gps.tracker.livraison.constants;

public class Url {
    private static final String BASE_URL     = "http://10.0.2.1:8090/api";
    public static String path(String path) {
        return BASE_URL + path;
    }
}
