package com.gps.tracker.livraison.adapter.items;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.databinding.CardItemBinding;
import com.gps.tracker.livraison.models.Item;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemHolder> {
    private List<Item> items;

    public ItemAdapter(@NonNull List<Item> providers) {
        this.items = providers;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater  inflater    = LayoutInflater.from(parent.getContext());
        CardItemBinding itemBinding = DataBindingUtil.inflate(inflater, R.layout.card_item, parent, false);
        return new ItemHolder(itemBinding, itemBinding.itemGridCard);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        Item item = items.get(position);
        holder.getItemBinding().setItem(item);
        holder.getItemBinding().setHandler(new ItemClickHandler());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
