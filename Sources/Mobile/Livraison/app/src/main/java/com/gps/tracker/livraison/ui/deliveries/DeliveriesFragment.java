package com.gps.tracker.livraison.ui.deliveries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.adapter.CustomDataBindingAdapter;
import com.gps.tracker.livraison.adapter.commandes.CommandeAdapter;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.Commande;

import java.util.ArrayList;
import java.util.List;

public class DeliveriesFragment extends Fragment {

    private DeliveriesViewModel mViewModel;

    public static DeliveriesFragment newInstance() {
        return new DeliveriesFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_deliveries, container, false);
        Utils.setLogo(view);
        createListComds(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DeliveriesViewModel.class);
    }

    private void createListComds(View view) {
        List<Commande> commandes = new ArrayList<>();
        commandes.add(new Commande("Mr. Dupond", "dupond@gmail.com", "3 articles", "36 000 MGA", "36 000 MGA", "data_store/images/1ca327c1066d452598b3476a069c1d01", "0.1 Kg", true));
        commandes.add(new Commande("Me. Duponde", "dupond@gmail.com", "3 articles", "12 000 MGA", "12 000 MGA", "data_store/images/23c8533582f64bb0a33265f7c240088f", "0.2 Kg", false));
        CustomDataBindingAdapter.createList(getContext(), view, new CommandeAdapter(commandes));
    }

}