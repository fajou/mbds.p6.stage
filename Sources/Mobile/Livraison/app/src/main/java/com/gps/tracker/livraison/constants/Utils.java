package com.gps.tracker.livraison.constants;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.ResponsiveUrl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.models.UserInfo;
import com.squareup.picasso.Picasso;

public class Utils {
    public static final String   SHARED_SESSION            = "com.gps.tracker.livraison.session";
    public static final Gson     GSON                      = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();
    public static final String   USER_CONNECTED            = "IS_USER_CONNECTE";
    public static final String   HIDE_SCANNER_HOWTO        = "HIDE_SCANNER_HOWTO";
    public static final String   ID                        = "ID_";
    public static final String   QR_CODE_EXTRA             = "com.gps.tracker.livraison.QR_CODE";
    public static final String   CALL_USSD_FOR_PAIMENT     = "com.gps.tracker.livraison.CALL_USSD";
    public static final int      REQUEST_CODE              = 1;
    public static final int      REQUEST_QR_CODE_PAIE      = 1425;
    public static final int      REQUEST_QR_CODE_TICKET    = 1426;
    public static final int      REQUEST_FOR_CALL          = 1525;
    public static final String   EXTRA_REPLY               = "com.gps.tracker.livraison.REPLY";
    public static final String   OPT_MOBILE                = "mobile";
    public static final String   GOOGLE_MAP_DIRECTIONS_API = "https://maps.googleapis.com/maps/api/directions/json";
    private static      UserInfo userConnected;

    public static String bearer(String token) {
        String bearer = String.format("%s %s", "Bearer", token);
        System.out.println(bearer);
        return bearer;
    }

    public static boolean hasValue(Object o) {
        return o != null;
    }

    public static void setLogo(View view) {
        try {
            ImageView image = view.findViewById(R.id.logo);
            MediaManager.get().responsiveUrl(image, "pub_events/logos_icons/Logo", ResponsiveUrl.Preset.FIT, url -> {
                String link = url.secure(true).generate();
                Picasso.get().load(link).into(image);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }


    public static void setNfcScanSuccess(View view) {
        try {
            pl.droidsonroids.gif.GifImageView image = view.findViewById(R.id.nfc_tag_image);
            MediaManager.get().responsiveUrl(image, "pub_events/logos_icons/success", ResponsiveUrl.Preset.AUTO_FILL, url -> {
                String link = url.secure(true).generate();
                Glide.with(view).load(link).into(image);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void setImage(ImageView image, String publicId) {
        try {
            MediaManager.get().responsiveUrl(image, publicId, ResponsiveUrl.Preset.FIT, url -> {
                String link = url.secure(true).generate();
                Picasso.get().load(link).into(image);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void setUserConnected(UserInfo info) {
        userConnected = info;
    }

    public static UserInfo getUserConnected() {
        return userConnected;
    }

    public static void setCustomer(boolean isCusto, View view) {
        View user       = view.findViewById(R.id.commande_user_info);
        View livraison  = view.findViewById(R.id.livraison_dans);
        View client     = view.findViewById(R.id.client_confirmer);
        View saisieCode = view.findViewById(R.id.conducteur_confirmer);
        View commande   = view.findViewById(R.id.code_commande);
        View actions    = view.findViewById(R.id.details_actions);

        user.setVisibility(isCusto ? View.GONE : View.VISIBLE);
        livraison.setVisibility(isCusto ? View.VISIBLE : View.GONE);
        client.setVisibility(isCusto ? View.VISIBLE : View.GONE);
        saisieCode.setVisibility(isCusto ? View.GONE : View.VISIBLE);
        commande.setVisibility(isCusto ? View.VISIBLE : View.GONE);
        actions.setVisibility(isCusto ? View.VISIBLE : View.GONE);
    }

}
