package com.gps.tracker.livraison.adapter.items;

import android.content.Intent;
import android.view.View;

import com.gps.tracker.livraison.NFCScanningActivity;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.Item;

public class ItemClickHandler {
    public void onClickItem(View view, Item item) {
        Intent intent = new Intent(view.getContext(), NFCScanningActivity.class);
        intent.putExtra(Utils.ID, item.getName());
        view.getContext().startActivity(intent);
    }
}
