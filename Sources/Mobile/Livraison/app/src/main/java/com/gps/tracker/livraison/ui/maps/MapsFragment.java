package com.gps.tracker.livraison.ui.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.BindingAdapter;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.GeoPoint;
import com.gps.tracker.livraison.models.GpsTraceur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompClient;

public class MapsFragment extends Fragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, LocationListener {
    public static        List<GeoPoint>  LOCAL_POSITIONS;
    private static final int             REQUEST_LOCATION = 1001;
    private              LocationManager locationManager;
    private              GoogleMap       mMap;
    private              MarkerOptions   CURRENT_LOCATION;
    private              StompClient     mStompClient;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        checkPermision();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @BindingAdapter({"mapLatLng"})
    public static void setMapLatLng(FragmentContainerView c, String latLng) {
    }

    private void checkPermision() {
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
    }

    private void setCurrentMarker() {
        locationManager = (LocationManager) requireContext().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria     = new Criteria();
        String   bestProvider = locationManager.getBestProvider(criteria, true);
        if (bestProvider == null)
            return;

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        Location location = locationManager.getLastKnownLocation(bestProvider);
        if (location != null) {
            double lat = -20.2569255;
            double lng = 57.46405899999999;
            /*double lat    = location.getLatitude();
            double lng    = location.getLongitude();*/
            /*LatLng latLng = new LatLng(lat, lng);
            CURRENT_LOCATION = new MarkerOptions().position(latLng).title("Moi");
            mMap.addMarker(CURRENT_LOCATION).showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));*/
            //onLocationChanged(location);
        }
    }

    private void setEventsAprox() {
        LOCAL_POSITIONS = new ArrayList<>();
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.2569255,\"lng\":57.46405899999999}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.442878,\"lng\":57.566779}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.495385,\"lng\":57.552776}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.510295,\"lng\":57.499508}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.503698,\"lng\":57.459701}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.500707,\"lng\":57.447255}"));
        LOCAL_POSITIONS.add(GeoPoint.toGeoPoint("{\"lat\":-20.502158,\"lng\":57.436388}"));
        String[]      customers                 = {"Moi", "Xavier", "Eric", "Emanuelle", "Christian", "Ilou", "Olivier"};
        int           i                         = 0;
        StringBuilder google_map_directions_api = new StringBuilder(Utils.GOOGLE_MAP_DIRECTIONS_API);
        google_map_directions_api.append("?origin=").append(LOCAL_POSITIONS.get(0).getLat()).append(",").append(LOCAL_POSITIONS.get(0).getLng());
        google_map_directions_api.append("&destination=").append(LOCAL_POSITIONS.get(LOCAL_POSITIONS.size() - 1).getLat()).append(",").append(LOCAL_POSITIONS.get(LOCAL_POSITIONS.size() - 1).getLng());
        google_map_directions_api.append("@waypoints=");
        PolylineOptions options = new PolylineOptions().geodesic(true).color(Color.BLUE);
        for (GeoPoint point : LOCAL_POSITIONS) {
//            LatLng position = GSON.fromJson(event.getLocalisation(), GeoPoint.class).getLatLng();
            LatLng position = point.getLatLng();
            options.add(position);
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(position)
                    .visible(true)
                    .title(customers[i]));
            marker.showInfoWindow();
//            marker.setTag("Id");
            i++;
            google_map_directions_api.append("via:").append(point.getLat()).append("%2C").append(point.getLng());
            if (i < LOCAL_POSITIONS.size()) google_map_directions_api.append("%7C");
        }
        google_map_directions_api.append("&key=" + R.string.google_maps_key);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(LOCAL_POSITIONS.get(0).getLatLng()));
        Polyline line = mMap.addPolyline(options);

        try {
            //String jsonData = downloadUrl(google_map_directions_api);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setEventsAprox();
        mMap.setOnMarkerClickListener(this);
        setCurrentMarker();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        Toast.makeText(requireContext(), marker.getTitle(), Toast.LENGTH_LONG).show();
        return false;
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        double latitude  = location.getLatitude();
        double longitude = location.getLongitude();
        LatLng latLng    = new LatLng(latitude, longitude);
        CURRENT_LOCATION.position(latLng);
        sendDataSocket(new GeoPoint(latitude, longitude));
    }

    @SuppressLint("CheckResult")
    public void sendDataSocket(GeoPoint position) {
        mStompClient = Stomp.over(Stomp.ConnectionProvider.JWS, "ws://192.168.100.2:8090/gps-traceur-websocket/websocket");
        mStompClient.connect();

        mStompClient.topic("/gps/receives").subscribe(topicMessage -> {
            System.out.println(topicMessage.getPayload());
        }).dispose();

        GpsTraceur traceur = new GpsTraceur();
        traceur.setDeliveryId(0);
        traceur.setVehicleId(1);
        GeoPoint geoPosition = new GeoPoint();
        geoPosition.setLat(position.getLat());
        geoPosition.setLng(position.getLng());
        traceur.setLocation(geoPosition);
        String data = new Gson().toJson(traceur);
        mStompClient.send("/app/traceur", data).subscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMap != null) {
            mMap.clear();
            setEventsAprox();
            setCurrentMarker();
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String            data          = "";
        InputStream       iStream       = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer   sb = new StringBuffer();
            String         line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            if (iStream != null) iStream.close();
            if (urlConnection != null) urlConnection.disconnect();
        }
        return data;
    }
}
