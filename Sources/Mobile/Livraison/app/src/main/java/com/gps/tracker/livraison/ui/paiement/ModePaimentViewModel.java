package com.gps.tracker.livraison.ui.paiement;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gps.tracker.livraison.models.OperateurCode;
import com.gps.tracker.livraison.repositories.PaimentRepository;

public class ModePaimentViewModel extends AndroidViewModel {
    private final PaimentRepository       repository;
    private       LiveData<OperateurCode> code;

    public ModePaimentViewModel(Application application) {
        super(application);
        this.repository = new PaimentRepository(application);
        code = repository.getCode();
    }

    public LiveData<OperateurCode> getCode() {
        return code;
    }

    public void insert(OperateurCode code) {
        this.repository.insert(code);
    }
}
