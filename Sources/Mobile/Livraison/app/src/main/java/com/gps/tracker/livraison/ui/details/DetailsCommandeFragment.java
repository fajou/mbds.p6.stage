package com.gps.tracker.livraison.ui.details;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.gps.tracker.livraison.NFCScanningActivity;
import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.adapter.CustomDataBindingAdapter;
import com.gps.tracker.livraison.adapter.items.ItemAdapter;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.Item;
import com.gps.tracker.livraison.ui.paiement.ModeDePaimentFragment;

import java.util.ArrayList;
import java.util.List;

public class DetailsCommandeFragment extends Fragment {
    private DetailsCommandeViewModel mViewModel;

    public static DetailsCommandeFragment newInstance() {
        return new DetailsCommandeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_commande, container, false);
        Utils.setLogo(view);
        createListItems(view);
        Utils.setImage(view.findViewById(R.id.avatar_logo), "data_store/images/1ca327c1066d452598b3476a069c1d01");
        Utils.setCustomer(!Utils.getUserConnected().isCousier(), view);
        Button boutton_valider = view.findViewById(R.id.btn_verifier);
        boutton_valider.setOnClickListener(v -> loadPaiementActivity());
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(DetailsCommandeViewModel.class);
    }

    private void createListItems(View view) {
        List<Item> items = new ArrayList<>();
        items.add(new Item("Chapeau ADIDAS", "1 Article", "36 000 MGA", "36 000 MGA", "data_store/images/066701e4cd844bf788634c9cd207e47f", "0.1 Kg"));
        items.add(new Item("Chapeau Bas - Chaud", "1 Article", "12 000 MGA", "12 000 MGA", "data_store/images/68ed0ec49bab4c75b9dac3bed9cd9bf7", "0.2 Kg"));
        items.add(new Item("Vans cuire", "1 Article", "12 000 MGA", "12 000 MGA", "data_store/images/e5dba5c1065a491a81b3d5caff057d23", "0.15 Kg"));
        CustomDataBindingAdapter.createList(getContext(), view, new ItemAdapter(items));
    }

    public void loadPaiementActivity() {
        ModeDePaimentFragment modeDePaimentFragment = new ModeDePaimentFragment();
        requireActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container_view_tag, modeDePaimentFragment, "fragment_container_view_tag")
                .addToBackStack(null)
                .commit();
    }

    public void onClickConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Confirmer");
        builder.setMessage("Voulez vous effectuez cette action?");
        builder.setCancelable(true);
        builder.setPositiveButton("Oui",
                (dialog, id) -> {
                    Intent intent = new Intent(getContext(), NFCScanningActivity.class);
                    startActivity(intent);
                });
        builder.setNegativeButton("Non", (dialog, id) -> {
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}