package com.gps.tracker.livraison.models;

public class UserLogin {
    private String  userName;
    private String  password;
    private boolean isCoursier;

    public UserLogin(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCoursier() {
        return isCoursier;
    }

    public void setCoursier(boolean coursier) {
        isCoursier = coursier;
    }

    public boolean isValide() {
        /*TODO: implemente an REST api for login*/
        if ("client@gmail.com".equals(userName) && "root".equals(password))
            return true;
        else if ("coursier@gmail.com".equals(userName) && "root".equals(password)) {
            isCoursier = true;
            return true;
        }
        return false;
    }
}
