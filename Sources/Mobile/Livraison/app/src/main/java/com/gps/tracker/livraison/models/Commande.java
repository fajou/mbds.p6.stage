package com.gps.tracker.livraison.models;

public class Commande {
    private boolean livree;
    private String  name;
    private String  mail;
    private String  nbs;
    private String  prixU;
    private String  prixT;
    private String  imgUrl;
    private String  weight;

    public Commande(String name, String mail, String nbs, String prixU, String prixT, String imgUrl, String weight, boolean livree) {
        this.name = name;
        this.nbs = nbs;
        this.mail = mail;
        this.prixU = prixU;
        this.prixT = prixT;
        this.imgUrl = imgUrl;
        this.weight = weight;
        this.livree = livree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNbs() {
        return nbs;
    }

    public void setNbs(String nbs) {
        this.nbs = nbs;
    }

    public String getPrixU() {
        return prixU;
    }

    public void setPrixU(String prixU) {
        this.prixU = prixU;
    }

    public String getPrixT() {
        return prixT;
    }

    public void setPrixT(String prixT) {
        this.prixT = prixT;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isLivree() {
        return livree;
    }

    public void setLivree(boolean livree) {
        this.livree = livree;
    }
}
