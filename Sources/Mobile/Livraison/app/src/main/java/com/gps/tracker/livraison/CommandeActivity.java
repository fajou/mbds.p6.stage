package com.gps.tracker.livraison;

import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.gps.tracker.livraison.adapter.CustomDataBindingAdapter;
import com.gps.tracker.livraison.adapter.items.ItemAdapter;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.Item;

import java.util.ArrayList;
import java.util.List;

import static android.nfc.NdefRecord.createMime;

public class CommandeActivity extends Activity implements CreateNdefMessageCallback {
    NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commande);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        View view = findViewById(R.id.container_details_commandes);
        Utils.setCustomer(!Utils.getUserConnected().isCousier(), view);
        if (nfcAdapter == null) {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        nfcAdapter.setNdefPushMessageCallback(this, this);
        createListItems(view);
    }

    private void createListItems(View view) {
        List<Item> items = new ArrayList<>();
        items.add(new Item("Chapeau ADIDAS", "1 Article", "36 000 MGA", "36 000 MGA", "data_store/images/066701e4cd844bf788634c9cd207e47f", "0.1 Kg"));
        items.add(new Item("Chapeau Bas - Chaud", "1 Article", "12 000 MGA", "12 000 MGA", "data_store/images/68ed0ec49bab4c75b9dac3bed9cd9bf7", "0.2 Kg"));
        items.add(new Item("Vans cuire", "1 Article", "12 000 MGA", "12 000 MGA", "data_store/images/e5dba5c1065a491a81b3d5caff057d23", "0.15 Kg"));
        CustomDataBindingAdapter.createList(getBaseContext(), view, new ItemAdapter(items));
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        String text = ("Beam me up, Android!\n\n" + "Beam Time: " + System.currentTimeMillis());
        NdefMessage msg = new NdefMessage(new NdefRecord[]{
                createMime("application/vnd.com.example.android.beam", text.getBytes())
                /**
                 * The Android Application Record (AAR) is commented out. When a device
                 * receives a push with an AAR in it, the application specified in the AAR
                 * is guaranteed to run. The AAR overrides the tag dispatch system.
                 * You can add it back in to guarantee that this
                 * activity starts when receiving a beamed message. For now, this code
                 * uses the tag dispatch system.
                */
                //,NdefRecord.createApplicationRecord("com.example.android.beam")
        });
        return msg;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            processIntent(getIntent());
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    void processIntent(Intent intent) {
        TextInputEditText text    = findViewById(R.id.code_delivery);
        Parcelable[]      rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        text.setText(new String(msg.getRecords()[0].getPayload()));
    }
}