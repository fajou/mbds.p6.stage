package com.gps.tracker.livraison.services.responses;

import com.gps.tracker.livraison.models.UserInfo;

public class LoginResponse {
    private boolean  error;
    private String   message;
    private UserInfo data;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserInfo getData() {
        return data;
    }

    public void setData(UserInfo data) {
        this.data = data;
    }
}
