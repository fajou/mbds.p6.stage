package com.gps.tracker.livraison.repositories.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.gps.tracker.livraison.models.OperateurCode;

@Dao
public interface PaimentDao {
    @Query("SELECT * FROM opt_code limit 1")
    LiveData<OperateurCode> get();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(OperateurCode code);
}
