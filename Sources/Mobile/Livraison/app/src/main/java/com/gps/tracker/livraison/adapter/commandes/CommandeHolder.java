package com.gps.tracker.livraison.adapter.commandes;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.gps.tracker.livraison.databinding.CardCommandeBinding;

public class CommandeHolder extends RecyclerView.ViewHolder {
    private CardCommandeBinding itemBinding;

    public CommandeHolder(CardCommandeBinding binding, View view) {
        super(view);
        itemBinding = binding;
    }

    public CardCommandeBinding getItemBinding() {
        return itemBinding;
    }
}