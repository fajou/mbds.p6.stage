package com.gps.tracker.livraison;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.provider.Settings;

import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class NFCScanningActivity extends AppCompatActivity implements NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {
    private NfcAdapter  nfcAdapter;
    private NdefMessage ndefMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc_scanning);
        setImgHowToScan();
        checkFeatureNFC();
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (nfcAdapter == null) {
            return;
        }

        if (!nfcAdapter.isNdefPushEnabled()) {
            startActivity(new Intent(Settings.ACTION_NFCSHARING_SETTINGS));
        }

        String msgTxt     = "Hello worldaa!";
        byte[] lang       = Locale.getDefault().getLanguage().getBytes(StandardCharsets.UTF_8);
        int    langeSize  = lang.length;
        byte[] data       = msgTxt.getBytes(StandardCharsets.UTF_8);
        int    dataLength = data.length;

        ByteArrayOutputStream payload = new ByteArrayOutputStream(1 + langeSize + dataLength);
        payload.write((byte) (langeSize & 0x1F));
        payload.write(lang, 0, langeSize);
        NdefRecord ndefRecord = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload.toByteArray());
        ndefMessage = new NdefMessage(ndefRecord);
        initNFCAdapter(ndefMessage);
    }

    private void initNFCAdapter(NdefMessage message) {
        nfcAdapter.setNdefPushMessageCallback(this, this);
        nfcAdapter.setNdefPushMessage(message, this, this);
        nfcAdapter.setOnNdefPushCompleteCallback(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled()) {
                checkNFC();
            }
            initNFCAdapter(ndefMessage);
        }
    }

    public NdefMessage createNdefMessage(NfcEvent event) {
        return ndefMessage;
    }

    public void onNdefPushComplete(NfcEvent arg0) {
        setImgSuccess();
    }

    private void setImgHowToScan() {
        try {
            GifImageView view          = findViewById(R.id.nfc_tag_image);
            GifDrawable  gifFromAssets = new GifDrawable(getAssets(), "CfKT.gif");
            view.setImageDrawable(gifFromAssets);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setImgSuccess() {
        try {
            GifImageView view          = findViewById(R.id.nfc_tag_image);
            GifDrawable  gifFromAssets = new GifDrawable(getAssets(), "success.gif");
            view.setImageDrawable(gifFromAssets);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkFeatureNFC() {
        PackageManager pkManager = getPackageManager();
        if (!pkManager.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            finish();
        }

        NfcManager nfcManager = (NfcManager) getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter    = nfcManager.getDefaultAdapter();
        if (adapter != null && !adapter.isEnabled()) {
            checkNFC();
        }
    }

    public void checkNFC() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Activer NFC");
        builder.setMessage("Vous devez activer la fonctionnalité NFC.");
        builder.setCancelable(true);
        builder.setPositiveButton("Activer", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS)));
        builder.setNegativeButton("Non merci", (dialog, id) -> finish());

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
