package com.gps.tracker.livraison.ui.compte;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.cloudinary.android.MediaManager;
import com.facebook.CallbackManager;
import com.gps.tracker.livraison.MainClientActivity;
import com.gps.tracker.livraison.MainCoursierActivity;
import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.databinding.ActivityLoginBinding;
import com.gps.tracker.livraison.models.UserInfo;
import com.gps.tracker.livraison.models.UserLogin;
import com.gps.tracker.livraison.services.responses.LoginResponse;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.gps.tracker.livraison.constants.Utils.USER_CONNECTED;

public class LoginActivity extends AppCompatActivity {
    private static final ExecutorService   EXECUTOR_SERVICE = Executors.newFixedThreadPool(1);
    private              SharedPreferences shared;
    private              CallbackManager   callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setUser(new UserLogin("client@gmail.com", "root"));
        callbackManager = CallbackManager.Factory.create();
        setMediaManager();
        View view = findViewById(R.id.lineaire_container);
        Utils.setLogo(view);
    }

    private void progres(boolean show) {
        ProgressBar bar = findViewById(R.id.loading_spinner);
        bar.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void setMediaManager() {
        try {
            MediaManager.init(this);
        } catch (IllegalStateException ignored) {
        }
    }

    public void goToHome(UserLogin login) {
        Intent intent;
        if (login.isCoursier()) {
            Utils.setUserConnected(UserInfo.getCousier());
            intent = new Intent(this, MainCoursierActivity.class);
        } else {
            Utils.setUserConnected(UserInfo.getClient());
            intent = new Intent(this, MainClientActivity.class);
        }
        startActivity(intent);
        finish();
    }

    public void onClickLogin(View view) {
        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.password);

        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
            progres(true);
            String          userName = username.getText().toString();
            String          pasword  = password.getText().toString();
            final UserLogin login    = new UserLogin(userName, pasword);
            if (login.isValide()) {
                goToHome(login);
            } else {
                Toast.makeText(this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
            }
            /*Utliser cette code commanter pour appeller le REST API de votre propre login*/
            /*EXECUTOR_SERVICE.execute(() -> {
                RequestQueue queue = Volley.newRequestQueue(this);
                GsonRequest<LoginResponse> request = new GsonRequest<>(Request.Method.POST, PathApi.LOGIN, null, GSON.toJson(login), LoginResponse.class,
                        this::onResponse, Throwable::printStackTrace);
                request.setContentType("application/json");
                queue.add(request);
            });*/
        } else {
            Toast.makeText(this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onResponse(LoginResponse response) {
        if (!response.isError()) {
            shared = getSharedPreferences(Utils.SHARED_SESSION, MODE_PRIVATE);
            shared.edit().putBoolean(USER_CONNECTED, true).apply();
            progres(false);
        } else Toast.makeText(this, "Invalid nom d'utilisateur ou mots de passe", Toast.LENGTH_LONG).show();
    }
}
