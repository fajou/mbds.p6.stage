package com.gps.tracker.livraison.services;

public class UssdPaiment {
    private String numero;
    private long   totalApayer;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public long getTotalApayer() {
        return totalApayer;
    }

    public void setTotalApayer(long totalApayer) {
        this.totalApayer = totalApayer;
    }
}
