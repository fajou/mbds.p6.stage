package com.gps.tracker.livraison.adapter.commandes;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.gps.tracker.livraison.R;
import com.gps.tracker.livraison.databinding.CardCommandeBinding;
import com.gps.tracker.livraison.models.Commande;

import java.util.List;

public class CommandeAdapter extends RecyclerView.Adapter<CommandeHolder> {
    private List<Commande> commandes;

    public CommandeAdapter(@NonNull List<Commande> providers) {
        this.commandes = providers;
    }

    @NonNull
    @Override
    public CommandeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater      inflater    = LayoutInflater.from(parent.getContext());
        CardCommandeBinding itemBinding = DataBindingUtil.inflate(inflater, R.layout.card_commande, parent, false);
        return new CommandeHolder(itemBinding, itemBinding.commandeGridCard);
    }

    @Override
    public void onBindViewHolder(@NonNull CommandeHolder holder, int position) {
        Commande item = commandes.get(position);
        if (item.isLivree()) {
            holder.getItemBinding().commandeStatus.setImageResource(R.drawable.ic_baseline_check_circle_24);
        }
        holder.getItemBinding().setCommande(item);
        holder.getItemBinding().setHandler(new CommandeClickHandler());
    }

    @Override
    public int getItemCount() {
        return commandes.size();
    }
}
