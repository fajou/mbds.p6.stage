package com.gps.tracker.livraison.adapter.commandes;

import android.content.Intent;
import android.view.View;

import com.gps.tracker.livraison.CommandeActivity;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.models.Commande;

public class CommandeClickHandler {
    public void onClickItem(View view, Commande item) {
        Intent intent = new Intent(view.getContext(), CommandeActivity.class);
        intent.putExtra(Utils.ID, item.getName());
        view.getContext().startActivity(intent);
    }
}
