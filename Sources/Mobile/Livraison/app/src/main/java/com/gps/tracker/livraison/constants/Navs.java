package com.gps.tracker.livraison.constants;

import com.gps.tracker.livraison.R;

public final class Navs {
    public static final int[] NAVS_ID = {
            R.id.nav_home,
            R.id.nav_proximite,
            R.id.nav_nfc_topay
    };
}
