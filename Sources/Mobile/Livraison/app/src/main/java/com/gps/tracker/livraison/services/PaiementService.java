package com.gps.tracker.livraison.services;

import com.gps.tracker.livraison.constants.Operateur;
import com.gps.tracker.livraison.models.OperateurCode;

public class PaiementService {
    public static OperateurCode CODE = new OperateurCode(1, "*111*1*4*1*%s*%s#", "*436*1*4*1*%s*%s#", "*121*1*4*1*%s*%s#");

    public static String codeGsm(UssdPaiment paiment, Operateur opt) {
        if (opt == Operateur.TELMA)
            return String.format(CODE.getCodeTelma(), paiment.getNumero(), paiment.getTotalApayer());
        if (opt == Operateur.ORANGE)
            return String.format(CODE.getCodeOrange(), paiment.getNumero(), paiment.getTotalApayer());
        if (opt == Operateur.AIRTEL)
            return String.format(CODE.getCodeAirtel(), paiment.getNumero(), paiment.getTotalApayer());
        return "";
    }
}
