package com.gps.tracker.livraison;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.cloudinary.android.MediaManager;
import com.google.android.material.navigation.NavigationView;
import com.gps.tracker.livraison.constants.Navs;
import com.gps.tracker.livraison.constants.Utils;
import com.gps.tracker.livraison.databinding.NavHeaderMainBinding;
import com.gps.tracker.livraison.ui.paiement.ModeDePaimentFragment;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainClientActivity extends AppCompatActivity {
    public static String              ACCESS_TOKEN;
    public static long                USER_COMTE_ID    = 0L;
    private final ExecutorService     EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);
    private       AppBarConfiguration mAppBarConfiguration;
    private       ViewModelProvider   viewModelProvider;
    private       SharedPreferences   shared;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_client);
        setNavitation();
        setMediaManager();
    }

    private void setNavitation() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout   drawer = findViewById(R.id.drawer_layout);
        NavigationView nav    = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(Navs.NAVS_ID).setOpenableLayout(drawer).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(nav, navController);

        View                 headerView = nav.getHeaderView(0);
        NavHeaderMainBinding binding    = NavHeaderMainBinding.bind(headerView);
        binding.setUser(Utils.getUserConnected());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_client_drawer, menu);
        return true;
    }

    private void setMediaManager() {
        try {
            MediaManager.init(this);
        } catch (IllegalStateException ignored) {
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    public void onClickPayer(View view) {
        ModeDePaimentFragment paimentFragment = new ModeDePaimentFragment();
        this.getSupportFragmentManager().beginTransaction()
                .replace(R.id.nav_host_fragment, paimentFragment, "fragment_container_view_tag")
                .addToBackStack("fragment_container_view_tag")
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }
}