import React, {useEffect} from 'react';
import './App.css';
import Header from './Components/Header/Header'
import Footer from './Components/Footer/Footer';
import Home from "./Pages/Home/Home";
import Child from "./Components/Child/Child";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Details from "./Pages/Details/Details";

function App() {
    return (
        <Child>
            <Header/>
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route path="/details/:id" component={Details} />
                        {/* <Details/>
                    </Route> */}
                </Switch>
            </Router>
            <Footer/>
        </Child>
    );
}

export default App;
