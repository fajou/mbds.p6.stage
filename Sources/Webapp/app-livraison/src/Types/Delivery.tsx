export default interface Delivery {
    "delivery": string,
    "duration": string,
    "costs": string
}