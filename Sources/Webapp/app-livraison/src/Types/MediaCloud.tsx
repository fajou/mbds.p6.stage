export default interface MediaCloud {
    resource_type: string,
    original_extension: string,
    secure_url: string,
    asset_id: string,
    type: string,
    version: number,
    url: string,
    public_id: string,
    tags: []
    original_filename: string,
    placeholder: boolean,
    width: number,
    height: number
}