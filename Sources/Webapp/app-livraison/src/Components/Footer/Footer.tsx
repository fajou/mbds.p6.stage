import React from "react";

class Footer extends React.Component<any, any> {
    render() {
        return (
            <footer className="page-footer bg-dark">
                <div className="pt-5 pb-0 pb-md-5 border-bottom border-light bg-shop-features" id="shop-features">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3 col-sm-6 border-right border-light">
                                <div className="icon-box text-center mb-5 mb-md-0">
                                    <div className="icon-box-icon"><i data-feather="truck"/></div>
                                    <h3 className="icon-box-title font-weight-semibold text-white">Free local delivery</h3>
                                    <p className="icon-box-text">Free delivery for all orders over $100</p>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6 border-right border-light">
                                <div className="icon-box text-center mb-5 mb-md-0">
                                    <div className="icon-box-icon"><i data-feather="refresh-cw"/></div>
                                    <h3 className="icon-box-title font-weight-semibold text-white">Money back guarantee</h3>
                                    <p className="icon-box-text">Free delivery for all orders over $100</p>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6 border-right border-light">
                                <div className="icon-box text-center mb-5 mb-md-0">
                                    <div className="icon-box-icon"><i data-feather="life-buoy"/></div>
                                    <h3 className="icon-box-title font-weight-semibold text-white">24/7 customer support</h3>
                                    <p className="icon-box-text">Friendly 24/7 customer support</p>
                                </div>
                            </div>
                            <div className="col-md-3 col-sm-6">
                                <div className="icon-box text-center mb-5 mb-md-0">
                                    <div className="icon-box-icon"><i data-feather="credit-card"/></div>
                                    <h3 className="icon-box-title font-weight-semibold text-white">Secure online payment</h3>
                                    <p className="icon-box-text">We posess SSL / Secure сertificate</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="pt-5 pb-4 bg-third-row">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6 text-center text-sm-left">
                                <div className="mb-4 mb-sm-0"><a className="d-inline-block" href="index.html"><img alt="MStore" src="/img/logo-light.png" width="100"/></a>
                                    <div className="navbar-lang-switcher dropdown border-light mt-3 mb-0 mt-sm-0">
                                        <div className="dropdown-toggle text-white" data-toggle="dropdown"><img alt="English" src="/img/flags/mg.png" width="20"/><span>MGA</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-6 text-center text-sm-right">
                                <a className="social-btn sb-facebook sb-light mx-1 mb-2" href="#"><i className="flaticon-facebook"/></a>
                                <a className="social-btn sb-instagram sb-light mx-1 mb-2" href="#"><i className="flaticon-instagram"/></a>
                                <a className="social-btn sb-twitter sb-light mx-1 mb-2" href="#"><i className="flaticon-twitter"/></a>
                                <a className="social-btn sb-vimeo sb-light mx-1 mb-2" href="#"><i className="flaticon-youtube"/></a>
                            </div>
                        </div>
                        <div className="row pt-4">
                            <div className="col-sm-6 text-center text-sm-left">
                                <ul className="list-inline font-size-sm">
                                    <li className="list-inline-item mr-3">
                                        <a className="nav-link-inline nav-link-light" href="#">Outlets</a>
                                    </li>
                                    <li className="list-inline-item mr-3"><a className="nav-link-inline nav-link-light" href="#">
                                        Affiliates</a>
                                    </li>
                                    <li className="list-inline-item mr-3">
                                        <a className="nav-link-inline nav-link-light" href="#">Support</a>
                                    </li>
                                    <li className="list-inline-item mr-3">
                                        <a className="nav-link-inline nav-link-light" href="#">Privacy</a>
                                    </li>
                                    <li className="list-inline-item mr-3">
                                        <a className="nav-link-inline nav-link-light" href="#">Terms of use</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-sm-6 text-center text-sm-right">
                                <div className="d-inline-block"><img alt="Payment methods" src="/img/cards.png" width="187"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="py-3 bg-copyright">
                    <div aria-label="Copyright" className="container font-size-xs text-center">
                        <span className="text-white opacity-60 mr-1">© All rights reserved. customezing by</span>
                        <a className="nav-link-inline nav-link-light" href="#" target="_blank">Olivier - MBDS</a>
                    </div>
                </div>
            </footer>
        );
    }

}

export default Footer;