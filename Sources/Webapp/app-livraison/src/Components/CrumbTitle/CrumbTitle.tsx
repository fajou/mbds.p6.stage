import React from "react";
import "./CrumbTitle.css"

const CrumbTitle = (props: any) => {
    return (
        <div aria-label="Page title" className="page-title-wrapper">
            <div className="container">
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="mt-n1 mr-1">
                            <i data-feather="home"/>
                        </li>
                        <li className="breadcrumb-item">
                            <a href="/">Home</a>
                        </li>
                        <li className="breadcrumb-item">
                            <a href="help-topics.html">{props.item}</a>
                        </li>
                    </ol>
                </nav>
                <h1 className="page-title">{props.title}</h1>
                <span className="d-block mt-2 text-muted"/>
                <hr className="mt-4"/>
            </div>
        </div>
    );
};

export default CrumbTitle;