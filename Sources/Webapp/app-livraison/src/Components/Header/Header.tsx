import React, {Component} from "react";
import "./Header.css";

class Header extends Component {
    render() {
        return (
            <header className="navbar navbar-expand-lg navbar-light bg-light px-0 fixed-top">
                <div className="container flex-sm-nowrap px-3">
                    <a className="navbar-brand mr-0 mr-sm-4 min-width-100" href="index.html">
                        <img alt="MStore" src="/img/logo-dark.png" width="100"/>
                    </a>
                    <div className="navbar-btns d-flex position-relative order-sm-3">
                        <div className="navbar-toggler navbar-btn collapsed bg-0 border-left-0 my-3" data-target="#menu" data-toggle="collapse">
                            <i className="mx-auto mb-2" data-feather="menu"/>Menu
                        </div>
                        <a className="navbar-btn bg-0 my-3 border-left-0 navbar-collapse-hidden" href="product-comparison.html">
                            <span className="d-block position-relative">
                                <span className="navbar-btn-badge bg-secondary border">3</span>
                                <i className="mx-auto mb-1" data-feather="repeat"/>Compare
                            </span>
                        </a>
                        <a className="navbar-btn bg-0 my-3" data-toggle="offcanvas" href="#offcanvas-account">
                            <i className="mx-auto mb-1" data-feather="log-in"/>Sign In/Up
                        </a>
                        <a className="navbar-btn bg-0 my-3" data-toggle="offcanvas" href="#offcanvas-cart">
                            <span className="d-block position-relative">
                                <span className="navbar-btn-badge bg-primary text-light">4</span>
                                <i className="mx-auto mb-1" data-feather="shopping-cart"/>$325.00
                            </span>
                        </a>
                    </div>
                    <div className="flex-grow-1 pb-3 pt-sm-3 my-1 px-sm-2 pr-lg-4 order-sm-2">
                        <div className="input-group flex-nowrap">
                            <div className="input-group-prepend">
                                <span className="input-group-text rounded-left" id="search-icon">
                                    <i data-feather="search"/>
                                </span>
                            </div>
                            <input aria-describedby="search-icon" aria-label="Search site" className="form-control rounded-right" id="site-search" placeholder="Search site" type="text"/>
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;