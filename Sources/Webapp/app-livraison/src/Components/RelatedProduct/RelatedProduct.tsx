import React from "react";
import data from "../../Data/products.json";
import ProductItem from "../Items/ProductItem";

class RelatedProduct extends React.Component<any, any> {
    render() {
        return (
            <div className="container pb-5">
                <h2 className="h3 pb-4">You may also like</h2>
                <div className="owl-carousel"
                     data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;margin&quot;: 15, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;360&quot;:{&quot;items&quot;:2}, &quot;630&quot;:{&quot;items&quot;:3}, &quot;850&quot;:{&quot;items&quot;:4}, &quot;1200&quot;:{&quot;items&quot;:5}} }">
                    {data.map((item, index) => (
                        <ProductItem key={index} {...item}/>
                    ))}
                </div>
            </div>
        );
    }
}

export default RelatedProduct;