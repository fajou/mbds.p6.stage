import React from "react";
import Section from "../Section";
import data from "../../../Data/brands.json";
import "./BrandsCarousel.css";

export interface BrandsInfo {
    name: string,
    url: string,
    image: string
}

export const Brands = (info: BrandsInfo) => (
    <a className="d-block bg-white border py-4 py-sm-5 px-2 some-mr" href={info.url}>
        <img alt={info.name} className="img-width d-block mx-auto" src={info.image}/>
    </a>
);

class BrandsCarousel extends React.Component<any, any> {
    render() {
        return (
            <Section className="container px-3 pb-4">
                <div className="owl-carousel border-right"
                     data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 3500, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;360&quot;:{&quot;items&quot;:2},&quot;600&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
                    {data.map(Brands)}
                </div>
            </Section>
        );
    }
}

export default BrandsCarousel;