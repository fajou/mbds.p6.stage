import React from "react";
import Section from "../Section";
import data from "./../../../Data/product-widgets.json";

interface Item {
    name: string,
    rating: string[],
    price: string,
    promo: string,
    image: string
}

interface Column {
    title: string,
    items: Item[]
}

export const Rating = (rating: string[]) => (
    <div className="star-rating">
        {rating && rating.map((i) => <i className={i ? `sr-star ${i}` : 'sr-star'} data-feather="star"/>)}
    </div>
);

export const ProductMedia = (item: Item, index: number) => {

    const price = (<del className="text-muted mr-1">{item.promo}</del>);

    return (
        <div key={index} className="media">
            <a className="featured-entry-thumb" href="#">
                <img alt={item.name} src={item.image} width="64"/>
            </a>
            <div className="media-body">
                <h6 className="featured-entry-title">
                    <a href="#">{item.name}</a>
                </h6>
                {item.rating && item.rating.length ? Rating(item.rating) : undefined}
                <p className="featured-entry-meta">
                    {item.promo ? price : undefined} {item.price}
                </p>
            </div>
        </div>
    );
}

export const BestSellers = (col: Column, index: number) => {
    return (
        <div key={index} className="widget widget-featured-entries">
            <h3 className="widget-title font-size-lg">{col.title}</h3>
            {col.items.map(ProductMedia)}
        </div>
    );
};

class Classements extends React.Component<any, any> {
    render() {
        return (
            <Section className="container px-3 pt-2 pb-4 mb-md-2">
                <div className="row">
                    {data.map((item, index) => (
                        <div className="col-md-4 col-sm-6 mb-2 py-3">
                            {BestSellers(item, index)}
                        </div>
                    ))}
                </div>
                <hr className="mt-4 mb-5"/>
            </Section>
        );
    }
}

export default Classements;