import React from "react";
import Section from "../Section";

class PromoBanner extends React.Component<any, any> {
    render() {
        return (
            <Section className="container py-4 my-3 px-3">
                <div className="bg-faded-info position-relative py-4">
                    <div className="row align-items-center">
                        <div className="col-md-5">
                            <span className="badge badge-danger ml-5">Limited offer</span>
                            <div className="pt-4 pl-4 pl-sm-5">
                                <h3 className="font-family-body font-weight-light mb-2">All new</h3>
                                <h2 className="mb-2 pb-1">iPad Pro 2019</h2>
                                <h5 className="font-family-body font-weight-light mb-3">at discounted price. Hurry up!</h5>
                                <div className="countdown h4 mb-4" data-date-time="09/10/2020 12:00"
                                     data-labels="{&quot;label-day&quot;: &quot;d&quot;, &quot;label-hour&quot;: &quot;h&quot;, &quot;label-minute&quot;: &quot;m&quot;, &quot;label-second&quot;: &quot;s&quot;}"/>
                                <a className="btn btn-primary" href="shop-style2-ls.html">
                                    View offers<i className="ml-2" data-feather="arrow-right"/>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-7">
                            <img alt="Promo banner" className="mx-auto" src="/img/home/electronics/offer.jpg"/>
                        </div>
                    </div>
                </div>
            </Section>
        );
    }
}

export default PromoBanner;