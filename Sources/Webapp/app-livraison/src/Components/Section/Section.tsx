import React from "react";

class Section extends React.Component<any, any> {
    render() {
        return (
            <section className={this.props.className}>
                {this.props.children}
            </section>
        );
    }
}

export default Section;