import React from "react";
import Section from "../Section";
import data from "../../../Data/products.json";
import ProductCard, {Product} from "../../Items/ProductCard";
import url from "../../../url.json";

const Item = (product: Product, index: number) => {
    return (
        <div key={index} className="col-lg-3 col-sm-4 col-6 border border-collapse">
            <ProductCard {...product}/>
        </div>
    );
};



class Featured extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        const numberFormat = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'MGA' });
        fetch(url.articles)
        .then(res => res.json())
        .then((result) => {
            const data = result._embedded.articles.map((item: any, index:number) => {
                item.classifier = item.classifier.substring(1, item.classifier.length).split(",");
                item.price = numberFormat.format(item.price);
                if(item.promo > 0)
                    item.promo = numberFormat.format(item.promo);
                return item;
            })
            this.setState({
                data: data
            });
        }, (error) => {
            console.error(error);
        });
    }

    render() {
        return (
            <Section className="container px-3 pt-4 mt-3 my-3">
                <div className="d-flex flex-wrap justify-content-between align-items-center pb-2">
                    <h2 className="h3 mb-3">Featured products</h2>
                    <a className="btn btn-outline-primary btn-sm border-0 mb-3" href="shop-style2-ls.html">
                        More products
                        <i className="ml-1 mr-n2" data-feather="chevron-right"/>
                    </a>
                </div>
                {this.state.data && (
                    <div className="row no-gutters">
                        {this.state.data.map((Item))}
                    </div>
                )}
            </Section>
        );
    }
}

export default Featured;