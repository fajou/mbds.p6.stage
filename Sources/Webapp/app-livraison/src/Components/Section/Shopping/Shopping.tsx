import React from "react";
import Categories from "../../Categories/Categories";
import HeroSlider from "../../HeroSlider/HeroSlider";
import OfferSlider from "../../OfferSlider/OfferSlider";
import Section from "../Section";

class Shopping extends React.Component<any, any> {
    render() {
        return (
            <Section className="container px-3 pt-2">
                <div className="mt-4 pt-lg-2 mb-4 mb-md-5">
                    <div className="row">
                        <div className="col-lg-4 d-none d-lg-block">
                            <Categories/>
                        </div>
                        <div className="col-lg-8">
                            <HeroSlider/>
                            <OfferSlider/>
                        </div>
                    </div>
                </div>
                <hr className="mt-4"/>
            </Section>
        );
    }
}

export default Shopping;