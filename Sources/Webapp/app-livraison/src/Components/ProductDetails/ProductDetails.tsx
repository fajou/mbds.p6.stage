import React from "react";
import data from "../../Data/details.json";
import "./ProductDetails.css";
import {Rating} from "../Section/Classements/Classements";
import url from "../../url.json";

class ProductDetails extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        const numberFormat = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'MGA' });
        if(this.props.id) {
            fetch(url.articles + "/" + this.props.id)
            .then(res => res.json())
            .then((result) => {
                result.classifier = result.classifier.substring(1, result.classifier.length).split(",");
                result.price = numberFormat.format(result.price);
                    if(result.promo > 0)
                    result.promo = numberFormat.format(result.promo);
                this.setState({
                    data: result
                });
            }, (error) => {
                console.error(error);
            });
        }
    }

    render() {
        // console.log("state", this.state.data);
        const data = this.state.data;
        if(data) {
            const Gallery = (img: any, index: number) => {
                return (
                    <a key={index} data-fancybox="prod-gallery" data-hash={img.asset_id + index} href={img.secure_url}>
                        <img alt={data.name} src={img.secure_url}/>
                    </a>
                );
            };

            const Thumbnails = (thumb: any, index: number) => {
                return (
                    <li className={index === 0 ? "active" : undefined}>
                        <a href={`#${thumb.asset_id + index}`}>
                            <img alt={data.name} src={thumb.secure_url}/>
                        </a>
                    </li>
                );
            };

            const Deliver = (deliv: any, index: number) => {
                return (
                    <div key={index} className={`d-flex justify-content-between ${index + 1 !== data.deliveries.length ? "border-bottom" : undefined} pb-2`}>
                        <div>
                            <div className="font-weight-semibold text-dark">{deliv.delivery}</div>
                            <div className="font-size-sm text-muted">{deliv.duration}</div>
                        </div>
                        <div>{deliv.costs}</div>
                    </div>
                );
            }

            return (
                <div className="container pb-4">
                    <div className="row">
                        <div className="col-lg-7">
                            <div className="product-gallery">
                                <span className="badge badge-danger">Sale -30%</span>
                                <ul className="product-thumbnails">
                                    {data.galleries && data.galleries.map(Thumbnails)}
                                    <li className="video-thumbnail">
                                        <a href={`#${data.video?.asset_id}`}>
                                            <span className="thumb-caption">Video</span>
                                            <img alt="Product thumb" src="/img/shop/electronics/single/th05.jpg"/>
                                        </a>
                                    </li>
                                </ul>
                                <div className="product-carousel owl-carousel">
                                    {data.galleries && data.galleries.map(Gallery)}
                                    <div className="pt-sm-5 mt-sm-5" data-hash={data.video?.asset_id}>
                                        <div className="embed-responsive embed-responsive-16by9">
                                            <iframe allowFullScreen className="embed-responsive-item" src={data.video?.secure_url}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 pt-4 pt-lg-0">
                            <div className="pb-4">
                                <a className="d-inline-block scroll-to" href={"#reviews"}>
                                    {Rating(data.rating?.stars)}
                                    <span className="d-inline-block align-middle font-size-sm mt-1 ml-1 text-body">
                                    {data.rating?.reviews ?? 0} reviews
                                </span>
                                </a>
                                <h2 className="h1 font-weight-light pt-3 pb-2">
                                    {data.promo ? <del className="lead text-muted mr-2">{data.promo}</del> : ""}
                                    <span className="text-primary">{data.price}</span>
                                </h2>
                                <div className="d-flex align-items-center pt-2">
                                    <input className="px-2 form-control mr-2 qt-width" name="quantity" required type="number" value="1"/>
                                    <button className="btn btn-primary btn-block" data-target="#cart-toast" data-toggle="toast" type="button">
                                        <i className="mr-2" data-feather="shopping-cart"/>
                                        Add to cart
                                    </button>
                                </div>
                                <div className="d-flex flex-wrap align-items-center pt-3">
                                    <a className="btn box-shadow-0 nav-link-inline px-4 border-right" data-target="#compare-toast" data-toggle="toast" href="#">
                                        <i className="align-middle mr-1 w-h" data-feather="repeat"/>
                                        Compare
                                    </a>
                                    <a className="btn box-shadow-0 nav-link-inline px-4" data-target="#wishlist-toast" data-toggle="toast" href="#">
                                        <i className="align-middle mr-1 w-h" data-feather="heart"/>
                                        Add to wishlist
                                    </a>
                                </div>
                            </div>
                            <div className="accordion" id="productPanels">
                                <div className="card">
                                    <div className="card-header">
                                        <h3 className="accordion-heading"><a aria-controls="productInfo" aria-expanded="true" data-toggle="collapse" href={"#productInfo"} role="button">
                                        <span className="d-inline-block pr-2 border-right mr-2 align-middle mt-n1">
                                            <i data-feather="info" className="w-h"/>
                                        </span>
                                            Product
                                            information<span className="accordion-indicator"><i data-feather="chevron-up"/></span>
                                        </a>
                                        </h3>
                                    </div>
                                    <div className="collapse show" data-parent="#productPanels" id="productInfo">
                                        <div className="card-body">
                                            <ul className="mb-0">
                                                <li>Tell the whole story with the 360° camera that captures every angle in 4K brilliance and share instantly</li>
                                                <li>Capture life as it happens in stunning 4K video and 15MP photos thanks to dual 180° lenses</li>
                                                <li>
                                                    <a className="scroll-to font-weight-semibold" href={"#product-details"}>More info</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header">
                                        <h3 className="accordion-heading">
                                            <a aria-controls="shippingOptions" aria-expanded="true" className="collapsed" data-toggle="collapse" href={"#shippingOptions"} role="button">
                                            <span className="d-inline-block pr-2 border-right mr-2 align-middle mt-n1">
                                                <i data-feather="truck" className="w-h"/>
                                            </span>
                                                Shipping options
                                                <span className="accordion-indicator">
                                                <i data-feather="chevron-up"/>
                                            </span>
                                            </a>
                                        </h3>
                                    </div>
                                    <div className="collapse" data-parent="#productPanels" id="shippingOptions">
                                        <div className="card-body">
                                            {data.deliveries && data.deliveries.map(Deliver)}
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="card-header">
                                        <h3 className="accordion-heading"><a aria-controls="tagCloud" aria-expanded="true" className="collapsed" data-toggle="collapse" href="#tagCloud" role="button"><span
                                            className="d-inline-block pr-2 border-right mr-2 align-middle mt-n1"><i data-feather="tag" className="w-h"/>
                                    </span>
                                            Tag cloud
                                            <span className="accordion-indicator"><i data-feather="chevron-up"/></span></a>
                                        </h3>
                                    </div>
                                    <div className="collapse" data-parent="#productPanels" id="tagCloud">
                                        <div className="card-body">
                                            {data.tags && data.tags.map((tag:any, i:number) => (
                                                <a key={i} className="tag-link mr-2 mb-2" href="#">#{tag}</a>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return <div></div>
        }
    }
}

export default ProductDetails;