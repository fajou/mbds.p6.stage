import React from "react";
import data from "../../Data/categs.json"
import "./Categories.css";

export interface Categorie {
    name: string,
    icon: any,
    price: any,
    image: any
}


export const Item = (catg: Categorie, index: any) => {
    const borderBottom = index + 1 < data.length ? "border-bottom" : "";
    const dataContent = "Generate from Server";

    return (
        <li key={index} className={borderBottom}>
            <a className={`d-flex align-items-center nav-link-inline ${index === 0 ? "pb-3" : index + 1 === data.length ? "pt-3" : "py-3"}`}
               data-content={dataContent}
               data-html="true"
               data-placement="right"
               data-toggle="popover"
               data-trigger="hover"
               href="#">
                <i className="text-primary opacity-60 mr-2 mt-1 size-icon" data-feather={catg.icon}/>
                <span>{catg.name}<i className="ml-1 size-chevron-right" data-feather="chevron-right"/></span>
            </a>
        </li>
    );
};

class Categories extends React.Component<any, any> {
    render() {
        return (
            <div className="border px-4 py-4">
                <ul className="list-unstyled mb-0">
                    {data.map(Item)}
                </ul>
            </div>
        );
    }
}

export default Categories;