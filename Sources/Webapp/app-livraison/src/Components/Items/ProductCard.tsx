import React from "react";
import {Link} from "react-router-dom";

export interface Product {
    "name": string,
    "categorie": string,
    "price": string,
    "promo": string,
    "rating": string,
    "outstock": string,
    "reduction": string,
    "image": any,
    "classifier": string[]
}

class ProductCard extends React.Component<Product, any> {
    render() {
        const badge = <span className={`badge ${this.props.promo ? "badge-danger" : "badge-success"} rounded-0`}>{this.props.reduction}</span>;
        const price = <del className="text-muted mr-1">{this.props.promo}</del>;
        const rating = (
            <div className="star-rating">
                <span className="sr-label mr-1">{this.props.rating}</span>
                <i className="sr-star active" data-feather="star"/>
            </div>
        );

        return (
            <div className="product-card">
                <div className="product-thumb">
                    <a className="product-thumb-link" href="shop-single-electronics.html"/>
                    {this.props.reduction ? badge : undefined}
                    {/* <span className="product-wishlist-btn" data-placement="left" data-toggle="tooltip" title="Add to wishlist">
                        <i data-feather="heart"/>
                    </span> */}
                    <img alt="Product" src={this.props.image.secure_url}/>
                </div>
                <div className="product-card-body">
                    <div className="d-flex flex-wrap justify-content-between pb-1">
                        <a className="product-meta" href="#">{this.props.categorie}</a>
                        {this.props.rating ? rating : undefined}
                    </div>
                    <h3 className="product-card-title">
                        <Link to="/details/1">
                            {this.props.name}
                        </Link>
                    </h3>
                    <span className="text-primary">{this.props.promo ? price : undefined}{this.props.price}</span>
                </div>
                <div className="product-card-body body-hidden pt-2">
                    <button className="btn btn-primary btn-sm btn-block" data-target="#cart-toast" data-toggle="toast" type="button">Add to cart</button>
                    <a className="quick-view-btn" data-target="#compare-toast" data-toggle="toast" href="#">
                        <i className="mr-1" data-feather="repeat"/>
                        Compare
                    </a>
                    <ul className="font-size-sm opacity-80 pl-4 pt-2 mb-2">
                        {this.props.classifier.map((item, index) => <li key={index}>{item}</li>)}
                    </ul>
                </div>
            </div>
        );
    }
}

export default ProductCard;