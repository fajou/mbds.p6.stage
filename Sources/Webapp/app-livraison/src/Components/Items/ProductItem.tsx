import {Product} from "./ProductCard";
import React from "react";

class ProductItem extends React.Component<Product, any> {
    render() {
        const badge = <span className={`badge ${this.props.promo ? "badge-danger" : "badge-success"} rounded-0`}>{this.props.reduction}</span>;
        const price = <del className="text-muted mr-1">{this.props.promo}</del>;
        const rating = (
            <div className="star-rating">
                <span className="sr-label mr-1">{this.props.rating}</span>
                <i className="sr-star active" data-feather="star"/>
            </div>
        );
        return (
            <div className="product-card">
                <div className="product-thumb box-shadow-0">
                    <a className="product-thumb-link" href="#"/>
                    {this.props.reduction ? badge : undefined}
                    <span className="product-wishlist-btn" data-placement="left" data-toggle="tooltip" title="Add to wishlist">
                        <i data-feather="heart"/>
                    </span>
                    <img alt="Product" src={this.props.image}/>
                </div>
                <div className="product-card-body box-shadow-0">
                    <div className="d-flex flex-wrap justify-content-between pb-1">
                        <a className="product-meta" href="#">{this.props.categorie}</a>
                        {this.props.rating ? rating : undefined}
                    </div>
                    <h3 className="product-card-title">
                        <a href="shop-single-electronics.html">{this.props.name}</a>
                    </h3>
                    <span className="text-primary">{this.props.promo ? price : undefined}{this.props.price}</span>
                </div>
            </div>
        );
    }
}

export default ProductItem;