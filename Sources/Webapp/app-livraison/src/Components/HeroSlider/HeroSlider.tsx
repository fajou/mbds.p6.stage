import React from "react";
import data from "../../Data/hero-slider.json";

export interface HeroItem {
    image: string,
    title: string,
    price: string
}

export const Item = (hero: HeroItem, index: any) => {
    return (
        <div key={index} className="row align-items-center py-5">
            <div className="col-md-5">
                <div className="pl-3 pr-3 pl-md-5 pr-md-0 pt-4 pt-lg-5 pb-5 text-center text-md-left">
                    <h3 className="mb-1">{hero.title}</h3>
                    <h4 className="font-weight-light opacity-70 pb-3">{hero.price}</h4>
                    <a className="btn btn-primary" href="shop-style2-ls.html">
                        Shop now<i className="ml-2" data-feather="arrow-right"/>
                    </a>
                </div>
            </div>
            <div className="col-md-7">
                <img alt={hero.title} className="d-block mx-auto" src={hero.image}/>
            </div>
        </div>
    );
};

class HeroSlider extends React.Component<any, any> {
    render() {
        return (
            <div className="bg-secondary bg-size-cover mb-grid-gutter" style={{backgroundImage: "/img/home/electronics/hero-main-bg.jpg"}}>
                <div className="owl-carousel trigger-carousel"
                     data-owl-carousel="{&quot;nav&quot;: true, &quot;dots&quot;: false, &quot;loop&quot;: true, &quot;autoHeight&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 3000}">
                    {data.map(Item)}
                </div>
            </div>
        );
    }
}

export default HeroSlider;