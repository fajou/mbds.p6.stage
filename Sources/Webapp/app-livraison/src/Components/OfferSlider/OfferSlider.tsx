import React from "react";
import data from "../../Data/offer-slider.json";

export interface OfferItem {
    image: string,
    title: string,
    bgColor: string,
    color: string
}

export const Item = (offer: OfferItem, index: number) => {
    return (
        <a key={index} className={`media align-items-center ${offer.bgColor} text-decoration-0`} href="shop-style2-ls.html">
            <img alt="Banner" className="mr-1" src={offer.image} width="125"/>
            <div className="media-body py-2 pr-2">
                <h5 className="text-body mb-1">
                    <span className="font-weight-light">Top Rated <strong>Gadgets</strong> are on <strong>Sale</strong></span>
                </h5>
                <span className={`${offer.color} font-size-sm font-weight-semibold`}>Shop now<i className="w-h-i" data-feather="chevron-right"/></span>
            </div>
        </a>
    );
};

class OfferSlider extends React.Component
    <any, any> {
    render() {
        return (
            <div className="owl-carousel"
                 data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;565&quot;:{&quot;items&quot;:2},&quot;850&quot;:{&quot;items&quot;:3},&quot;992&quot;:{&quot;items&quot;:2},&quot;1200&quot;:{&quot;items&quot;:3}} }">
                {data.map(Item)}
            </div>
        );
    }
}

export default OfferSlider;