
export type ThemeName = 'light' | 'dark';

/*
*
* New types
* */
export interface BaseType {
    id: number,
    version: number,
    updatedDate: Date,
    createdDate: Date
}

export interface PointRelay extends BaseType {
    rayon: number;
    address: string;
    currPosition: string;
    unit: string,
}

export interface TimeInterval extends BaseType {
    timeMin: Date;
    timeMoy: Date;
    timeMax: Date;
}

export interface TypeVehicule extends BaseType {
    name: string;
    icon: string;
}

export interface Categorie extends BaseType {
    name: string;
    icon: string;
    price: string;
}

export interface Distance extends BaseType {
    distValue: number;
    distText: string;
    duraValue: number;
    duraText: string;
}

export interface Customer1 extends BaseType {
    firstName: string;
    lastName: string;
    currPosition: string;
    address: string;
    email: string;
    city: string;
    zipcode: string;
    avatar: string;
    mobileNumbers: MobileNumbers;
    distance: Distance;
}

export interface Driver extends BaseType {
    age: number;
    name: string;
    license: string;
    occuped: boolean;
    sexe: string;
    avatar: string;
    type: string;
}

export interface MobileNumbers extends BaseType {
    telma: string;
    orange: string;
    airtel: string;
    others: string;
}

export interface Carburant extends BaseType {
    carburantMax: number;
    carburantCurr: number;
    carburantType: string;
}

export type VehicleStatus = 'CREATED' | 'UPDATED' | "MAINTENANCE" | "DELIVERING" | "RESERVED" | "FREE";

export interface Vehicle extends BaseType {
    brand: any;
    status: VehicleStatus;
    weightMin: number;
    weightMax: number;
    speedMin: number;
    speedMoy: number;
    speedMax: number;
    trajetMin: number | undefined;
    trajetMax: number | undefined;
    numImm: number;
    colorVeh: string;
    descVeh: string;
    driver: Driver;
    carburant: Carburant;
    type: TypeVehicule;
    timeIntr: TimeInterval;
}

export type DeliveryStatus = 'CREATED' | 'UPDATED' | 'ADDED' | 'PREPARED' | 'PENDING' | 'DELIVERING' | 'DELIVERED';

export interface Delivery extends BaseType {
    departDate: Date;
    arrivalDate: Date;
    status: DeliveryStatus,
    duration: any;
    totalWeight: number;
    totalItems: number;
    subTotal: number;
    totalTax: number;
    grandTotal: number;
    vehicle: Vehicle;
    containers: Command[];
}

export interface Command extends BaseType {
    code: string;
    dateDelivery: Date;
    totalItems: number;
    totalWeight: number;
    subTotal: number;
    totalTax: number;
    totalDelivery: number;
    grandTotal: number;
    customer: Customer1;
    items: Item[];
    deliveries: Delivery;
    payementInfo: PayementInfo;
}

export interface PaimentMethod extends BaseType {
    libelle: string;
    code: string;
    type: string;
    purlExt: string;
}

export interface PayementInfo extends BaseType {
    total: number;
    advance: number;
    paimentDate: Date;
    method: PaimentMethod;
    container: Command;
}

export interface Item extends BaseType {
    itemName: any;
    itemCount: number;
    unitePrice: number;
    priceTotal: number;
    uniteWeight: number;
    totalWeight: number;
    article: Article;
    order: Command | {};
}

export interface MediaCloud extends BaseType {
    filepath: string;
    resource_type: string;
    original_filename: string;
    signature: string;
    format: string;
    secure_url: string;
    url: string;
    public_id: string;
    placeholder: string;
    height: number;
    width: number;
}

export interface Article extends BaseType {
    name: string;
    price: number;
    promo: number;
    prating: number;
    outstock: string;
    reduction: string;
    classifier: string;
    image: MediaCloud;
    categorie: Categorie;
}

/*
*
* */

export interface Category extends BaseType {
    name: string;
    icon: string;
    price: string;
    media: MediaCloud
}

export interface Product extends BaseType {
    category_id: number;
    description: string;
    height: number;
    image: string;
    price: number;
    reference: string;
    stock: number;
    thumbnail: string;
    width: number;
}

export interface Customer extends BaseType {
    first_name: string;
    last_name: string;
    address: string;
    city: string;
    zipcode: string;
    avatar: string;
    birthday: string;
    first_seen: string;
    last_seen: string;
    has_ordered: boolean;
    latest_purchase: string;
    has_newsletter: boolean;
    groups: string[];
    nb_commands: number;
    total_spent: number;
}

export type OrderStatus = 'ADDED' | 'DELIVERING' | 'PENDING' | 'DELIVERED' | 'CANCELLED' | 'GROUPED';

export interface Order extends BaseType {
    status: OrderStatus;
    code: string;
    returned: boolean;
    dateDelivery: Date;
    totalItems: number;
    totalWeight: number;
    subTotal: number;
    totalTax: number;
    totalDelivery: number;
    grandTotal: number;
    customer: Customer1;
    items: Item[];
    deliveries: Delivery;
    payementInfo: PayementInfo;
}

export interface BasketItem {
    product_id: number;
    quantity: number;
}
