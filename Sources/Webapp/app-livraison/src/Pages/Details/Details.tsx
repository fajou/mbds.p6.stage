import React from "react";
import Child from "../../Components/Child/Child";
import ProductDetails from "../../Components/ProductDetails/ProductDetails";
import RelatedProduct from "../../Components/RelatedProduct/RelatedProduct";
import CrumbTitle from "../../Components/CrumbTitle/CrumbTitle";

export default class Details extends React.Component<any, any> {    
    render() {
        return (
            <Child>
                <CrumbTitle item="Product" title="Product Name"/>
                <ProductDetails id={this.props.match.params.id}/>
                <RelatedProduct/>
            </Child>
        );
    }
}
