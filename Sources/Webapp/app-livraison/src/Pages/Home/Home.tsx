import React from "react";
import Child from "../../Components/Child/Child";
import BrandsCarousel from "../../Components/Section/Brands/BrandsCarousel";
import Classements from "../../Components/Section/Classements/Classements";
import PromoBanner from "../../Components/Section/PromoBanner/PromoBanner";
import Shopping from "../../Components/Section/Shopping/Shopping";
import Featured from "../../Components/Section/Featured/Featured";

class Home extends React.Component<any, any> {
    render() {
        return (
            <Child>
                <Shopping/>
                <Featured/>
                <PromoBanner/>
                <BrandsCarousel/>
                <Classements/>
            </Child>
        );
    }
}

export default Home;