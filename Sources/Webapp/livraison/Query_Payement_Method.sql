insert into PAYMENT_METHOD
values (1, sysdate, 'CREATED',  null, 0, 'M_M', 'Mobile Money', 'Operateur Local');
insert into PAYMENT_METHOD
values (2, sysdate, 'CREATED',  null, 0, 'C_C', 'Carte Credit', 'https://www.stripe.com');
insert into PAYMENT_METHOD
values (3, sysdate, 'CREATED', null,  0, 'P_P', 'PayPal', 'https://www.paypal.com');