alter sequence public.hibernate_sequence restart with 700000;

create table if not exists carburant
(
	id bigint not null
		constraint carburant_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	carburant_curr bigint not null,
	carburant_max bigint not null,
	carburant_type varchar(255)
);

alter table carburant owner to db_livraison;

create table if not exists media_cloud
(
	id bigint not null
		constraint media_cloud_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	filepath varchar(255),
	format varchar(255),
	height integer not null,
	original_filename varchar(255),
	placeholder varchar(255),
	public_id varchar(255),
	resource_type varchar(255),
	secure_url varchar(255),
	signature varchar(255),
	url varchar(255),
	width integer not null
);

alter table media_cloud owner to db_livraison;

create table if not exists categorie
(
	id bigint not null
		constraint categorie_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	icon varchar(255),
	image varchar(255),
	name varchar(255),
	price varchar(255),
	media_id bigint
		constraint fk_media_cloud
			references media_cloud
);

alter table categorie owner to db_livraison;

create table if not exists article
(
	id bigint not null
		constraint article_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	classifier varchar(255),
	name varchar(255),
	outstock varchar(255),
	price double precision not null,
	promo double precision not null,
	rating double precision not null,
	reduction varchar(255),
	weight double precision,
	categorie_id bigint
		constraint fk_categorie
			references categorie,
	image_id bigint
		constraint fk_media_cloud
			references media_cloud
);

alter table article owner to db_livraison;

create table if not exists mobile_numbers
(
	id bigint not null
		constraint mobile_numbers_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	airtel varchar(255),
	orange varchar(255),
	others varchar(255),
	telma varchar(255)
);

alter table mobile_numbers owner to db_livraison;

create table if not exists driver
(
	id bigint not null
		constraint driver_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	age integer not null,
	avatar varchar(255),
	email varchar(255),
	license varchar(255),
	name varchar(255),
	occuped boolean not null,
	sexe varchar(255),
	numbers_id bigint
		constraint fk_mobile_numbers
			references mobile_numbers
);

alter table driver owner to db_livraison;

create table if not exists payment_method
(
	id bigint not null
		constraint payment_method_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	code varchar(255),
	libelle varchar(255),
	type varchar(255),
	url_ext varchar(255)
);

alter table payment_method owner to db_livraison;

create table if not exists payment_info
(
	id bigint not null
		constraint payment_info_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	advance double precision not null,
	paiment_date date,
	total double precision not null,
	method_id bigint
		constraint fk_payment_method
			references payment_method
);

alter table payment_info owner to db_livraison;

create table if not exists type_delivery
(
	id bigint not null
		constraint type_delivery_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	couts double precision not null,
	delivery varchar(255),
	duration varchar(255)
);

alter table type_delivery owner to db_livraison;

create table if not exists type_vehicule
(
	id bigint not null
		constraint type_vehicule_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	icon varchar(255),
	name varchar(255)
);

alter table type_vehicule owner to db_livraison;

create table if not exists time_interval
(
	id bigint not null
		constraint time_interval_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version bigint not null,
	time_max double precision not null,
	time_min double precision not null,
	time_moy double precision not null
);

alter table time_interval owner to db_livraison;

create table if not exists vehicle
(
	id bigint not null
		constraint vehicle_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	brand varchar(255),
	color_veh varchar(255),
	desc_veh varchar(255),
	num_imm varchar(255),
	speed_max integer not null,
	speed_min integer not null,
	speed_moy integer not null,
	weight_max double precision not null,
	weight_min double precision not null,
	carburant_id bigint
		constraint fk_carburant
			references carburant,
	driver_id bigint
		constraint fk_driver
			references driver,
	type_id bigint
		constraint fk_type_vehicule
			references type_vehicule,
	last_delivery date,
	trajet_max integer,
	trajet_min integer,
	time_intr_id bigint
		constraint fk_time_interval
			references time_interval
);

alter table vehicle owner to db_livraison;

create table if not exists delivery
(
	id bigint not null
		constraint delivery_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	arrival_date date,
	depart_date date,
	duration bigint,
	grand_total double precision not null,
	sub_total double precision not null,
	total_items double precision not null,
	total_tax double precision not null,
	total_weight double precision not null,
	vehicle_id bigint
		constraint fk_vehicle
			references vehicle
);

alter table delivery owner to db_livraison;

create table if not exists distance
(
	id bigint not null
		constraint distance_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version bigint not null,
	dist_text varchar(255),
	dist_value bigint not null,
	dura_text varchar(255),
	dura_value bigint not null
);

alter table distance owner to db_livraison;

create table if not exists customer
(
	id bigint not null
		constraint customer_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	address varchar(255),
	avatar varchar(255),
	city varchar(255),
	curr_position varchar(255),
	email varchar(255),
	first_name varchar(255),
	last_name varchar(255),
	zipcode varchar(255),
	mobile_numbers_id bigint
		constraint fk_mobile_numbers
			references mobile_numbers,
	distance_id bigint
		constraint fk_distance
			references distance
);

alter table customer owner to db_livraison;

create table if not exists command
(
	id bigint not null
		constraint order_container_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	code varchar(255),
	date_delivery date,
	grand_total double precision not null,
	sub_total double precision not null,
	total_delivery double precision not null,
	total_items double precision not null,
	total_tax double precision not null,
	total_weight double precision not null,
	customer_id bigint
		constraint fk_customer
			references customer,
	delivery_id bigint
		constraint fk_delivery
			references delivery,
	payment_info_id bigint
		constraint fk_payment_info
			references payment_info,
	returned boolean default false,
	date_command date
);

alter table command owner to db_livraison;

create table if not exists item
(
	id bigint not null
		constraint item_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version integer not null,
	item_count bigint not null,
	price_total double precision not null,
	tax double precision not null,
	total_weight double precision not null,
	unite_price double precision not null,
	unite_weight double precision not null,
	article_id bigint
		constraint fk_article
			references article,
	order_id bigint not null
		constraint fk_command
			references command
);

alter table item owner to db_livraison;

create table if not exists point_relay
(
	id bigint not null
		constraint point_relay_pkey
			primary key,
	created_date date,
	status varchar(255),
	updated_date date,
	version bigint not null,
	address varchar(255),
	curr_position varchar(255),
	rayon integer not null
);

alter table point_relay owner to db_livraison;

alter sequence hibernate_sequence start with 600000;
