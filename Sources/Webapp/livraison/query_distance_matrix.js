function sendDataDist(data_) {
    const direction = JSON.parse(data_.currPosition);
    const origin1 = new google.maps.LatLng(-20.2569255, 57.46405899999999);
    const destinationB = new google.maps.LatLng(direction.lat, direction.lng);

    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
        {
            origins: [origin1],
            destinations: [destinationB],
            travelMode: 'DRIVING',
            drivingOptions: {
                departureTime: new Date(Date.now() + 2000),  // for the time N milliseconds from now.
                trafficModel: 'optimistic'
            },
            unitSystem: google.maps.UnitSystem.METRIC
        }, callback);

    function callback(response, status) {
        console.log(response);
        console.log(status);
        if (status === "OK") {
            console.log(response);
            const dist = response.rows[0].elements[0];
            data_.distance = {};
            data_.distance.distText = dist.distance.text;
            data_.distance.distValue = dist.distance.value;
            data_.distance.duraText = dist.duration.text;
            data_.distance.duraValue = dist.duration.value;

            const myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            const raw = JSON.stringify(data_);
            const requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("http://localhost:8081/data/saveRelaies", requestOptions)
                .then(response => response.text())
                .then(result => console.log(result))
                .catch(error => console.log('error', error));
        }
        console.log(data_);
    }
}

function req() {
    fetch("http://localhost:8081/data/relaies")
        .then(response => response.json())
        .then(result => {
            result.forEach((res) => {
                sendDataDist(res);
            });
        })
        .catch(error => console.log('error', error));
}

document.ready = req();