package com.gps.tracker.livraison;

import com.gps.tracker.livraison.repositories.CustomerRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.DeliveryItemRepository;
import com.gps.tracker.livraison.services.LivraisonService;
import com.gps.tracker.livraison.services.tasks.AutoGroupingCommands;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LivraisonApplicationTests {
    private final LivraisonService       livraisonService;
    private final CustomerRepository     customerRepository;
    private final AutoGroupingCommands   commands;
    private final DeliveryItemRepository deliveryItemRepository;

    @Autowired
    public LivraisonApplicationTests(LivraisonService livraisonService, CustomerRepository customerRepository, AutoGroupingCommands commands, DeliveryItemRepository deliveryItemRepository) {
        this.livraisonService = livraisonService;
        this.customerRepository = customerRepository;
        this.commands = commands;
        this.deliveryItemRepository = deliveryItemRepository;
    }


    @Test
    void contextLoads() {
        /*var customers = customerRepository.findAll();
        var ids       = new ArrayList<Long>();
        customers.forEach(customer -> {
            var relay = livraisonService.findPointRelayCustomer(customer);
            ids.add(relay.getId());
        });
        var rs = ids.stream().distinct().collect(Collectors.toList());
        rs.forEach(System.out::println);*/
        /*commands.work();*/
    }

}
