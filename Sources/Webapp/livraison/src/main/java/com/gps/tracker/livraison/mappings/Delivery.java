package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.time.Duration;
import java.util.Date;
import java.util.List;

@Entity
public class Delivery extends BaseEntity implements Serializable {
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date          departDate;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date          arrivalDate;
    private Duration      duration;
    private double        totalWeight;
    private double        totalItems;
    private double        subTotal;
    private double        totalTax;
    private double        grandTotal;
    @OneToOne
    private PointRelay    pointRelay;
    @OneToOne
    private Vehicle       vehicle;
    @OneToMany(mappedBy = "delivery")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Command> orders;

    public enum StatusData {
        PREPARED,
        PENDING,
        GROUPED,
        DELIVERING,
        DELIVERED
    }

    @Override
    protected void onCreate() {
        try {
            var status = status(getStatus());
            super.onCreate();
            if (isCustomStatus(status))
                setStatus(status);
            calculeTotals();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onUpdate() {
        try {
            String status = status(getStatus());
            super.onUpdate();
            if (isCustomStatus(status))
                setStatus(status);
            calculeTotals();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculeTotals() {
        if (orders == null || orders.size() == 0)
            return;
        totalItems = orders.stream().mapToDouble(Command::getTotalItems).sum();
        totalWeight = orders.stream().mapToDouble(Command::getTotalWeight).sum();
        subTotal = orders.stream().mapToDouble(Command::getSubTotal).sum();
        totalTax = orders.stream().mapToDouble(Command::getTotalTax).sum();
        grandTotal = subTotal + totalTax;

        if (orders != null) {
            for (Command container : orders) {
                container.setDelivery(this);
            }
        }
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public double getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(double totalItems) {
        this.totalItems = totalItems;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public List<Command> getOrders() {
        return orders;
    }

    public void setOrders(List<Command> orders) {
        this.orders = orders;
    }

    public PointRelay getPointRelay() {
        return pointRelay;
    }

    public void setPointRelay(PointRelay pointRelay) {
        this.pointRelay = pointRelay;
    }

    public boolean addCommand(Command command) {
        var wt = totalWeight + command.getTotalWeight();
        if (wt <= vehicle.getWeightMax()) {
            command.setStatus(Command.StatusData.GROUPED.name());
            getOrders().add(command);
        } else return false;

        if (wt < vehicle.getWeightMin())
            setStatus(StatusData.PENDING.name());
        else if (vehicle.getWeightMin() < wt && wt <= vehicle.getWeightMax()) {
            setStatus(StatusData.DELIVERED.name());
            vehicle.setStatus(Vehicle.StatusData.FREE.name());
        }

        return true;
    }

    public boolean isFull(double wt) {
        var total = wt + getTotalWeight();
        return vehicle.getWeightMax() - getTotalWeight() >= vehicle.getWeightMin() && total > vehicle.getWeightMax();
    }

    public String status(String status) throws Exception {
        if (status == null)
            throw new Exception("Status must be not null.");
        switch (status.toUpperCase()) {
            case "PREPARED":
                return Delivery.StatusData.PREPARED.toString();
            case "GROUPED":
                return StatusData.GROUPED.toString();
            case "PENDING":
                return Delivery.StatusData.PENDING.toString();
            case "DELIVERING":
                return Delivery.StatusData.DELIVERING.toString();
            case "DELIVERED":
                return Delivery.StatusData.DELIVERED.toString();
            case "CREATED":
                return BaseEntity.StatusData.CREATED.toString();
            case "UPDATED":
                return BaseEntity.StatusData.UPDATED.toString();
            case "DELETED":
                return BaseEntity.StatusData.DELETED.toString();
            case "CANCELED":
                return BaseEntity.StatusData.CANCELED.toString();
            case "PAYED":
                return BaseEntity.StatusData.PAYED.toString();
            default:
                throw new Exception("Unknown status : " + status);
        }
    }
}
