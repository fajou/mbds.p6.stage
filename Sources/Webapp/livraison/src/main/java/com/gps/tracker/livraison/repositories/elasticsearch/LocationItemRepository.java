package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.LocationItem;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface LocationItemRepository extends ElasticsearchRepository<LocationItem, String> {
    @Query("{\"bool\": {" +
            "      \"must\": {" +
            "        \"match_all\": {}" +
            "      }," +
            "      \"filter\": {" +
            "        \"geo_distance\": {" +
            "          \"distance\": \"?2\"," +
            "          \"location\": {" +
            "            \"lat\": ?0," +
            "            \"lon\": ?1" +
            "          }" +
            "        }" +
            "      }" +
            "    }" +
            " }")
    List<LocationItem> groupeByPointRelay(double a, double b, String distance);

    @Query("{\"bool\": {" +
            "      \"must\": [" +
            "        { \"match\": { \"originId\": ?3}}" +
            "      ]," +
            "      \"filter\": {" +
            "        \"geo_distance\": {" +
            "          \"distance\": \"?2\"," +
            "          \"location\": {" +
            "            \"lat\": ?0," +
            "            \"lon\": ?1" +
            "          }" +
            "        }" +
            "      }" +
            "    }" +
            " }")
    LocationItem findPointRelayCustomer(double a, double b, String distance, long customerId);
}
