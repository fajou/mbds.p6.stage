package com.gps.tracker.livraison.mappings.stores;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gps.tracker.livraison.mappings.BaseEntity;
import com.gps.tracker.livraison.mappings.Categorie;
import com.gps.tracker.livraison.mappings.media.MediaCloud;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import static javax.persistence.CascadeType.*;

@Entity
public class Article extends BaseEntity {
    private String     name;
    private double     price;
    private double     promo;
    private Double     weight;
    private double     rating;
    private String     outstock;
    private String     reduction;
    private String     classifier;
    @OneToOne(cascade = {ALL})
    private MediaCloud image;
    @JsonIgnore
    @OneToOne(cascade = {PERSIST, MERGE, DETACH, REFRESH}, fetch = FetchType.LAZY)
    private Categorie  categorie;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPromo() {
        return promo;
    }

    public void setPromo(double promo) {
        this.promo = promo;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getOutstock() {
        return outstock;
    }

    public void setOutstock(String outstock) {
        this.outstock = outstock;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public MediaCloud getImage() {
        return image;
    }

    public void setImage(MediaCloud image) {
        this.image = image;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
