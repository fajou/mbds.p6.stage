package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Delivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(exported = false)
public interface DeliveryRepository extends BaseRepository<Delivery> {
    @RestResource(path = "vehicle")
    Page<Delivery> findAllByVehicle_Id(long id, @PageableDefault Pageable pageable);

    Delivery findByVehicle_IdAndStatus(long id, String status);

    List<Delivery> findALlByVehicle_IdAndStatusIn(long id, Iterable<String> status);
}
