package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class MobileNumbers extends BaseEntity implements Serializable {
    private String telma;
    private String orange;
    private String airtel;
    private String others;

    public String getTelma() {
        return telma;
    }

    public void setTelma(String telma) {
        this.telma = telma;
    }

    public String getOrange() {
        return orange;
    }

    public void setOrange(String orange) {
        this.orange = orange;
    }

    public String getAirtel() {
        return airtel;
    }

    public void setAirtel(String airtel) {
        this.airtel = airtel;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }
}
