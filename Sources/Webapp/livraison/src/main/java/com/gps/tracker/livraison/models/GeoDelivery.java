package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.Customer;
import com.gps.tracker.livraison.mappings.PointRelay;
import com.gps.tracker.livraison.mappings.Vehicle;

import java.util.List;

public class GeoDelivery {
    private Vehicle        vehicle;
    private PointRelay     relay;
    private List<Customer> customers;

    public GeoDelivery() {
    }

    public GeoDelivery(Vehicle vehicle, PointRelay relay, List<Customer> customers) {
        this.vehicle = vehicle;
        this.relay = relay;
        this.customers = customers;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public PointRelay getRelay() {
        return relay;
    }

    public void setRelay(PointRelay relay) {
        this.relay = relay;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
