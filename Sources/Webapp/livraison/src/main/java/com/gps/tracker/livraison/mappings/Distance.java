package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Distance extends BaseEntity implements Serializable {
    private long   distValue;
    private String distText;
    private long   duraValue;
    private String duraText;

    public long getDistValue() {
        return distValue;
    }

    public void setDistValue(long distValue) {
        this.distValue = distValue;
    }

    public String getDistText() {
        return distText;
    }

    public void setDistText(String distText) {
        this.distText = distText;
    }

    public long getDuraValue() {
        return duraValue;
    }

    public void setDuraValue(long duraValue) {
        this.duraValue = duraValue;
    }

    public String getDuraText() {
        return duraText;
    }

    public void setDuraText(String duraText) {
        this.duraText = duraText;
    }

    public long kmValue() {
        return Math.abs(distValue / 1000);
    }

    public static String toDistText(long value) {
        var val = value / 1000;
        return Math.round(val) + " km";
    }

    public static String toDuraText(long value) {
        var text = "";
        var min  = value / 60;
        if (min > 59) {
            var hour = min / 60;
            min = min % 60;
            if (hour > 23) {
                var jour = hour / 24;
                hour = hour % 24;
                text += jour + "j ";
            }
            text += hour + "h ";
        }
        if (min != 0)
            text += min + "min";
        return text;
    }
}
