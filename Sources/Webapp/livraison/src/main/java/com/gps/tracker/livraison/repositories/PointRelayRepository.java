package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.PointRelay;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(
        path = "relaies",
        collectionResourceRel = "relaies",
        itemResourceRel = "relaies")
public interface PointRelayRepository extends BaseRepository<PointRelay> {
}
