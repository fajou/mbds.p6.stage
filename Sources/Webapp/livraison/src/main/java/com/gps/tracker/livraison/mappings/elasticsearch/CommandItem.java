package com.gps.tracker.livraison.mappings.elasticsearch;

import com.gps.tracker.livraison.mappings.Command;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "idx_command_item")
public class CommandItem extends BaseItem {
    @Field(type = FieldType.Auto)
    private Command command;

    public CommandItem(Command command) {
        this.command = command;
    }

    public CommandItem() {

    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
