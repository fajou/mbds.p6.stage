package com.gps.tracker.livraison.services.handlers;

import com.gps.tracker.livraison.mappings.Vehicle;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleItem;
import com.gps.tracker.livraison.repositories.VehicleRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleItemRepository;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;

@Service
@RepositoryEventHandler
public class VehicleEventHandler {
    private final VehicleItemRepository repository;
    private final VehicleRepository     vehicleRepository;

    public VehicleEventHandler(VehicleItemRepository repository, VehicleRepository vehicleRepository) {
        this.repository = repository;
        this.vehicleRepository = vehicleRepository;
    }

    @HandleAfterSave
    @HandleAfterCreate
    @HandleAfterLinkSave
    public void indexingVehicleItem(Vehicle vehicle) {
        var item = repository.findByVehicle_Id(vehicle.getId());
        if (item == null)
            item = new VehicleItem(vehicle);
        repository.save(item);
    }

    @HandleAfterDelete
    @HandleAfterLinkDelete
    public void deleteIndexingVehicleItem(Vehicle vehicle) {
        var item = repository.findByVehicle_Id(vehicle.getId());
        if (item != null)
            repository.delete(item);
    }

    @PostConstruct
    public void massIndex() {
        repository.deleteAll();
        var vehicles = vehicleRepository.findAll();
        var items    = vehicles.stream().map(VehicleItem::new).collect(Collectors.toList());
        repository.saveAll(items);
    }
}