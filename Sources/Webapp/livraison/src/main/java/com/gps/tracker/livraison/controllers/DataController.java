package com.gps.tracker.livraison.controllers;

import com.gps.tracker.livraison.mappings.*;
import com.gps.tracker.livraison.mappings.elasticsearch.GpsTraceur;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleItem;
import com.gps.tracker.livraison.models.GeoDelivery;
import com.gps.tracker.livraison.models.PointRelayInfo;
import com.gps.tracker.livraison.models.VehicleFilter;
import com.gps.tracker.livraison.repositories.*;
import com.gps.tracker.livraison.repositories.elasticsearch.*;
import com.gps.tracker.livraison.services.LivraisonService;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

@Controller
@RequestMapping("/data")
public class DataController {
    private final CategorieRepository        categorieRepository;
    private final TypeVehiculeRepository     typeVehiculeRepository;
    private final VehicleRepository          vehicleRepository;
    private final MediaCloudRepository       cloudRepository;
    private final ArticleRepository          articleRepository;
    private final CustomerRepository         customerRepository;
    private final DriverRepository           driverRepository;
    private final LivraisonService           livraisonService;
    private final CommandRepository          commandRepository;
    private final PointRelayRepository       pointRelayRepository;
    private final LocationItemRepository     locationItemRepository;
    private final ElasticsearchOperations    elasticsearchOperations;
    private final RestHighLevelClient        elasticsearchClient;
    private final VehicleItemRepository      vehicleItemRepository;
    private final DeliveryItemRepository     deliveryItemRepository;
    private final VehicleHistoItemRepository histoItemRepository;
    private final GpsTraceurRepository       traceurRepository;

    public DataController(CategorieRepository categorieRepository,
                          TypeVehiculeRepository typeVehiculeRepository,
                          VehicleRepository vehicleRepository,
                          MediaCloudRepository cloudRepository,
                          ArticleRepository articleRepository,
                          CustomerRepository customerRepository,
                          DriverRepository driverRepository,
                          LivraisonService livraisonService,
                          CommandRepository commandRepository,
                          PointRelayRepository pointRelayRepository,
                          LocationItemRepository locationItemRepository,
                          ElasticsearchOperations elasticsearchOperations,
                          RestHighLevelClient elasticsearchClient,
                          VehicleItemRepository vehicleItemRepository,
                          DeliveryItemRepository deliveryItemRepository,
                          VehicleHistoItemRepository histoItemRepository,
                          GpsTraceurRepository traceurRepository) {
        this.categorieRepository = categorieRepository;
        this.typeVehiculeRepository = typeVehiculeRepository;
        this.vehicleRepository = vehicleRepository;
        this.cloudRepository = cloudRepository;
        this.articleRepository = articleRepository;
        this.customerRepository = customerRepository;
        this.driverRepository = driverRepository;
        this.livraisonService = livraisonService;
        this.commandRepository = commandRepository;
        this.pointRelayRepository = pointRelayRepository;
        this.locationItemRepository = locationItemRepository;
        this.elasticsearchOperations = elasticsearchOperations;
        this.elasticsearchClient = elasticsearchClient;
        this.vehicleItemRepository = vehicleItemRepository;
        this.deliveryItemRepository = deliveryItemRepository;
        this.histoItemRepository = histoItemRepository;
        this.traceurRepository = traceurRepository;
    }

    @ResponseBody
    @GetMapping("/types_vehicle")
    public List<TypeVehicule> getTypeVehicles() {
        return typeVehiculeRepository.findAll();
    }

    @ResponseBody
    @GetMapping("/vehicles")
    public Slice<Vehicle> getVehicules(@RequestParam(required = false) Long type,
                                       @RequestParam(required = false) String status,
                                       @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                                       @RequestParam(required = false) Date dateMin,
                                       @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
                                       @RequestParam(required = false) Date dateMax,
                                       @RequestParam(required = false) int page,
                                       @RequestParam(required = false) int size,
                                       @RequestParam(required = false) String[] sort) {
        var filter = new VehicleFilter();
        if (type != null)
            filter.setType(type);
        filter.setStatus(status);
        filter.setDateMin(dateMin);
        filter.setDateMax(dateMax);
        Pageable pageable;
        if (sort != null && sort.length == 2)
            pageable = PageRequest.of(page, size, Sort.by(sort[1].equals("DESC") ? DESC : ASC, sort[0]));
        else pageable = PageRequest.of(page, size);

        var specs = filter.buildSpecs();
        return vehicleRepository.findAll(Specification.where(specs), pageable);
    }

    @ResponseBody
    @GetMapping("/deliveringMap/{status}")
    public List<GeoDelivery> getDeliveriesOnMap(@PathVariable String status) {
        var st         = status.toUpperCase();
        var deliveries = deliveryItemRepository.findAllByDelivery_Status(st, null);
        return deliveries.stream().map(item -> new GeoDelivery(
                item.getDelivery().getVehicle(),
                item.getDelivery().getPointRelay(),
                item.getDelivery().getOrders().stream().map(Command::getCustomer).collect(Collectors.toList())
        )).collect(Collectors.toList());
    }

    @ResponseBody
    @GetMapping("/deliveringMap/{status}/{id}")
    public GeoDelivery getDeliveriesOnMap(@PathVariable String status, @PathVariable String id) {
        var st   = status.toUpperCase();
        var item = deliveryItemRepository.findById(id);
        if (item.isEmpty() || !item.get().getDelivery().getStatus().equals(status))
            return new GeoDelivery();

        var delivery = item.get();
        return new GeoDelivery(
                delivery.getDelivery().getVehicle(),
                delivery.getDelivery().getPointRelay(),
                delivery.getDelivery().getOrders().stream().map(Command::getCustomer).collect(Collectors.toList())
        );
    }

    @ResponseBody
    @GetMapping("/drivers_free")
    public List<Driver> getDriversFree(@RequestParam(required = false) Long id) {
        var drivers = driverRepository.findAllByOccuped(false);
        if (id != null) {
            var driver = driverRepository.findById(id.longValue());
            drivers.add(driver);
        }
        return drivers;
    }

    @ResponseBody
    @GetMapping("/customers")
    public List<Customer> getCustomers() {
        return customerRepository.findAll();
    }

    @ResponseBody
    @GetMapping("/traceurs")
    public Iterable<GpsTraceur> gpsTraceur() {
        return traceurRepository.findAll();
    }

    @ResponseBody
    @PostMapping("/saveCustomers")
    public Customer saveAll(@RequestBody Customer customers) {
        return customerRepository.save(customers);
    }

    @ResponseBody
    @PostMapping("/save_drivers")
    public List<Driver> saveDrivers(@RequestBody List<Driver> drivers) {
        for (Driver driver : drivers) {
            var numbers = new MobileNumbers();
            driver.setNumbers(numbers);
        }
        driverRepository.saveAll(drivers);
        return drivers;
    }

    @ResponseBody
    @PostMapping("/savePoints")
    public List<PointRelay> savePointRelay(@RequestBody List<PointRelay> relays) {
        return pointRelayRepository.saveAll(relays);
    }

    @ResponseBody
    @GetMapping("groupingCustomer")
    public List<PointRelayInfo> groupingCustomer() {
        return livraisonService.groupingCustomersByLocation();
    }

    @ResponseBody
    @GetMapping("/relaies")
    public List<PointRelay> getPointRelaies() {
        return pointRelayRepository.findAll();
    }

    @ResponseBody
    @PostMapping("/saveRelaies")
    public PointRelay savePointRelaies(@RequestBody PointRelay relays) {
        return pointRelayRepository.save(relays);
    }

    @ResponseBody
    @GetMapping("/find_vehicle{distance}")
    public List<VehicleItem> getVehicleItem(@PathVariable Long distance) {
        return vehicleItemRepository.findAllVehicleItemDispo(distance);
    }
}
