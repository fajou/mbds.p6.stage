package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Command;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(
        path = "commands",
        collectionResourceRel = "commands",
        itemResourceRel = "command")
public interface CommandRepository extends BaseRepository<Command> {
    @RestResource(path = "status")
    Page<Command> findAllByStatus(String status, @PageableDefault Pageable pageable);

    Page<Command> findAll(Pageable pageable);

    List<Command> findAllByCustomer_IdIn(Iterable<Long> id);

    List<Command> findAllByStatus(String status);

    List<Command> findAllByDelivery_Id(Long id);
}
