package com.gps.tracker.livraison.services;

import com.gps.tracker.livraison.mappings.*;
import com.gps.tracker.livraison.mappings.elasticsearch.CommandItem;
import com.gps.tracker.livraison.mappings.elasticsearch.DeliveryItem;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleItem;
import com.gps.tracker.livraison.models.CommandInfo;
import com.gps.tracker.livraison.models.GeoLocation;
import com.gps.tracker.livraison.models.PointRelayInfo;
import com.gps.tracker.livraison.repositories.*;
import com.gps.tracker.livraison.repositories.elasticsearch.CommandItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.DeliveryItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.LocationItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleItemRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static com.gps.tracker.livraison.mappings.Delivery.StatusData.PENDING;

@Service
@Transactional
public class LivraisonService implements IBaseService {
    private final ArticleRepository      articleRepository;
    private final CustomerRepository     customerRepository;
    private final CommandRepository      commandRepository;
    private final DeliveryRepository     deliveryRepository;
    private final PointRelayRepository   pointRelayRepository;
    private final LocationItemRepository locationItemRepository;
    private final VehicleItemRepository  vehicleItemRepository;
    private final CommandItemRepository  commandItemRepository;
    private final DeliveryItemRepository deliveryItemRepository;
    private final VehicleRepository      vehicleRepository;

    private static final Random r = new Random();

    public LivraisonService(ArticleRepository articleRepository,
                            CustomerRepository customerRepository,
                            CommandRepository commandRepository,
                            DeliveryRepository deliveryRepository,
                            PointRelayRepository pointRelayRepository,
                            LocationItemRepository locationItemRepository,
                            VehicleItemRepository vehicleItemRepository,
                            CommandItemRepository commandItemRepository, DeliveryItemRepository deliveryItemRepository, VehicleRepository vehicleRepository) {
        this.articleRepository = articleRepository;
        this.customerRepository = customerRepository;
        this.commandRepository = commandRepository;
        this.deliveryRepository = deliveryRepository;
        this.pointRelayRepository = pointRelayRepository;
        this.locationItemRepository = locationItemRepository;
        this.vehicleItemRepository = vehicleItemRepository;
        this.commandItemRepository = commandItemRepository;
        this.deliveryItemRepository = deliveryItemRepository;
        this.vehicleRepository = vehicleRepository;
    }

    private String dateToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public String generateCode(int n) {
        byte[] array = new byte[256];
        new Random().nextBytes(array);
        String        randomString       = new String(array, StandardCharsets.UTF_8);
        StringBuilder r                  = new StringBuilder();
        String        alphaNumericString = randomString.replaceAll("[^A-Za-z0-9]", "");
        for (int k = 0; k < alphaNumericString.length(); k++) {
            if (Character.isLetter(alphaNumericString.charAt(k)) && (n > 0) || Character.isDigit(alphaNumericString.charAt(k)) && (n > 0)) {
                r.append(alphaNumericString.charAt(k));
                n--;
            }
        }
        return r.toString();
    }

    public List<Item> buildItems(List<Item> items) {
        for (var item : items) {
            if (item.getItemCount() < 1 || item.getArticle() == null || item.getArticle().getId() < 1)
                continue;
            var article = item.getArticle();
            item.setArticle(article);
            item.setUnitePrice(article.getPrice());
            item.setUniteWeight(article.getWeight());
            item.calculeTotals(20);
        }
        return items;
    }

    public Command buildOrder(Customer customer, List<Item> items) {
        var order = new Command();
        order.setCustomer(customer);
        items = buildItems(items);
        order.setItems(items);
        order.calculeTotals();
        order.setStatus(Command.StatusData.ADDED.name());
        return order;
    }

    public List<Command> buildOrders(List<CommandInfo> orders) {
        var containers = new ArrayList<Command>();
        for (CommandInfo info : orders) {
            var cmd = buildOrder(info.getCustomer(), info.getItems());
            cmd.setDateCommand(info.getDateCommand());
            containers.add(cmd);
        }
        return containers;
    }

    /*@Scheduled(fixedRate = 600000)*/
    public void generateCommandInfo() {
        var items     = new ArrayList<Item>();
        var articles  = articleRepository.findAll();
        var customers = customerRepository.findAll();
        var info      = new CommandInfo();
        var customer  = customers.get(r.nextInt(customers.size() - 1));

        var size = articles.size();
        var min  = r.nextInt(size - 1);
        var temp = size - min;
        var max  = r.nextInt(temp < 4 ? temp : 3) + min;

        if (max == min && 3 <= min) {
            min = min - 3;
        } else if (max == min) {
            max = min + 3;
        }

        var ars = articles.subList(min, max);
        info.setCustomerId(customer.getId());
        info.setCustomer(customer);
        for (var article : ars) {
            var item = new Item();
            item.setArticle(article);
            item.setItemCount(r.nextInt(2) + 1);
            items.add(item);
        }
        info.setItems(items);
        info.setDateCommand(new Date());
        var cmd = buildOrder(info.getCustomer(), info.getItems());
        cmd.setDateCommand(info.getDateCommand());
        commandRepository.save(cmd);
    }

    public List<CommandInfo> buildCommandInfos(List<Date> dates) {
        var customers = customerRepository.findAll();
        var articles  = articleRepository.findAll();
        var orders    = new ArrayList<CommandInfo>();
        var s         = dates.size();
        var i         = 0;
        while (i < s) {
            for (var customer : customers) {
                if (s <= i)
                    break;
                var items     = new ArrayList<Item>();
                var orderInfo = new CommandInfo();

                var size = articles.size();
                var min  = r.nextInt(size - 1);
                var max  = r.nextInt(1) + min;
                if (max == min && 1 <= min) {
                    min = min - 1;
                } else if (max == min) {
                    max = min + 1;
                }
                System.out.println(min + " - " + max);
                var ars = articles.subList(min, max);
                orderInfo.setCustomerId(customer.getId());
                orderInfo.setCustomer(customer);
                for (var article : ars) {
                    var item = new Item();
                    item.setArticle(article);
                    item.setItemCount(1);
                    items.add(item);
                }
                orderInfo.setItems(items);
                orderInfo.setDateCommand(dates.get(i));
                orders.add(orderInfo);
                i++;
            }
        }
        return orders;
    }

    @Transactional
    public List<Command> generateCodeOrder() {
        var orders = commandRepository.findAllByStatus(String.valueOf(Command.StatusData.GROUPED), null).toList();
        for (var order : orders) {
            var code = generateCode(6);
            order.setCode(code);
        }
        commandRepository.flush();
        return orders;
    }

    public List<PointRelayInfo> groupingCustomersByLocation() {
        var                  relaies = pointRelayRepository.findAll();
        List<PointRelayInfo> results = new ArrayList<>();
        for (PointRelay relay : relaies) {
            var location = GeoLocation.toGeoLocation(relay.getCurrPosition());
            if (location == null)
                continue;
            var geopoint = location.toGeoPoint();
            var items    = locationItemRepository.groupeByPointRelay(geopoint.getLat(), geopoint.getLon(), relay.rayon());
            results.add(new PointRelayInfo(relay, items));
        }
        return results;
    }

    public List<VehicleItem> findVehiculeDisponible(long distance) {
        var items = vehicleItemRepository.findAllVehicleItemDispo(distance);
        if (ObjectUtils.isEmpty(items))
            return new ArrayList<>();
        return items;
    }

    private List<Delivery> createDeliveries(PointRelay relay, List<VehicleItem> vehicles, List<Command> commands) {
        var deliveries = new ArrayList<Delivery>();
        for (var item : vehicles) {
            var vehicle  = item.getVehicle();
            var delivery = deliveryRepository.findByVehicle_IdAndStatus(vehicle.getId(), PENDING.name());
            if (delivery == null) {
                delivery = new Delivery();
                delivery.setOrders(new ArrayList<>());
                delivery.setStatus(PENDING.name());
            }

            var wt   = delivery.getTotalWeight();
            var cmds = new ArrayList<Command>();
            var gprs = commands.stream().filter(command -> !command.getStatus().equals(Command.StatusData.GROUPED.name())).collect(Collectors.toList());
            var nb   = gprs.size();
            var i    = 0;

            if (nb <= i)
                return deliveries;

            while (i < nb) {
                var cmd = gprs.get(i);
                wt += cmd.getTotalWeight();
                if (wt < vehicle.getWeightMax()) {
                    cmd.setStatus(Command.StatusData.GROUPED.name());
                    cmds.add(cmd);
                    i++;
                } else break;
            }

            if (wt < vehicle.getWeightMin())
                delivery.setStatus(PENDING.name());
            else delivery.setStatus(Delivery.StatusData.GROUPED.name());

            if (vehicle.getStatus().equals(Vehicle.StatusData.FREE.name()))
                vehicle.setStatus(Vehicle.StatusData.RESERVED.name());

            delivery.setVehicle(vehicle);
            delivery.getOrders().addAll(cmds);
            delivery.setPointRelay(relay);
            deliveries.add(delivery);
        }
        return deliveries;
    }

    @Transactional
    public List<Delivery> groupingDelivery() {
        var relayInfos = groupingCustomersByLocation();
        var deliveries = new ArrayList<Delivery>();
        for (PointRelayInfo pri : relayInfos) {
            if (!pri.hasItems())
                continue;
            List<Command> commands = commandItemRepository.findAllByCommand_Customer_IdIn(pri.customerIds())
                    .stream()
                    .map(CommandItem::getCommand)
                    .collect(Collectors.toList());
            if (ObjectUtils.isEmpty(commands))
                continue;
            var vehicles = findVehiculeDisponible(pri.getRelay().getDistance().kmValue());
            var delivs   = createDeliveries(pri.getRelay(), vehicles, commands);
            deliveries.addAll(delivs);
        }
        if (deliveries.size() == 0)
            return deliveries;
        deliveryRepository.saveAll(deliveries);
        return deliveries;
    }

    public PointRelay findPointRelayCustomer(Customer customer) {
        var relaies = pointRelayRepository.findAll();
        for (PointRelay relay : relaies) {
            var location = GeoLocation.toGeoLocation(relay.getCurrPosition());
            if (location == null)
                continue;
            var geopoint = location.toGeoPoint();
            var result = locationItemRepository
                    .findPointRelayCustomer(geopoint.getLat(), geopoint.getLon(), relay.rayon(), customer.getId());
            if (result != null)
                return relay;
        }
        return null;
    }

    @Transactional
    public void groupingCommand(Command command) {
        var pr = findPointRelayCustomer(command.getCustomer());
        command.calculeTotals();

        if (pr == null) {
            command.setStatus(PENDING.name());
            commandRepository.save(command);
            commandItemRepository.save(new CommandItem(command));
            return;
        }

        DeliveryItem item = null;
        try {
            item = deliveryItemRepository.findByDelivery_PointRelay_IdAndDelivery_StatusOrderByDelivery_CreatedDate(pr.getId(), PENDING.name());
        } catch (Exception ignored) {
            System.out.println("Delivery not found.");
        }
        Delivery delivery;
        if (item == null || item.getDelivery().isFull(command.getTotalWeight())) {
            var distMax = pr.getDistance().kmValue() + pr.rayonKm();
            var vehicle = vehicleItemRepository.findVehicleItemDispo((long) distMax);
            if (vehicle == null) {
                command.setStatus(PENDING.name());
                commandRepository.save(command);
                commandItemRepository.save(new CommandItem(command));
                return;
            }

            delivery = new Delivery();
            delivery.setOrders(new ArrayList<>());
            delivery.setStatus(PENDING.name());
            delivery.setPointRelay(pr);
            delivery.setVehicle(vehicle.getVehicle());
            var date = LocalDateTime.from(command.getDateCommand().toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime());
            delivery.setCreatedDate(date);
            item = new DeliveryItem(delivery);
        } else {
            delivery = item.getDelivery();
            var commands = commandRepository.findAllByDelivery_Id(delivery.getId());
            delivery.setOrders(commands);
        }

        if (delivery.addCommand(command)) {
            delivery.calculeTotals();
            deliveryRepository.save(delivery);
            commandRepository.save(command);
            deliveryItemRepository.save(item);
            commandItemRepository.save(new CommandItem(command));
            vehicleRepository.save(delivery.getVehicle());
            var vehicleItem = vehicleItemRepository.findByVehicle_Id(delivery.getVehicle().getId());
            vehicleItem.setVehicle(delivery.getVehicle());
            vehicleItemRepository.save(vehicleItem);
        }
    }
}
