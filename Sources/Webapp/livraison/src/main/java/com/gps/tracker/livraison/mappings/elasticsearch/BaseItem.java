package com.gps.tracker.livraison.mappings.elasticsearch;

import org.springframework.data.annotation.Id;

public class BaseItem {
    @Id
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
