package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static com.gps.tracker.livraison.mappings.BaseEntity.StatusData.*;

@MappedSuperclass
public class BaseEntity implements Serializable {
    public enum StatusData {
        CREATED,
        UPDATED,
        DELETED,
        CANCELED,
        PAYED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long          id;
    @org.springframework.data.annotation.Version
    private Long          version = 0L;
    private String        status;
    @Field(type = FieldType.Date, format = DateFormat.date_hour_minute_second_millis)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime updatedDate;
    @Field(type = FieldType.Date, format = DateFormat.date_hour_minute_second_millis)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        if (createdDate == null)
            createdDate = LocalDateTime.now();
        status = CREATED.toString();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedDate = LocalDateTime.now();
        status = UPDATED.toString();
    }

    public Long getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime date) {
        this.createdDate = date;
    }

    @JsonIgnore
    protected boolean isCustomStatus(String status) {
        return !status.equals(CREATED.toString()) && !status.equals(UPDATED.toString()) && !status.equals(DELETED.toString());
    }
}
