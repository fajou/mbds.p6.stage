package com.gps.tracker.livraison.repositories.specs;

import com.gps.tracker.livraison.mappings.Vehicle;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class VehicleSpecs {
    public static Specification<Vehicle> isAfter(String path, Date date) {
        return (Specification<Vehicle>) (root, query, builder) -> builder.lessThanOrEqualTo(root.get(path), date);
    }

    public static Specification<Vehicle> isBefore(String path, Date date) {
        return (Specification<Vehicle>) (root, query, builder) -> builder.greaterThanOrEqualTo(root.get(path), date);
    }

    public static Specification<Vehicle> eq(String path, Object value) {
        return (Specification<Vehicle>) (root, query, builder) -> builder.equal(root.get(path), value);
    }

    public static Specification<Vehicle> typId(long value) {
        return (Specification<Vehicle>) (root, query, builder) -> builder.equal(root.get("type").get("id"), value);
    }
}
