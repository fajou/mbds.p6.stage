package com.gps.tracker.livraison.mappings.elasticsearch;

import com.gps.tracker.livraison.mappings.Vehicle;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "idx_vehicle_item")
public class VehicleItem extends BaseItem {
    @Field(type = FieldType.Auto)
    private Vehicle vehicle;

    public VehicleItem() {
    }

    public VehicleItem(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }
}
