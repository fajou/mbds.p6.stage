package com.gps.tracker.livraison.services.handlers;

import com.gps.tracker.livraison.mappings.Command;
import com.gps.tracker.livraison.mappings.elasticsearch.CommandItem;
import com.gps.tracker.livraison.repositories.CommandRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.CommandItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleHistoItemRepository;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;

@Service
@RepositoryEventHandler
public class CommandEventHandler {
    private final CommandRepository          commandRepository;
    private final CommandItemRepository      repository;
    private final VehicleHistoItemRepository itemRepository;

    public CommandEventHandler(CommandRepository commandRepository, CommandItemRepository repository, VehicleHistoItemRepository itemRepository) {
        this.commandRepository = commandRepository;
        this.repository = repository;
        this.itemRepository = itemRepository;
    }

    @HandleAfterSave
    @HandleAfterCreate
    @HandleAfterLinkSave
    public void indexingVehicleItem(Command command) {
        var item = repository.findByCommand_Id(command.getId());
        if (item == null)
            item = new CommandItem(command);
        repository.save(item);
        System.out.println("save command item");
    }

    @HandleAfterDelete
    @HandleAfterLinkDelete
    public void deleteIndexingVehicleItem(Command command) {
        var item = repository.findByCommand_Id(command.getId());
        if (item != null)
            repository.delete(item);
        System.out.println("delete command item");
    }

    @PostConstruct
    public void massIndex() {
        System.out.println("Idx CMD");
        repository.deleteAll();
        var commands = commandRepository.findAll();
        var items    = commands.stream().map(CommandItem::new).collect(Collectors.toList());
        repository.saveAll(items);
    }
}
