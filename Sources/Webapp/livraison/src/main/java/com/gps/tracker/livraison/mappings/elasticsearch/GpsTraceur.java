package com.gps.tracker.livraison.mappings.elasticsearch;

import com.gps.tracker.livraison.models.GeoLocation;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "idx_gps_traceur")
public class GpsTraceur extends BaseItem {
    private long        vehicleId;
    private long        deliveryId;
    private GeoLocation location;

    public long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }
}
