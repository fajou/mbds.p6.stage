package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Categorie;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "*")
public interface CategorieRepository extends BaseRepository<Categorie> {
    Categorie findCategorieByName(String name);
}
