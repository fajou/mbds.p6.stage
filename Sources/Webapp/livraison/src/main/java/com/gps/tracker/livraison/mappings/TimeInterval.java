package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class TimeInterval extends BaseEntity implements Serializable {
    private double timeMin;
    private double timeMoy;
    private double timeMax;

    public double getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(double timeMin) {
        this.timeMin = timeMin;
    }

    public double getTimeMoy() {
        return timeMoy;
    }

    public void setTimeMoy(double timeMoy) {
        this.timeMoy = timeMoy;
    }

    public double getTimeMax() {
        return timeMax;
    }

    public void setTimeMax(double timeMax) {
        this.timeMax = timeMax;
    }
}
