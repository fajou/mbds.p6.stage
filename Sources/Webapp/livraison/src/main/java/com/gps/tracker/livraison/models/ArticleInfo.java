package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.stores.Article;

import java.util.List;

public class ArticleInfo {
    private String       name;
    private String       price;
    private String       promo;
    private String       rating;
    private String       outstock;
    private String       reduction;
    private String       image;
    private String       categorie;
    private List<String> classifier;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPromo() {
        return promo;
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getOutstock() {
        return outstock;
    }

    public void setOutstock(String outstock) {
        this.outstock = outstock;
    }

    public String getReduction() {
        return reduction;
    }

    public void setReduction(String reduction) {
        this.reduction = reduction;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public List<String> getClassifier() {
        return classifier;
    }

    public void setClassifier(List<String> classifier) {
        this.classifier = classifier;
    }

    public Article getArticle() {
        var article = new Article();
        article.setName(name);
        article.setOutstock(outstock);
        article.setPrice(Double.parseDouble(price) * 40);
        article.setRating(Double.parseDouble(rating));
        article.setPromo(Double.parseDouble(promo));
        article.setReduction(reduction);
        article.setClassifier("[" + String.join(",", classifier) + "]");
        return article;
    }
}
