package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.GpsTraceur;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GpsTraceurRepository extends ElasticsearchRepository<GpsTraceur, String> {

}
