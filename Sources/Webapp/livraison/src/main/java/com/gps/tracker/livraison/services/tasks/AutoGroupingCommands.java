package com.gps.tracker.livraison.services.tasks;

import com.gps.tracker.livraison.mappings.Command;
import com.gps.tracker.livraison.repositories.CommandRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.CommandItemRepository;
import com.gps.tracker.livraison.services.LivraisonService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

@Service
public class AutoGroupingCommands {
    private final LivraisonService      livraisonService;
    private final CommandRepository     commandRepository;
    private final CommandItemRepository commandItemRepository;

    public AutoGroupingCommands(LivraisonService livraisonService, CommandRepository commandRepository, CommandItemRepository commandItemRepository) {
        this.livraisonService = livraisonService;
        this.commandRepository = commandRepository;
        this.commandItemRepository = commandItemRepository;
    }

    private List<Date> load(String path) {
        List<Date> records = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(path))) {
            while (scanner.hasNextLine()) {
                var  format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date   = format.parse(scanner.nextLine());
                records.add(date);
            }
        } catch (FileNotFoundException | ParseException e) {
            e.printStackTrace();
        }
        return records;
    }

    private void gCmd() {
        var dates     = load("D:\\Etudes\\M2\\MBDS\\STAGE\\mbds.p6.stage\\Sources\\Webapp\\livraison\\src\\main\\resources\\1599765889.csv").subList(11500, 40000);
        var commandes = livraisonService.buildCommandInfos(dates);
        var cmds      = livraisonService.buildOrders(commandes);
        for (Command cmd : cmds) {
            livraisonService.groupingCommand(cmd);
        }
    }

    @Scheduled(fixedRate = 3000000)
    public void work() {
        /*var deliveries = livraisonService.groupingDelivery();*/
        /*gCmd();*/
    }
}