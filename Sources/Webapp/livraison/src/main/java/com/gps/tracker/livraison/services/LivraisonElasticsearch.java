package com.gps.tracker.livraison.services;

import com.gps.tracker.livraison.mappings.Delivery;
import com.gps.tracker.livraison.mappings.elasticsearch.DeliveryItem;
import com.gps.tracker.livraison.models.SatatsAnnuelle;
import com.gps.tracker.livraison.models.VehicleDeliveryInfo;
import com.gps.tracker.livraison.repositories.elasticsearch.DeliveryItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleHistoItemRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class LivraisonElasticsearch {
    private final DeliveryItemRepository     deliveryItemRepository;
    private final VehicleHistoItemRepository histoItemRepository;

    public LivraisonElasticsearch(DeliveryItemRepository deliveryItemRepository, VehicleHistoItemRepository histoItemRepository) {
        this.deliveryItemRepository = deliveryItemRepository;
        this.histoItemRepository = histoItemRepository;
    }

    public SatatsAnnuelle statsAnnuelle(int year) {
        LocalDate decembre = LocalDate.of(year, Month.DECEMBER, 31);
        for (int month = 1; month <= 12; month++) {
            LocalDate local      = LocalDate.of(year, month, 1);
            var       date       = Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant());
            long      deliveries = deliveryItemRepository.countAllByDelivery_DepartDateAndDelivery_Status(date, Delivery.StatusData.DELIVERED.name());
            System.out.println(deliveries);
        }
        return null;
    }

    public VehicleDeliveryInfo getDeliveryInfo(String id) {
        var item = deliveryItemRepository.findById(id);
        if (item.isEmpty())
            return new VehicleDeliveryInfo();
        var vehicle      = item.get().getDelivery().getVehicle();
        var deliveryInfo = new VehicleDeliveryInfo();
        var delivery     = item.get().getDelivery();
        var histoItem    = delivery != null ? histoItemRepository.findByVehicleIdAndDeliveryId(vehicle.getId(), delivery.getId()) : null;
        deliveryInfo.setDelivery(item.get().getDelivery());
        deliveryInfo.setHistoItem(histoItem);
        deliveryInfo.setId(id);
        deliveryInfo.setVehicle(vehicle);
        return deliveryInfo;
    }

    public List<VehicleDeliveryInfo> getVehicleDeliveryInfos(ArrayList<VehicleDeliveryInfo> deliveries, List<DeliveryItem> delivs) {
        delivs.forEach(item -> {
            var vehicle      = item.getDelivery().getVehicle();
            var deliveryInfo = new VehicleDeliveryInfo();
            var delivery     = item.getDelivery();
            var histoItem    = delivery != null ? histoItemRepository.findByVehicleIdAndDeliveryId(vehicle.getId(), delivery.getId()) : null;
            deliveryInfo.setId(item.getId());
            deliveryInfo.setDelivery(item.getDelivery());
            deliveryInfo.setHistoItem(histoItem);
            deliveryInfo.setVehicle(vehicle);
            deliveries.add(deliveryInfo);
        });
        return deliveries;
    }
}
