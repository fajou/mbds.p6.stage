package com.gps.tracker.livraison.repositories.namings;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class NamingStrategieHibernate implements PhysicalNamingStrategy {

    private Identifier convertToSnakeCase(final Identifier identifier, JdbcEnvironment jdbcEnv, boolean isCatalog) {
        final String regex       = "([a-z])([A-Z])";
        final String replacement = "$1_$2";
        String       text;
        Identifier   catalog     = jdbcEnv.getCurrentCatalog();
        if (catalog != null && isCatalog) {
            text = catalog.getText().replaceAll(regex, replacement).toUpperCase();
            return Identifier.toIdentifier(text);
        }

        if (identifier != null) {
            text = identifier.getText().replaceAll(regex, replacement).toUpperCase();
            return Identifier.toIdentifier(text);
        }
        return null;
    }

    @Override
    public Identifier toPhysicalCatalogName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeCase(identifier, jdbcEnv, true);
    }

    @Override
    public Identifier toPhysicalColumnName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeCase(identifier, jdbcEnv, false);
    }

    @Override
    public Identifier toPhysicalSchemaName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeCase(identifier, jdbcEnv, false);
    }

    @Override
    public Identifier toPhysicalSequenceName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeCase(identifier, jdbcEnv, false);
    }

    @Override
    public Identifier toPhysicalTableName(final Identifier identifier, final JdbcEnvironment jdbcEnv) {
        return convertToSnakeCase(identifier, jdbcEnv, false);
    }
}
