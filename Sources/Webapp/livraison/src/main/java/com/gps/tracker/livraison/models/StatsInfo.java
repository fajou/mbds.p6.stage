package com.gps.tracker.livraison.models;

public class StatsInfo {
    private long nbArticle;
    private long nbCommand;
    private long nbDelivery;

    public long getNbArticle() {
        return nbArticle;
    }

    public void setNbArticle(long nbArticle) {
        this.nbArticle = nbArticle;
    }

    public long getNbCommand() {
        return nbCommand;
    }

    public void setNbCommand(long nbCommand) {
        this.nbCommand = nbCommand;
    }

    public long getNbDelivery() {
        return nbDelivery;
    }

    public void setNbDelivery(long nbDelivery) {
        this.nbDelivery = nbDelivery;
    }
}
