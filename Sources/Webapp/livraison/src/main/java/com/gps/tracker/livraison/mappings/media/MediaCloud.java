package com.gps.tracker.livraison.mappings.media;


import com.gps.tracker.livraison.mappings.BaseEntity;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class MediaCloud extends BaseEntity implements Serializable {
    private String filepath;
    private String resource_type;
    private String original_filename;
    private String signature;
    private String format;
    private String secure_url;
    private String url;
    private String public_id;
    private String placeholder;
    private int    height;
    private int    width;

    public String getSignature() {
        return signature;
    }

    public String getFormat() {
        return format;
    }

    public String getSecure_url() {
        return secure_url;
    }

    public String getUrl() {
        return url;
    }

    public String getPublic_id() {
        return public_id;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getResource_type() {
        return resource_type;
    }

    public String getOriginal_filename() {
        return original_filename;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }
}
