package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Date;

@Entity
public class PaymentInfo extends BaseEntity implements Serializable {
    private double         total;
    private double         advance;
    private Date           paimentDate;
    @OneToOne
    private PaymentMethod method;
    @JsonIgnore
    @OneToOne(mappedBy = "paymentInfo")
    private Command       command;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAdvance() {
        return advance;
    }

    public void setAdvance(double advance) {
        this.advance = advance;
    }

    public Date getPaimentDate() {
        return paimentDate;
    }

    public void setPaimentDate(Date paimentDate) {
        this.paimentDate = paimentDate;
    }

    public PaymentMethod getMethod() {
        return method;
    }

    public void setMethod(PaymentMethod method) {
        this.method = method;
    }

    public Command getOrderContainer() {
        return command;
    }

    public void setOrderContainer(Command command) {
        this.command = command;
    }
}
