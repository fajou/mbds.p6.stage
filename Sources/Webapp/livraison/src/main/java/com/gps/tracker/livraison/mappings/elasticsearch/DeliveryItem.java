package com.gps.tracker.livraison.mappings.elasticsearch;

import com.gps.tracker.livraison.mappings.Delivery;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "idx_delivery_item")
public class DeliveryItem extends BaseItem {
    @Field
    private Delivery delivery;

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public DeliveryItem(Delivery delivery) {
        this.delivery = delivery;
    }

    public DeliveryItem() {
    }
}
