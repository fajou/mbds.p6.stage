package com.gps.tracker.livraison.mappings;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class PointRelay extends BaseEntity implements Serializable {
    private double   rayon;
    private String   address;
    private String   unit;
    private String   currPosition;
    @OneToOne(cascade = CascadeType.ALL)
    private Distance distance;

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCurrPosition() {
        return currPosition;
    }

    public void setCurrPosition(String currPosition) {
        this.currPosition = currPosition;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public String rayon() {
        return rayon + unit;
    }

    public double rayonKm() {
        if (unit.equals("m"))
            return Math.abs(rayon / 1000);
        return rayon;
    }
}
