package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.stores.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "*")
/*@RepositoryRestResource(exported = false)*/
public interface ArticleRepository extends BaseRepository<Article> {
    Page<Article> findAllByCategorie_Id(long id, Pageable pageable);
}
