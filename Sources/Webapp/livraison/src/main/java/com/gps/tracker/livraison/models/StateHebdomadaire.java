package com.gps.tracker.livraison.models;

public class StateHebdomadaire {
    private StatsInfo dimanche;
    private StatsInfo lundi;
    private StatsInfo mardi;
    private StatsInfo mercredi;
    private StatsInfo jeudi;
    private StatsInfo vendredi;
    private StatsInfo samedi;

    public StatsInfo getDimanche() {
        return dimanche;
    }

    public void setDimanche(StatsInfo dimanche) {
        this.dimanche = dimanche;
    }

    public StatsInfo getLundi() {
        return lundi;
    }

    public void setLundi(StatsInfo lundi) {
        this.lundi = lundi;
    }

    public StatsInfo getMardi() {
        return mardi;
    }

    public void setMardi(StatsInfo mardi) {
        this.mardi = mardi;
    }

    public StatsInfo getMercredi() {
        return mercredi;
    }

    public void setMercredi(StatsInfo mercredi) {
        this.mercredi = mercredi;
    }

    public StatsInfo getJeudi() {
        return jeudi;
    }

    public void setJeudi(StatsInfo jeudi) {
        this.jeudi = jeudi;
    }

    public StatsInfo getVendredi() {
        return vendredi;
    }

    public void setVendredi(StatsInfo vendredi) {
        this.vendredi = vendredi;
    }

    public StatsInfo getSamedi() {
        return samedi;
    }

    public void setSamedi(StatsInfo samedi) {
        this.samedi = samedi;
    }
}
