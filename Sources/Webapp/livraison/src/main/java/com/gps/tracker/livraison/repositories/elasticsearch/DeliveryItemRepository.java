package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.DeliveryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(collectionResourceRel = "deliveryItems", itemResourceRel = "delivery")
public interface DeliveryItemRepository extends ElasticsearchRepository<DeliveryItem, String> {
    DeliveryItem findByDelivery_Id(Long id);

    DeliveryItem findByDelivery_PointRelay_IdAndDelivery_StatusOrderByDelivery_CreatedDate(Long id, String status);

    Page<DeliveryItem> findAllByDelivery_Status(String status, Pageable pageable);

    DeliveryItem findByIdAndDelivery_Status(String id, String status);

    List<DeliveryItem> findByDeliveryStatusOrDeliveryStatusOrDeliveryStatus(String s1, String s2, String s3);

    List<DeliveryItem> findByDeliveryStatus(String statut);

    long countAllByDelivery_DepartDateAndDelivery_Status(Date departDate, String status);
}