package com.gps.tracker.livraison;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = {
        "com.gps.tracker.livraison.controllers",
        "com.gps.tracker.livraison.repositories",
        "com.gps.tracker.livraison.services"
})
@EnableScheduling
public class LivraisonApplication {
    public static void main(String[] args) {
        SpringApplication.run(LivraisonApplication.class, args);
    }
}
