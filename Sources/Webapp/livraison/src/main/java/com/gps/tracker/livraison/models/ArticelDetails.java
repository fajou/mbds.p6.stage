package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.TypeDelivery;
import com.gps.tracker.livraison.mappings.media.MediaCloud;
import com.gps.tracker.livraison.mappings.stores.Article;

import java.util.List;

public class ArticelDetails {
    private Article            article;
    private RnR                rating;
    private MediaCloud         video;
    private List<String>       classifier;
    private List<String>       tags;
    private List<MediaCloud>   galleries;
    private List<TypeDelivery> deliveries;
}
