package com.gps.tracker.livraison.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.Singleton;
import com.cloudinary.Uploader;
import com.cloudinary.utils.ObjectUtils;
import com.google.gson.Gson;
import com.gps.tracker.livraison.mappings.media.MediaCloud;
import com.gps.tracker.livraison.repositories.MediaCloudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;


@Service
@Transactional
public class UploadService implements IBaseService {
    private final MediaCloudRepository cloudRepository;
    private final Gson                 gson = new Gson();

    public UploadService(MediaCloudRepository cloudRepository) {
        this.cloudRepository = cloudRepository;
        Singleton.registerCloudinary(new Cloudinary("cloudinary://763683466599636:xfcnJd6RPqLUXWIG5essg-9oYGw@dm4m7evkz"));
    }

    private Map options(String filename) {
        String publicId = String.format("data_store/images/%s", filename);
        return ObjectUtils.asMap("public_id", publicId, "overwrite", true, "resource_type", "image");
    }

    private MediaCloud uploadFile(File file) throws IOException {
        Uploader   uploader = Singleton.getCloudinary().uploader();
        Map        options  = options(UUID.randomUUID().toString().replaceAll("-", ""));
        Map        map      = uploader.upload(file, options);
        String     result   = gson.toJson(map);
        MediaCloud cloud    = gson.fromJson(result, MediaCloud.class);
        cloud.setFilepath(file.getPath());
        return cloud;
    }

    @Transactional
    public void uploadImgs(String basePath) throws IOException {
        File   dir   = new File(basePath);
        File[] files = dir.listFiles();
        if (files == null)
            return;

        for (File file : files) {
            if (file.isFile()) {
                MediaCloud cloud = uploadFile(file);
                cloudRepository.save(cloud);
                System.out.println(file.getPath());
            } else if (file.isDirectory()) {
                uploadImgs(file.getPath());
            }
        }
        System.out.println("Terminer");
    }
}
