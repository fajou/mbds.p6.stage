package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.CascadeType.ALL;


@Entity
public class Vehicle extends BaseEntity implements Serializable {
    private String       brand;
    private double       weightMin;
    private double       weightMax;
    private int          speedMin;
    private int          speedMoy;
    private int          speedMax;
    private Integer      trajetMin;
    private Integer      trajetMax;
    private String       numImm;
    private String       colorVeh;
    private String       descVeh;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date         lastDelivery;
    @OneToOne(cascade = ALL)
    private Driver       driver;
    @OneToOne(cascade = ALL)
    private Carburant    carburant;
    @OneToOne(cascade = ALL)
    private TypeVehicule type;
    @OneToOne(cascade = ALL)
    private TimeInterval timeIntr;

    public enum StatusData {
        MAINTENANCE,
        DELIVERING,
        RESERVED,
        PENDING,
        FREE
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        setStatus(StatusData.FREE.toString());
    }

    @Override
    protected void onUpdate() {
        var str = getStatus();
        super.onUpdate();
        if (isCustomStatus(str)) {
            setStatus(str);
        }
        if (driver != null) driver.setOccuped(true);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getWeightMin() {
        return weightMin;
    }

    public void setWeightMin(double weightMin) {
        this.weightMin = weightMin;
    }

    public double getWeightMax() {
        return weightMax;
    }

    public void setWeightMax(double weightMax) {
        this.weightMax = weightMax;
    }

    public int getSpeedMin() {
        return speedMin;
    }

    public void setSpeedMin(int speedMin) {
        this.speedMin = speedMin;
    }

    public int getSpeedMoy() {
        return speedMoy;
    }

    public void setSpeedMoy(int speedMoy) {
        this.speedMoy = speedMoy;
    }

    public int getSpeedMax() {
        return speedMax;
    }

    public void setSpeedMax(int speedMax) {
        this.speedMax = speedMax;
    }

    public String getNumImm() {
        return numImm;
    }

    public void setNumImm(String numImm) {
        this.numImm = numImm;
    }

    public String getColorVeh() {
        return colorVeh;
    }

    public void setColorVeh(String colorVeh) {
        this.colorVeh = colorVeh;
    }

    public String getDescVeh() {
        return descVeh;
    }

    public void setDescVeh(String descVeh) {
        this.descVeh = descVeh;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Carburant getCarburant() {
        return carburant;
    }

    public void setCarburant(Carburant carburant) {
        this.carburant = carburant;
    }

    public TypeVehicule getType() {
        return type;
    }

    public void setType(TypeVehicule type) {
        this.type = type;
    }

    public Date getLastDelivery() {
        return lastDelivery;
    }

    public void setLastDelivery(Date lastDelivery) {
        this.lastDelivery = lastDelivery;
    }

    public TimeInterval getTimeIntr() {
        return timeIntr;
    }

    public void setTimeIntr(TimeInterval timeIntr) {
        this.timeIntr = timeIntr;
    }

    public Integer getTrajetMin() {
        return trajetMin;
    }

    public void setTrajetMin(Integer trajetMin) {
        this.trajetMin = trajetMin;
    }

    public Integer getTrajetMax() {
        return trajetMax;
    }

    public void setTrajetMax(Integer trajetmax) {
        this.trajetMax = trajetmax;
    }
}
