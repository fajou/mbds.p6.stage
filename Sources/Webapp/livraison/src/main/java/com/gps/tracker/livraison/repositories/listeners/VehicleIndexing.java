package com.gps.tracker.livraison.repositories.listeners;

import com.gps.tracker.livraison.mappings.Vehicle;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleItem;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleItemRepository;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

public class VehicleIndexing {
    private VehicleItemRepository repository;

    public VehicleIndexing() {

    }

    private void indexing(Vehicle vehicle) {
        var item = repository.findByVehicle_Id(vehicle.getId());
        if (item == null)
            item = new VehicleItem(vehicle);
        else item.setVehicle(vehicle);
        repository.save(item);
    }

    private void setRepository(VehicleItemRepository repository) {
        this.repository = repository;
    }

    @PostPersist
    @PostUpdate
    private void afterAnyUpdate(Vehicle vehicle) {
        indexing(vehicle);
    }

    @PostRemove
    private void afterRemove(Vehicle vehicle) {
        repository.deleteVehicleItemByVehicle_Id(vehicle.getId());
    }
}
