package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class TypeVehicule extends BaseEntity implements Serializable {
    private String name;
    private String icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
