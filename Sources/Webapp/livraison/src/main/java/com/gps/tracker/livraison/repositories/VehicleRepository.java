package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "*")
public interface VehicleRepository extends BaseRepository<Vehicle> {
    Page<Vehicle> findAll(Specification<Vehicle> filering, @PageableDefault Pageable pageable);
}
