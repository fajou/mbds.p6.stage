package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class PaymentMethod extends BaseEntity implements Serializable {
    private String libelle;
    private String code;
    private String type;
    private String urlExt;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrlExt() {
        return urlExt;
    }

    public void setUrlExt(String urlExt) {
        this.urlExt = urlExt;
    }
}
