package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.CommandItem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface CommandItemRepository extends ElasticsearchRepository<CommandItem, String> {
    CommandItem findByCommand_Id(Long id);

    List<CommandItem> findAllByCommand_Customer_IdIn(Iterable<Long> ids);

    void deleteByCommand_Id(Long id);

}
