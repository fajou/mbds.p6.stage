package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Driver;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(exported = false)
public interface DriverRepository extends BaseRepository<Driver> {
    Driver findById(long id);

    List<Driver> findAllByOccuped(boolean occuped);
}
