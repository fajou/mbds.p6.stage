package com.gps.tracker.livraison.repositories.config;

import com.gps.tracker.livraison.mappings.BaseEntity;
import com.gps.tracker.livraison.mappings.elasticsearch.BaseItem;
import org.reflections.Reflections;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class SpringDataRestCustomization implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        var                              baseEntities = new Reflections(BaseEntity.class.getPackageName());
        var                              baseItems    = new Reflections(BaseItem.class.getPackageName());
        Set<Class<? extends BaseEntity>> entities     = baseEntities.getSubTypesOf(BaseEntity.class);
        Set<Class<? extends BaseItem>>   items        = baseItems.getSubTypesOf(BaseItem.class);
        config.exposeIdsFor(entities.toArray(Class[]::new));
        config.exposeIdsFor(items.toArray(Class[]::new));
    }
}