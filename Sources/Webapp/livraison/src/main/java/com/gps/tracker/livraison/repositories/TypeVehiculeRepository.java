package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.TypeVehicule;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(exported = false)
public interface TypeVehiculeRepository extends BaseRepository<TypeVehicule> {

}
