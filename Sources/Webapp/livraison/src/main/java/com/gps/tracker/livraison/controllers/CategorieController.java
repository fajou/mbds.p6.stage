package com.gps.tracker.livraison.controllers;

import com.gps.tracker.livraison.mappings.Categorie;
import com.gps.tracker.livraison.repositories.CategorieRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categorie")
@CrossOrigin(origins = "*")
public class CategorieController {
    private final CategorieRepository repository;

    public CategorieController(CategorieRepository repository) {
        this.repository = repository;
    }

    @ResponseBody
    @PostMapping("/save-all")
    public List<Categorie> saveAll(@RequestBody List<Categorie> categories) {
        return repository.saveAll(categories);
    }
}
