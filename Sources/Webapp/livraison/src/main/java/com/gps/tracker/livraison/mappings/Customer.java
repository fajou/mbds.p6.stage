package com.gps.tracker.livraison.mappings;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class Customer extends BaseEntity implements Serializable {
    private String        firstName;
    private String        lastName;
    private String        currPosition;
    private String        address;
    private String        email;
    private String        city;
    private String        zipcode;
    private String        avatar;
    @OneToOne(cascade = CascadeType.ALL)
    private MobileNumbers mobileNumbers;
    @OneToOne(cascade = CascadeType.ALL)
    private Distance      distance;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCurrPosition() {
        return currPosition;
    }

    public void setCurrPosition(String currPosition) {
        this.currPosition = currPosition;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public MobileNumbers getMobileNumbers() {
        return mobileNumbers;
    }

    public void setMobileNumbers(MobileNumbers mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }
}
