package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class TypeDelivery extends BaseEntity implements Serializable {
    private String delivery;
    private String duration;
    private double couts;

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getCouts() {
        return couts;
    }

    public void setCouts(double couts) {
        this.couts = couts;
    }
}
