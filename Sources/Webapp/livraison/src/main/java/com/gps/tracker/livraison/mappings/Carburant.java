package com.gps.tracker.livraison.mappings;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Carburant extends BaseEntity implements Serializable {
    private long   carburantMax;
    private long   carburantCurr;
    private String carburantType;

    public long getCarburantMax() {
        return carburantMax;
    }

    public void setCarburantMax(long carburantMax) {
        this.carburantMax = carburantMax;
    }

    public long getCarburantCurr() {
        return carburantCurr;
    }

    public void setCarburantCurr(long carburantCurr) {
        this.carburantCurr = carburantCurr;
    }

    public String getCarburantType() {
        return carburantType;
    }

    public void setCarburantType(String carburantType) {
        this.carburantType = carburantType;
    }
}
