package com.gps.tracker.livraison.controllers;

import com.gps.tracker.livraison.mappings.elasticsearch.GpsTraceur;
import com.gps.tracker.livraison.repositories.elasticsearch.GpsTraceurRepository;
import com.gps.tracker.livraison.services.sockets.Greeting;
import com.gps.tracker.livraison.services.sockets.HelloMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.util.HtmlUtils;

@Controller
@CrossOrigin(origins = "*")
public class GpsController {
    private final GpsTraceurRepository traceurRepository;

    public GpsController(GpsTraceurRepository traceurRepository) {
        this.traceurRepository = traceurRepository;
    }

    @MessageMapping("/hello")
    @SendTo("/gps/greeting")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

    @MessageMapping("/traceur")
    @SendTo("/gps/receives")
    public GpsTraceur gpsTraceur(GpsTraceur traceur) {
        traceurRepository.save(traceur);
        return traceur;
    }
}