package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.Delivery;
import com.gps.tracker.livraison.mappings.Vehicle;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleHistoItem;

public class VehicleDeliveryInfo {
    private String           id;
    private VehicleHistoItem histoItem;
    private Delivery         delivery;
    private Vehicle          vehicle;

    public VehicleHistoItem getHistoItem() {
        return histoItem;
    }

    public void setHistoItem(VehicleHistoItem histoItem) {
        this.histoItem = histoItem;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VehicleDeliveryInfo() {
    }
}
