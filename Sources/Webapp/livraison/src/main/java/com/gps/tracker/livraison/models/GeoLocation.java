package com.gps.tracker.livraison.models;

import com.google.gson.Gson;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.util.StringUtils;

public class GeoLocation {
    private double lat;
    private double lng;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public static GeoLocation toGeoLocation(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }
        return new Gson().fromJson(str, GeoLocation.class);
    }

    public GeoPoint toGeoPoint() {
        return new GeoPoint(lat, lng);
    }
}
