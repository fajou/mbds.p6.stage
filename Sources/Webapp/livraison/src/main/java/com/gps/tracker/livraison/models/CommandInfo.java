package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.Customer;
import com.gps.tracker.livraison.mappings.Item;

import java.util.Date;
import java.util.List;

public class CommandInfo {
    private long       customerId;
    private Date       dateCommand;
    private Customer   customer;
    private List<Item> items;

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDateCommand() {
        return dateCommand;
    }

    public void setDateCommand(Date dateCommand) {
        this.dateCommand = dateCommand;
    }
}
