package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.Transient;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static javax.persistence.CascadeType.*;

@Entity
public class Command extends BaseEntity implements Serializable {
    private String      code;
    @Column(columnDefinition = "boolean default false")
    private boolean     returned;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date        dateDelivery;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date        dateCommand;
    private double      totalItems;
    private double      totalWeight;
    private double      subTotal;
    private double      totalTax;
    private double      totalDelivery;
    private double      grandTotal;
    @OneToOne
    private Customer    customer;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "order", cascade = ALL)
    private List<Item>  items;
    @JsonIgnore
    @ManyToOne()
    @Transient
    private Delivery    delivery;
    @OneToOne(cascade = ALL)
    private PaymentInfo paymentInfo;

    public enum StatusData {
        ADDED,
        CANCELLED,
        PREPARED,
        DELIVERED,
        GROUPED
    }

    public Command() {
    }

    public void calculeTotals() {
        totalItems = items.stream().mapToDouble(Item::getItemCount)
                .sum();
        totalWeight = items.stream().mapToDouble(Item::getTotalWeight)
                .sum();
        subTotal = items.stream().mapToDouble(Item::getPriceTotal)
                .sum();
        totalTax = items.stream().mapToDouble(Item::getTax)
                .sum();
        grandTotal = subTotal + totalTax;
        if (items != null) {
            for (Item item : items) {
                item.setOrder(this);
            }
        }
    }

    @Override
    protected void onCreate() {
        try {
            var status = status(getStatus());
            super.onCreate();
            if (isCustomStatus(status))
                setStatus(status);
            calculeTotals();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onUpdate() {
        try {
            String status = status(getStatus());
            super.onUpdate();
            if (isCustomStatus(status))
                setStatus(status);
            calculeTotals();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery deliveries) {
        this.delivery = deliveries;
    }

    public PaymentInfo getPayementInfo() {
        return paymentInfo;
    }

    public void setPayementInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public List<Item> getItems() {
        return items;
    }

    public double getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(double totalItems) {
        this.totalItems = totalItems;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public double getTotalDelivery() {
        return totalDelivery;
    }

    public void setTotalDelivery(double totalDelivery) {
        this.totalDelivery = totalDelivery;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Date getDateCommand() {
        return dateCommand;
    }

    public void setDateCommand(Date dateCommand) {
        this.dateCommand = dateCommand;
    }

    public String status(String status) {
        if (status == null)
            return BaseEntity.StatusData.CREATED.toString();
        switch (status.toUpperCase()) {
            case "CANCELLED":
                return StatusData.CANCELLED.toString();
            case "ADDED":
                return StatusData.ADDED.toString();
            case "PREPARED":
                return StatusData.PREPARED.toString();
            case "DELIVERED":
                return StatusData.DELIVERED.toString();
            case "GROUPED":
                return StatusData.GROUPED.toString();
            case "UPDATED":
                return BaseEntity.StatusData.UPDATED.toString();
            case "DELETED":
                return BaseEntity.StatusData.DELETED.toString();
            case "CANCELED":
                return BaseEntity.StatusData.CANCELED.toString();
            case "PAYED":
                return BaseEntity.StatusData.PAYED.toString();
            default:
                return BaseEntity.StatusData.CREATED.toString();
        }
    }
}
