package com.gps.tracker.livraison.mappings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gps.tracker.livraison.mappings.stores.Article;
import org.springframework.data.annotation.Transient;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class Item extends BaseEntity implements Serializable {
    private String  articleRef;
    private long    itemCount;
    private double  unitePrice;
    private double  priceTotal;
    private double  uniteWeight;
    private double  totalWeight;
    private double  tax;
    @OneToOne
    private Article article;
    @JsonIgnore
    @ManyToOne(optional = false)
    @Transient
    private Command order;

    public long getItemCount() {
        return itemCount;
    }

    public void setItemCount(long itemCount) {
        this.itemCount = itemCount;
    }

    public double getUnitePrice() {
        return unitePrice;
    }

    public void setUnitePrice(double unitePrice) {
        this.unitePrice = unitePrice;
    }

    public double getPriceTotal() {
        return priceTotal;
    }

    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    public double getUniteWeight() {
        return uniteWeight;
    }

    public void setUniteWeight(double uniteWeight) {
        this.uniteWeight = uniteWeight;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Command getOrder() {
        return order;
    }

    public void setOrder(Command containers) {
        this.order = containers;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public void calculeTotals(int tva) {
        totalWeight = uniteWeight * itemCount;
        priceTotal = unitePrice * itemCount;
        tax = tva * priceTotal / 100;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        calculeTotals(20);
        setArticleRef("ARTC" + article.getId());
    }

    @Override
    protected void onUpdate() {
        super.onUpdate();
        calculeTotals(20);
        setArticleRef("ARTC" + article.getId());
    }

    public String getArticleRef() {
        return articleRef;
    }

    public void setArticleRef(String articleRef) {
        this.articleRef = articleRef;
    }
}
