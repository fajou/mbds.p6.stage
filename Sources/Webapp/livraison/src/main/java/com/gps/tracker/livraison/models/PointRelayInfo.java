package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.PointRelay;
import com.gps.tracker.livraison.mappings.Vehicle;
import com.gps.tracker.livraison.mappings.elasticsearch.LocationItem;

import java.util.ArrayList;
import java.util.List;

public class PointRelayInfo {
    private PointRelay         relay;
    private List<LocationItem> items;
    private Vehicle            vehicle;

    public PointRelayInfo() {
    }

    public PointRelayInfo(PointRelay relay, List<LocationItem> items) {
        this.relay = relay;
        this.items = items;
    }

    public PointRelay getRelay() {
        return relay;
    }

    public void setRelay(PointRelay relay) {
        this.relay = relay;
    }

    public List<LocationItem> getItems() {
        return items;
    }

    public void setItems(List<LocationItem> items) {
        this.items = items;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public boolean hasVehicle() {
        return vehicle != null;
    }

    public Iterable<Long> customerIds() {
        var iterations = new ArrayList<Long>();
        if (items == null)
            return iterations;
        for (var item : items)
            iterations.add(item.getOriginId());
        return iterations;
    }

    public boolean hasItems() {
        return items != null && items.size() > 0;
    }
}
