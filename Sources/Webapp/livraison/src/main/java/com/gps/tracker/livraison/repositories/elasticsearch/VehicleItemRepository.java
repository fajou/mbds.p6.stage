package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.VehicleItem;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin(origins = "*")
@RepositoryRestResource(
        path = "idx_vehicle",
        collectionResourceRel = "items",
        itemResourceRel = "item")
public interface VehicleItemRepository extends ElasticsearchRepository<VehicleItem, String> {
    @Query("{\"bool\": {" +
            "   \"filter\": [" +
            "        {\"range\": { \"vehicle.trajetMin\": {\"lte\": ?0 }}}," +
            "        {\"range\": { \"vehicle.trajetMax\": {\"gte\": ?0 }}}" +
            "      ]" +
            "   }" +
            "}")
    List<VehicleItem> findAllVehicleItemDispo(long distance);

    @Query("{\"bool\": {" +
            "      \"must\": [" +
            "        { \"match\": { \"vehicle.status\": \"FREE\"}}" +
            "      ]," +
            "   \"filter\": [" +
            "        {\"range\": { \"vehicle.trajetMin\": {\"lte\": ?0 }}}," +
            "        {\"range\": { \"vehicle.trajetMax\": {\"gte\": ?0 }}}" +
            "      ]" +
            "   }" +
            "}")
    VehicleItem findVehicleItemDispo(long distance);

    VehicleItem findByVehicle_Id(Long id);

    void deleteVehicleItemByVehicle_Id(Long id);


}
