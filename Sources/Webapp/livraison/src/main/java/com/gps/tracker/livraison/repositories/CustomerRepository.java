package com.gps.tracker.livraison.repositories;

import com.gps.tracker.livraison.mappings.Customer;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(exported = false)
public interface CustomerRepository extends BaseRepository<Customer> {

    List<Customer> findByEmail(String name);
}