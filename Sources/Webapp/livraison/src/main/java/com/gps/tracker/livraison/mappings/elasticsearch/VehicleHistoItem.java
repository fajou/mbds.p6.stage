package com.gps.tracker.livraison.mappings.elasticsearch;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gps.tracker.livraison.mappings.Distance;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.GeoPointField;

import java.io.Serializable;
import java.util.Date;

@Document(indexName = "idx_vehicle_histo_item")
public class VehicleHistoItem extends BaseItem implements Serializable {
    private Long     vehicleId;
    private Long     deliveryId;
    private boolean  back;
    private Distance estimDuration;
    @GeoPointField
    private GeoPoint localisation;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date     estimDepartDate;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date     estimArrivalDate;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date     departDate;
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date     arrivalDate;

    public VehicleHistoItem() {
    }

    public VehicleHistoItem(Long deliveryId,
                            Long vehicleId,
                            boolean back,
                            Distance estimDuration,
                            GeoPoint localisation,
                            Date estimDepartDate,
                            Date estimArrivalDate,
                            Date departDate,
                            Date arrivalDate) {

        this.vehicleId = vehicleId;
        this.deliveryId = deliveryId;
        this.back = back;
        this.estimDuration = estimDuration;
        this.estimDepartDate = estimDepartDate;
        this.localisation = localisation;
        this.estimArrivalDate = estimArrivalDate;
        this.departDate = departDate;
        this.arrivalDate = arrivalDate;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public boolean isBack() {
        return back;
    }

    public void setBack(boolean back) {
        this.back = back;
    }

    public Distance getEstimDuration() {
        return estimDuration;
    }

    public void setEstimDuration(Distance estimDuration) {
        this.estimDuration = estimDuration;
    }

    public GeoPoint getLocalisation() {
        return localisation;
    }

    public void setLocalisation(GeoPoint localisation) {
        this.localisation = localisation;
    }

    public Date getEstimArrivalDate() {
        return estimArrivalDate;
    }

    public void setEstimArrivalDate(Date estimArrivalDate) {
        this.estimArrivalDate = estimArrivalDate;
    }

    public Date getDepartDate() {
        return departDate;
    }

    public void setDepartDate(Date departDate) {
        this.departDate = departDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getEstimDepartDate() {
        return estimDepartDate;
    }

    public void setEstimDepartDate(Date estimDepartDate) {
        this.estimDepartDate = estimDepartDate;
    }
}
