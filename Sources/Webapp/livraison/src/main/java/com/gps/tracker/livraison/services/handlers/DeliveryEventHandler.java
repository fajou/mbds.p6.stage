package com.gps.tracker.livraison.services.handlers;

import com.gps.tracker.livraison.mappings.Delivery;
import com.gps.tracker.livraison.mappings.Distance;
import com.gps.tracker.livraison.mappings.elasticsearch.DeliveryItem;
import com.gps.tracker.livraison.mappings.elasticsearch.VehicleHistoItem;
import com.gps.tracker.livraison.repositories.DeliveryRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.DeliveryItemRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.VehicleHistoItemRepository;
import org.elasticsearch.common.geo.GeoPoint;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.stream.Collectors;

@Service
@RepositoryEventHandler
public class DeliveryEventHandler {
    private final DeliveryRepository         deliveryRepository;
    private final DeliveryItemRepository     repository;
    private final VehicleHistoItemRepository itemRepository;

    public DeliveryEventHandler(DeliveryRepository deliveryRepository, DeliveryItemRepository repository, VehicleHistoItemRepository itemRepository) {
        this.deliveryRepository = deliveryRepository;
        this.repository = repository;
        this.itemRepository = itemRepository;
    }

    @HandleAfterSave
    @HandleAfterCreate
    @HandleAfterLinkSave
    public void indexingVehicleItem(Delivery vehicle) {
        var item = repository.findByDelivery_Id(vehicle.getId());
        if (item == null)
            item = new DeliveryItem(vehicle);
        repository.save(item);
    }

    @HandleAfterDelete
    @HandleAfterLinkDelete
    public void deleteIndexingVehicleItem(Delivery vehicle) {
        var item = repository.findByDelivery_Id(vehicle.getId());
        if (item != null)
            repository.delete(item);
    }

    @PostConstruct
    public void massIndex() {
        repository.deleteAll();
        /*var dels       = repository.findAll();*/
        var deliveries = deliveryRepository.findAll();
        var histos = deliveries.stream().map(delivery -> {
            var distance = new Distance();
            var dists    = delivery.getOrders().stream().mapToLong(command -> command.getCustomer().getDistance().getDistValue()).sum();
            var duras    = delivery.getOrders().stream().mapToLong(command -> command.getCustomer().getDistance().getDuraValue()).sum();
            distance.setDistValue(dists);
            distance.setDuraValue(duras);
            distance.setDuraText(Distance.toDuraText(duras));
            distance.setDistText(Distance.toDistText(dists));

            LocalDateTime arival = null;
            LocalDateTime depart;
            if (!ObjectUtils.isEmpty(delivery.getDepartDate())) {
                var date = LocalDateTime.from(delivery.getDepartDate().toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDateTime());
                arival = date.plusSeconds(duras);
                depart = arival.plusMinutes((long) delivery.getVehicle().getTimeIntr().getTimeMax());
            } else {
                depart = LocalDateTime.now().plusMinutes((long) delivery.getVehicle().getTimeIntr().getTimeMax());
            }

            return new VehicleHistoItem(
                    delivery.getId(),
                    delivery.getVehicle().getId(),
                    false,
                    distance,
                    new GeoPoint(20, 30),
                    Date.from(depart.atZone(ZoneId.systemDefault()).toInstant()),
                    arival != null ? Date.from(arival.atZone(ZoneId.systemDefault()).toInstant()) : null,
                    delivery.getDepartDate(),
                    delivery.getArrivalDate()
            );

        }).collect(Collectors.toList());
        itemRepository.deleteAll();
        itemRepository.saveAll(histos);
        /*dels.forEach(de -> deliveryRepository.save(de.getDelivery()));*/

        var items = deliveries.stream().map(DeliveryItem::new).collect(Collectors.toList());
        repository.saveAll(items);
    }
}
