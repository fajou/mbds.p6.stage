package com.gps.tracker.livraison.repositories.elasticsearch;

import com.gps.tracker.livraison.mappings.elasticsearch.VehicleHistoItem;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
@CrossOrigin(origins = "*")
@RepositoryRestResource(
        path = "vehicleHistos",
        collectionResourceRel = "items",
        itemResourceRel = "item")
public interface VehicleHistoItemRepository extends ElasticsearchRepository<VehicleHistoItem, String> {
    @RestResource(path = "/last")
    List<VehicleHistoItem> findAllByVehicleIdAndDeliveryId(long vehicleId, long deliveryId);

    VehicleHistoItem findByVehicleIdAndDeliveryId(Long vehicleId, Long deliveryId);
}
