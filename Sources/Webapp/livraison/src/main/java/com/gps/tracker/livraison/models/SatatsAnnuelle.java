package com.gps.tracker.livraison.models;

public class SatatsAnnuelle {
    private StatsInfo janvier;
    private StatsInfo fevrier;
    private StatsInfo mars;
    private StatsInfo avril;
    private StatsInfo mais;
    private StatsInfo juin;
    private StatsInfo juillet;
    private StatsInfo aout;
    private StatsInfo septembre;
    private StatsInfo octobre;
    private StatsInfo novembre;
    private StatsInfo decembre;

    public StatsInfo getJanvier() {
        return janvier;
    }

    public void setJanvier(StatsInfo janvier) {
        this.janvier = janvier;
    }

    public StatsInfo getFevrier() {
        return fevrier;
    }

    public void setFevrier(StatsInfo fevrier) {
        this.fevrier = fevrier;
    }

    public StatsInfo getMars() {
        return mars;
    }

    public void setMars(StatsInfo mars) {
        this.mars = mars;
    }

    public StatsInfo getAvril() {
        return avril;
    }

    public void setAvril(StatsInfo avril) {
        this.avril = avril;
    }

    public StatsInfo getMais() {
        return mais;
    }

    public void setMais(StatsInfo mais) {
        this.mais = mais;
    }

    public StatsInfo getJuin() {
        return juin;
    }

    public void setJuin(StatsInfo juin) {
        this.juin = juin;
    }

    public StatsInfo getJuillet() {
        return juillet;
    }

    public void setJuillet(StatsInfo juillet) {
        this.juillet = juillet;
    }

    public StatsInfo getAout() {
        return aout;
    }

    public void setAout(StatsInfo aout) {
        this.aout = aout;
    }

    public StatsInfo getSeptembre() {
        return septembre;
    }

    public void setSeptembre(StatsInfo septembre) {
        this.septembre = septembre;
    }

    public StatsInfo getOctobre() {
        return octobre;
    }

    public void setOctobre(StatsInfo octobre) {
        this.octobre = octobre;
    }

    public StatsInfo getNovembre() {
        return novembre;
    }

    public void setNovembre(StatsInfo novembre) {
        this.novembre = novembre;
    }

    public StatsInfo getDecembre() {
        return decembre;
    }

    public void setDecembre(StatsInfo decembre) {
        this.decembre = decembre;
    }
}
