package com.gps.tracker.livraison.services.handlers;

import com.gps.tracker.livraison.mappings.Customer;
import com.gps.tracker.livraison.mappings.elasticsearch.LocationItem;
import com.gps.tracker.livraison.models.GeoLocation;
import com.gps.tracker.livraison.repositories.CustomerRepository;
import com.gps.tracker.livraison.repositories.elasticsearch.LocationItemRepository;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Service
@RepositoryEventHandler
public class CustomerEventHandler {
    private final CustomerRepository     customerRepository;
    private final LocationItemRepository locationItemRepository;

    public CustomerEventHandler(CustomerRepository customerRepository, LocationItemRepository locationItemRepository) {
        this.customerRepository = customerRepository;
        this.locationItemRepository = locationItemRepository;
    }

    @PostConstruct
    public void massIndex() {
        var customers = customerRepository.findAll();
        var geos      = new ArrayList<LocationItem>();
        for (Customer customer : customers) {
            var geoLocation = GeoLocation.toGeoLocation(customer.getCurrPosition());
            if (geoLocation == null)
                continue;
            var location = new LocationItem();
            location.setLocation(geoLocation.toGeoPoint());
            location.setOrigin(customer.getClass().getSimpleName());
            location.setOriginId(customer.getId());
            geos.add(location);
        }

        locationItemRepository.deleteAll();
        locationItemRepository.saveAll(geos);
    }
}
