package com.gps.tracker.livraison.controllers;

import com.gps.tracker.livraison.mappings.*;
import com.gps.tracker.livraison.mappings.media.MediaCloud;
import com.gps.tracker.livraison.mappings.stores.Article;
import com.gps.tracker.livraison.models.ArticleInfo;
import com.gps.tracker.livraison.models.SatatsAnnuelle;
import com.gps.tracker.livraison.models.VehicleDeliveryInfo;
import com.gps.tracker.livraison.repositories.*;
import com.gps.tracker.livraison.repositories.elasticsearch.DeliveryItemRepository;
import com.gps.tracker.livraison.services.LivraisonElasticsearch;
import com.gps.tracker.livraison.services.LivraisonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static com.gps.tracker.livraison.mappings.Delivery.StatusData.*;

@Controller
@RequestMapping("/livraison")
public class LivraisonController {
    private final CategorieRepository    categorieRepository;
    private final TypeVehiculeRepository typeVehiculeRepository;
    private final VehicleRepository      vehicleRepository;
    private final MediaCloudRepository   cloudRepository;
    private final ArticleRepository      articleRepository;
    private final CommandRepository      commandRepository;
    private final DeliveryRepository     deliveryRepository;
    private final LivraisonService       livraisonService;
    private final DeliveryItemRepository deliveryItemRepository;
    private final CustomerRepository     customerRepository;
    private final LivraisonElasticsearch elasticsearch;

    public LivraisonController(CategorieRepository categorieRepository,
                               TypeVehiculeRepository typeVehiculeRepository,
                               VehicleRepository vehicleRepository,
                               MediaCloudRepository cloudRepository,
                               ArticleRepository articleRepository,
                               CommandRepository commandRepository,
                               DeliveryRepository deliveryRepository,
                               LivraisonService livraisonService,
                               DeliveryItemRepository deliveryItemRepository,
                               CustomerRepository customerRepository, LivraisonElasticsearch elasticsearch) {
        this.categorieRepository = categorieRepository;
        this.typeVehiculeRepository = typeVehiculeRepository;
        this.vehicleRepository = vehicleRepository;
        this.cloudRepository = cloudRepository;
        this.articleRepository = articleRepository;
        this.commandRepository = commandRepository;
        this.deliveryRepository = deliveryRepository;
        this.livraisonService = livraisonService;
        this.deliveryItemRepository = deliveryItemRepository;
        this.customerRepository = customerRepository;
        this.elasticsearch = elasticsearch;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/api";
    }

    @PostMapping("/post-categs")
    @ResponseBody
    public List<Categorie> categs(@RequestBody List<Categorie> categories) {
        return categorieRepository.saveAll(categories);
    }

    @PostMapping("/post-types-vehicle")
    @ResponseBody
    public List<TypeVehicule> saveTypes(@RequestBody List<TypeVehicule> categories) {
        return typeVehiculeRepository.saveAll(categories);
    }

    @PostMapping("/post-vehicle")
    @ResponseBody
    public List<Vehicle> saveVehicle(@RequestBody List<Vehicle> categories) {
        return vehicleRepository.saveAll(categories);
    }

    @PostMapping("/post-article")
    @ResponseBody
    public List<Article> saveArticle(@RequestBody List<ArticleInfo> articleInfos) {
        List<Article> articles = new ArrayList<>();
        for (ArticleInfo info : articleInfos) {
            Article    article = info.getArticle();
            MediaCloud cloud   = cloudRepository.findByFilepath(info.getImage());
            article.setImage(cloud);
            Categorie categorie = categorieRepository.findCategorieByName(info.getCategorie());
            article.setCategorie(categorie);
            articles.add(article);
        }
        articleRepository.saveAll(articles);
        return articles;
    }

    @PostMapping("/post-items/{customerId}")
    @ResponseBody
    public Command getOrderContainer(@PathVariable long customerId, @RequestBody List<Item> items) {
        var customer = customerRepository.findById(customerId);
        return livraisonService.buildOrder(customer, items);
    }

    @PostMapping("/post-order")
    @ResponseBody
    public Command saveContainer(@RequestBody Command command) {
        commandRepository.save(command);
        return command;
    }

    @GetMapping("/get-containers/{status}")
    @ResponseBody
    public List<Command> getContainers(@PathVariable String status) {
        return commandRepository.findAllByStatus(status.toUpperCase());
    }

    @GetMapping("/set-order/{id}/{status}")
    @ResponseBody
    public ResponseEntity<Object> setContainer(@PathVariable long id, @PathVariable String status) {
        try {
            Command command = commandRepository.findById(id);
            command.setStatus(command.status(status));
            commandRepository.save(command);
            return new ResponseEntity<>(command, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/set-delivery/{id}/{status}")
    @ResponseBody
    public ResponseEntity<Object> setDelivery(@PathVariable long id, @PathVariable String status) {
        try {
            Delivery delivery = deliveryRepository.findById(id);
            delivery.setStatus(delivery.status(status));
            deliveryRepository.save(delivery);
            return new ResponseEntity<>(delivery, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/generate-code-orders")
    @ResponseBody
    public List<Command> orderContainers() {
        return livraisonService.generateCodeOrder();
    }

    @ResponseBody
    @GetMapping("/vehicleDeliveryInfo")
    public List<VehicleDeliveryInfo> getVehicleDeliveryInfo() {
        var deliveries = new ArrayList<VehicleDeliveryInfo>();
        var delivs     = deliveryItemRepository.findByDeliveryStatusOrDeliveryStatusOrDeliveryStatus(DELIVERING.name(), PENDING.name(), GROUPED.name());
        return elasticsearch.getVehicleDeliveryInfos(deliveries, delivs);
    }

    @ResponseBody
    @GetMapping("/delivering")
    public List<VehicleDeliveryInfo> getDelivering() {
        var deliveries = new ArrayList<VehicleDeliveryInfo>();
        var delivs     = deliveryItemRepository.findByDeliveryStatus(DELIVERING.name());
        return elasticsearch.getVehicleDeliveryInfos(deliveries, delivs);
    }

    @ResponseBody
    @GetMapping("/pedding")
    public List<VehicleDeliveryInfo> getPedding() {
        var deliveries = new ArrayList<VehicleDeliveryInfo>();
        var delivs     = deliveryItemRepository.findByDeliveryStatus(PENDING.name());
        return elasticsearch.getVehicleDeliveryInfos(deliveries, delivs);
    }

    @ResponseBody
    @GetMapping("/getDeliveryInfo/{id}")
    public VehicleDeliveryInfo getDeliveryInfo(@PathVariable String id) {
        return elasticsearch.getDeliveryInfo(id);
    }

    @ResponseBody
    @GetMapping("/getStatsAnnuelle/{year}")
    public SatatsAnnuelle getStatsAnnuelle(@PathVariable int year) {
        return elasticsearch.statsAnnuelle(year);
    }

    @ResponseBody
    @GetMapping("/getStatsHebdomadaire/{debut}/{fin}")
    public SatatsAnnuelle getStateHebdomadaire(@PathVariable String debut, @PathVariable String fin) {
        return null;
    }
}
