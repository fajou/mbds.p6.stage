package com.gps.tracker.livraison.models;

import com.gps.tracker.livraison.mappings.Vehicle;
import com.gps.tracker.livraison.repositories.specs.VehicleSpecs;
import org.springframework.data.jpa.domain.Specification;
import org.thymeleaf.util.StringUtils;

import java.util.Date;

public class VehicleFilter {
    private long   type;
    private String status;
    private Date   dateMin;
    private Date   dateMax;

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateMin() {
        return dateMin;
    }

    public void setDateMin(Date dateMin) {
        this.dateMin = dateMin;
    }

    public Date getDateMax() {
        return dateMax;
    }

    public void setDateMax(Date dateMax) {
        this.dateMax = dateMax;
    }

    public Specification<Vehicle> buildSpecs() {
        Specification<Vehicle> specs = null;
        if (0 < type) specs = VehicleSpecs.typId(type);

        if (!StringUtils.isEmpty(status)) {
            var spec = VehicleSpecs.eq("status", status);
            if (specs != null)
                specs.and(spec);
            else specs = spec;
        }

        if (dateMin != null) {
            var spec = VehicleSpecs.isAfter("lastDelivery", dateMin);
            if (specs != null)
                specs.and(spec);
            else specs = spec;
        }

        if (dateMax != null) {
            var spec = VehicleSpecs.isBefore("lastDelivery", dateMax);
            if (specs != null)
                specs.and(spec);
            else specs = spec;
        }

        return specs;
    }
}
