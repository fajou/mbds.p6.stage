package com.gps.tracker.livraison.mappings;

import com.gps.tracker.livraison.mappings.media.MediaCloud;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class Categorie extends BaseEntity implements Serializable {
    private String name;
    private String icon;
    private String price;
    private String image;
    @OneToOne
    private MediaCloud media;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public MediaCloud getMedia() {
        return media;
    }

    public void setMedia(MediaCloud media) {
        this.media = media;
    }
}
