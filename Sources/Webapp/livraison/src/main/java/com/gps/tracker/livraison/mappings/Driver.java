package com.gps.tracker.livraison.mappings;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Entity
public class Driver extends BaseEntity implements Serializable {
    private int           age;
    private String        name;
    private String        license;
    private boolean       occuped;
    private String        sexe;
    private String        email;
    private String        avatar;
    @OneToOne(cascade = CascadeType.ALL)
    private MobileNumbers numbers;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public boolean isOccuped() {
        return occuped;
    }

    public void setOccuped(boolean occuped) {
        this.occuped = occuped;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String type) {
        this.email = type;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public MobileNumbers getNumbers() {
        return numbers;
    }

    public void setNumbers(MobileNumbers numbers) {
        this.numbers = numbers;
    }
}
