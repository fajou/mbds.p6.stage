import * as React from 'react';
import {useCallback, useEffect, useState} from "react";
import {initMap} from "./map";
import "./Gps.css";
import {useVersion} from "react-admin";
import {apiUrlData} from "../dataProvider/ConstUrl";
import SocketGps from './SocketGps'

const Gps = (props: any) => {
    interface State {
        geo: any;
    }

    const version = useVersion()
    const [state, setState] = useState<State>({geo: undefined});

    const fetchTypes = useCallback(async () => {
        fetch(apiUrlData + `/deliveringMap/GROUPED/${props.id}`)
            .then(res => res.json())
            .then((result) => {
                console.log(result);
                setState(state => ({
                    ...state,
                    geo: result,
                }));
            })
    }, [state]);

    useEffect(() => {
        fetchTypes().then();
    }, [version]);
    useEffect(init, [state]);

    function init() {
        if (state && state.geo)
            initMap(state.geo)
    }

    return (
        <>
            <div id="floating-panel">
                <b>Mode de trajet: </b>
                <select id="mode">
                    <option value="DRIVING">Conduite</option>
                    <option value="BICYCLING">Aller à Moto</option>
                </select>
                <SocketGps/>
            </div>
            <div id="map" className="map"/>
        </>
    );
}

export default Gps;