import * as React from 'react';
import "./Gps.css";
import {useVersion} from "react-admin";
import {useCallback, useEffect, useState} from "react";
import {apiUrl, apiUrlData} from "../dataProvider/ConstUrl";

const ConduiteHisto = (props: any) => {
    interface State {
        histo: any;
    }

    const version = useVersion()
    const [state, setState] = useState<State>({histo: undefined});

    const fetchTypes = useCallback(async () => {
        fetch(apiUrl + `/vehicleHistos/search/last?vehicleId=${props.vehicleId}&deliveryId=${props.deliveryId}`)
            .then(res => res.json())
            .then((result) => {
                setState(state => ({
                    ...state,
                    histo: result?._embedded.items,
                }));
            })
    }, [state]);

    useEffect(() => {
        fetchTypes().then();
    }, [version]);

    if (!state.histo)
        return <p>Chargement...</p>

    return (
        <table className="table-conduite">
            <thead>
            <tr>
                <th>Temps</th>
                <th>Conduite</th>
                <th>Evenement</th>
            </tr>
            </thead>
            <tbody>
            {state.histo.map((his: any, i: number) => {
                return (
                    <tr key={i}>
                        <td>{his.estimArrivalDate}</td>
                        <td>Normal</td>
                        <td>
                            {his.back ? "Retour" : "Aller"}
                        </td>
                    </tr>);
            })}
            </tbody>
        </table>
    );
}

export default ConduiteHisto;