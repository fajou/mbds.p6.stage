import FlagIcon from '@material-ui/icons/Flag';

import Gps from './Gps';

export default {
    list: Gps,
    icon: FlagIcon,
};