import * as React from 'react';
import SwipeableViews from 'react-swipeable-views';
import {makeStyles, Theme, useTheme} from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Card, Paper} from "@material-ui/core";
import ConduiteHisto from "./ConduiteHisto";
import "./Gps.css";
import CommandeHisto from "./CommandeHisto";

interface TabPanelProps {
    children?: React.ReactNode;
    dir?: string;
    index: any;
    value: any;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <div key={index}>
                    {children}
                </div>
            )}
        </div>
    );
}

function a11yProps(index: any) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: '100%',
    },
    tab: {}
}));

export default function GpsInfo() {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index: number) => {
        setValue(index);
    };

    return (
        <div className={classes.root}>
            <Paper square>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Conduite" {...a11yProps(0)} />
                    <Tab label="Historique" {...a11yProps(1)} />
                </Tabs>
            </Paper>
            <SwipeableViews
                className="card-conduite"
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}>
                <TabPanel value={value} index={0}>
                    <Card className="border-0">
                        <ConduiteHisto vehicleId={252} deliveryId={1286093}/>
                    </Card>
                </TabPanel>
                <TabPanel value={value} index={1}>
                   <Card className="border-0">
                       <CommandeHisto vehicleId={252} deliveryId={1286093}/>
                   </Card>
                </TabPanel>
            </SwipeableViews>
        </div>
    );
}
