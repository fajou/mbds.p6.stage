import {GeoDelivery, Vehicle} from "../types";
import GpsInfo from "./GpsInfo";


interface City {
    center: google.maps.LatLngLiteral;
    population: number;
}

function initMap(data: GeoDelivery): void {
    console.log(data);
    if (!data.vehicle || !data.relay || !data.customers)
        return;

    const selectedMode = (document.getElementById("mode") as HTMLInputElement).value;
    const directionsRenderer = new google.maps.DirectionsRenderer();
    const directionsService = new google.maps.DirectionsService();
    const entrepot = {"lat": -20.2569255, "lng": 57.46405899999999};

    const relay = JSON.parse(data.relay?.currPosition);
    const points: any = [];
    data.customers.forEach((custo, i) => {
        let pos = JSON.parse(custo.currPosition);
        points.push({
            location: {lat: pos.lat, lng: pos.lng},
        });
    });

    /*points.push({
        location: {lat: relay.lat, lng: relay.lng},
        stopover: true
    })*/

    let route = {
        origin: {lat: entrepot.lat, lng: entrepot.lng},
        destination: {lat: relay.lat, lng: relay.lng},
        waypoints: points,
        // @ts-ignore
        travelMode: google.maps.TravelMode[selectedMode],
        provideRouteAlternatives: false,
        drivingOptions: {
            departureTime: new Date(Date.now() + 200),  // for the time N milliseconds from now.
            trafficModel: 'optimistic'
        }
    };

    const citymap: Record<string, City> = {
        relay: {
            center: {lat: relay.lat, lng: relay.lng},
            population: data.relay.rayon
        }
    };

    const map = new google.maps.Map(
        document.getElementById("map") as HTMLElement, {
            zoom: 14,
            center: {lat: entrepot.lat, lng: entrepot.lng}
        }
    );
    directionsRenderer.setMap(map);

    calculateAndDisplayRoute(directionsService, directionsRenderer, route);
    (document.getElementById("mode") as HTMLInputElement).addEventListener("change", () => {
            calculateAndDisplayRoute(directionsService, directionsRenderer, route);
        }
    );

    for (const city in citymap) {
        const cityCircle = new google.maps.Circle({
            strokeColor: "#0040ff",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#fff",
            fillOpacity: 0.35,
            map,
            center: citymap[city].center,
            radius: citymap[city].population * 1000
        });
    }

    currentVehicle(map, data.vehicle);
}

function currentVehicle(map: any, vehicle: Vehicle) {
    const icon = 'https://res.cloudinary.com/dm4m7evkz/image/upload/v1599990543/pub_events/logos_icons/truck_red.png';
    const suivi = {
        position: new google.maps.LatLng(-20.191082, 57.486269),
        type: 'parking'
    };

    const infowindow = new google.maps.InfoWindow({
        content: `<div class="leaflet-popup-content-wrapper">
                   <div class="leaflet-popup-content" style="width: 231px;">
                        <div id="popup_detailed">
                            <table>
                                <tbody>
                                <tr>
                                    <td><strong>Vehicule:</strong></td>
                                    <td>${vehicle.type.name} - ${vehicle.numImm}</td>
                                </tr>
                                <tr>
                                    <td><strong>Aller-retour:</strong></td>
                                    <td>Retour</td>
                                </tr>
                                <tr>
                                    <td><strong>Vitesse min.:</strong></td>
                                    <td>${vehicle.speedMin} KH</td>
                                </tr>
                                <tr>
                                    <td><strong>Vitesse max.:</strong></td>
                                    <td>${vehicle.speedMax} KH</td>
                                </tr>
                                <tr>
                                    <td><strong>Vitesse:</strong></td>
                                    <td>74 KH</td>
                                </tr>
                                <tr>
                                    <td><strong>Date:</strong></td>
                                    <td>2020-09-13 12:00:23</td>
                                </tr>
                                <tr>
                                    <td><strong>Carburant:</strong></td>
                                    <td>${vehicle.carburant.carburantCurr} L</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>`
    });

    const marker = new google.maps.Marker({
        position: suivi.position,
        // @ts-ignore
        icon: icon,
        title: `${vehicle.brand} - ${vehicle.numImm}`,
        map: map
    });

    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });
}

function calculateAndDisplayRoute(
    directionsService: google.maps.DirectionsService,
    directionsRenderer: google.maps.DirectionsRenderer, route: any) {
    directionsService.route(route,
        (response, status) => {
            if (status == "OK") {
                directionsRenderer.setDirections(response);
                console.log(response);
            } else {
                window.alert("Directions request failed due to " + status);
            }
        }
    );
}

// [END maps_directions_travel_modes]
export {initMap};