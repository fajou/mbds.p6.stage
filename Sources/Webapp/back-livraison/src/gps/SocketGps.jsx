import React from 'react';
import SockJsClient from 'react-stomp';

class SocketGps extends React.Component {
    constructor(props) {
        super(props);
    }

    sendMessage = (msg) => {
        this.clientRef.sendMessage('/gps/traceur', msg);
    }

    render() {
        return (
            <div>
                <SockJsClient url='http://localhost:8090/gps-traceur-websocket' topics={['/gps/receives']}
                              onMessage={(msg) => {
                                  console.log(msg);
                              }}
                              ref={(client) => {
                                  this.clientRef = client
                              }}/>
            </div>
        );
    }
}

export default SocketGps;