import * as React from 'react';
import {Create, required, SimpleForm, TextInput, useTranslate,} from 'react-admin';
import {Box, Typography} from '@material-ui/core';
import {makeStyles, Theme} from '@material-ui/core/styles';
import {Styles} from '@material-ui/styles/withStyles';
import TypeVehicleInput from "./inputs/TypeVehicleInput";
import {Vehicle} from "../types";

let minMax = (name: string, min: number, moy: number | undefined, max: number, errors: any) => {
    if (min > max) {
        errors[name] = ['resources.vehicles.fields.not_max'];
        return;
    }

    if (moy && (moy < min || max < moy)) {
        errors[name] = ['resources.vehicles.fields.not_moy'];
    }
}

let notZero = (name: string, value: number, errors: any) => {
    if (value <= 0) {
        errors[name] = ['resources.vehicles.fields.not_zero'];
    }
}

export let validateForm = (record: Vehicle) => {
    let errors = {} as any;

    notZero('weightMin', record.weightMin, errors);
    notZero('weightMax', record.weightMax, errors);
    notZero('carburant.carburantCurr', record.carburant?.carburantCurr, errors);
    notZero('carburant.carburantMax', record.carburant?.carburantMax, errors);
    notZero('speedMin', record.speedMin, errors);
    notZero('speedMoy', record.speedMoy, errors);
    notZero('speedMax', record.speedMax, errors);

    minMax('weightMin', record.weightMin, undefined, record.weightMax, errors);
    if (record.carburant)
        minMax('carburant.carburantCurr', record.carburant.carburantCurr, undefined, record.carburant.carburantMax, errors);
    minMax('speedMoy', record.speedMin, record.speedMoy, record.speedMax, errors);

    return errors;
};

export const styles: Styles<Theme, any> = {
    right: {display: 'inline-block'},
    left: {display: 'inline-block', marginLeft: 32},
    desc: {width: 544},
    max: {maxWidth: 544},
    veh_min: {display: 'inline-block'},
    veh_max: {display: 'inline-block', marginLeft: 32},
    comment: {
        maxWidth: '20em',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    },
    weight_min: {display: 'inline-block'},
    weight_max: {display: 'inline-block', marginLeft: 32},
};

const useStyles = makeStyles(styles);

const VehicleCreate = (props: any) => {
    const classes = useStyles();

    return (
        <Create {...props}>
            <SimpleForm validate={validateForm}>
                <SectionTitle label="resources.vehicles.fields.create"/>
                <TextInput
                    autoFocus
                    label="resources.vehicles.fields.marque"
                    source="brand"
                    formClassName={classes.right}
                    validate={requiredValidate}
                />
                <TextInput
                    source="colorVeh"
                    label="resources.vehicles.fields.color_veh"
                    formClassName={classes.left}
                    validate={requiredValidate}
                />
                <TextInput
                    label="resources.vehicles.fields.des_veh"
                    source="descVeh"
                    fullWidth
                    multiline
                    rows={3}
                    formClassName={classes.desc}
                />
                <TextInput
                    autoFocus
                    label="resources.vehicles.fields.num_imm"
                    source="numImm"
                    validate={requiredValidate}
                    formClassName={classes.right}
                />
                <TypeVehicleInput
                    validate={requiredValidate}
                    formClassName={classes.left}
                />
                <Separator/>

                <SectionTitle label="resources.vehicles.vitesse.create"/>
                <TextInput
                    source="speedMin"
                    type="number"
                    label="resources.vehicles.vitesse.vit_min"
                    formClassName={classes.veh_min}
                    validate={requiredValidate}
                />
                <TextInput
                    source="speedMax"
                    type="number"
                    label="resources.vehicles.vitesse.vit_max"
                    formClassName={classes.veh_max}
                    validate={requiredValidate}
                />
                <TextInput
                    source="speedMoy"
                    type="number"
                    label="resources.vehicles.vitesse.vit_moy"
                    formClassName={classes.max}
                    validate={requiredValidate}
                />
                <SectionTitle label='resources.vehicles.carburant.title'/>
                <TextInput
                    label="resources.vehicles.carburant.c_type"
                    source="carburant.carburantType"
                    validate={requiredValidate}
                    fullWidth
                    formClassName={classes.max}
                />
                <TextInput
                    label="resources.vehicles.carburant.c_curr"
                    type="number"
                    source="carburant.carburantCurr"
                    formClassName={classes.right}
                    validate={requiredValidate}
                />
                <TextInput
                    label="resources.vehicles.carburant.c_max"
                    type="number"
                    source="carburant.carburantMax"
                    formClassName={classes.left}
                    validate={requiredValidate}
                />

                <Separator/>
                <SectionTitle label="resources.vehicles.weight.create"/>
                <TextInput
                    label="resources.vehicles.weight.w_min"
                    type="number"
                    source="weightMin"
                    formClassName={classes.weight_min}
                    validate={requiredValidate}
                />
                <TextInput
                    label="resources.vehicles.weight.w_max"
                    type="number"
                    source="weightMax"
                    formClassName={classes.weight_max}
                    validate={requiredValidate}
                />
            </SimpleForm>
        </Create>
    );
};

const requiredValidate = [required()];

const SectionTitle = ({label}: { label: string }) => {
    const translate = useTranslate();

    return (
        <Typography variant="h6" gutterBottom>
            {translate(label)}
        </Typography>
    );
};

const Separator = () => <Box pt="1em"/>;

export default VehicleCreate;
