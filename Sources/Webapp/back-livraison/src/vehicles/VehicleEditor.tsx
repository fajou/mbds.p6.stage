import * as React from 'react';
import {FC} from 'react';
import {Edit, FormWithRedirect, required, TextInput, Toolbar, useTranslate,} from 'react-admin';
import {Box, Card, CardContent, Divider, Typography} from '@material-ui/core';

import Aside from './Aside';
import DrivierNameField from './fields/DrivierNameField';
import {validateForm} from './VehicleCreate';
import {FieldProps, Vehicle} from '../types';
import TypeVehicleInput from "./inputs/TypeVehicleInput";
import DriverVehicleInput from "./inputs/DriverVehicleInput";

const VehicleEditor = (props: any) => {
    return (
        <Edit
            title={<VehicleTitle/>}
            aside={<Aside/>}
            component="div"
            {...props}>
            <VehicleForm/>
        </Edit>
    );
};

const VehicleTitle: FC<FieldProps<Vehicle>> = ({record}) => {
    return record ? <DrivierNameField record={record} size="32"/> : null;
};

const VehicleForm = (props: any) => {
    const translate = useTranslate();

    return (
        <FormWithRedirect
            {...props}
            validate={validateForm}
            render={(formProps: any) => (
                <Card>
                    <form>
                        <CardContent>
                            <Box display={{md: 'block', lg: 'flex'}}>
                                <Box flex={2} mr={{md: 0, lg: '1em'}}>
                                    <Typography variant="body1" gutterBottom>
                                        {translate('resources.vehicles.fields.update')}
                                    </Typography>
                                    <Box display={{xs: 'block', sm: 'flex'}}>
                                        <Box flex={1} mr={{xs: 0, sm: '0.5em'}}>
                                            <TextInput
                                                autoFocus
                                                label="resources.vehicles.fields.marque"
                                                source="brand"
                                                validate={requiredValidate}
                                                fullWidth
                                            />
                                        </Box>
                                        <Box flex={1} ml={{xs: 0, sm: '0.5em'}}>
                                            <TextInput
                                                source="colorVeh"
                                                label="resources.vehicles.fields.color_veh"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                    </Box>
                                    <Box mt="1em"/>
                                    <Typography variant="body1" gutterBottom>
                                        {translate(
                                            'resources.vehicles.vitesse.update'
                                        )}
                                    </Typography>
                                    <Box display={{xs: 'block', sm: 'flex'}}>
                                        <Box
                                            flex={1}
                                            mr={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                source="speedMin"
                                                type="number"
                                                label="resources.vehicles.vitesse.vit_min"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                source="speedMoy"
                                                type="number"
                                                label="resources.vehicles.vitesse.vit_moy"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                source="speedMax"
                                                type="number"
                                                label="resources.vehicles.vitesse.vit_max"
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                    </Box>

                                    <Box mt="1em"/>

                                    <Typography variant="body1" gutterBottom>
                                        {translate(
                                            'resources.vehicles.carburant.title'
                                        )}
                                    </Typography>
                                    <Box display={{xs: 'block', sm: 'flex'}}>
                                        <Box
                                            flex={1}
                                            mr={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.carburant.c_type"
                                                source="carburant.carburantType"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.carburant.c_max"
                                                type="number"
                                                source="carburant.carburantMax"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.carburant.c_curr"
                                                type="number"
                                                source="carburant.carburantCurr"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                    </Box>

                                    <Typography variant="body1" gutterBottom>
                                        {translate('resources.vehicles.weight.update')}
                                    </Typography>
                                    <Box display={{xs: 'block', sm: 'flex'}}>
                                        <Box
                                            flex={1}
                                            mr={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.weight.w_min"
                                                type="number"
                                                source="weightMin"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.weight.w_max"
                                                type="number"
                                                source="weightMax"
                                                fullWidth
                                                validate={requiredValidate}
                                            />
                                        </Box>
                                    </Box>

                                    <Typography variant="body1" gutterBottom>
                                        {translate('resources.vehicles.trajet.update')}
                                    </Typography>
                                    <Box display={{xs: 'block', sm: 'flex'}}>
                                        <Box
                                            flex={1}
                                            mr={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.trajet.min"
                                                type="number"
                                                source="trajetMin"
                                                fullWidth
                                            />
                                        </Box>
                                        <Box
                                            flex={1}
                                            ml={{xs: 0, sm: '0.5em'}}
                                        >
                                            <TextInput
                                                label="resources.vehicles.trajet.max"
                                                type="number"
                                                source="trajetMax"
                                                fullWidth
                                            />
                                        </Box>
                                    </Box>
                                    <TextInput
                                        label="resources.vehicles.fields.des_veh"
                                        source="descVeh"
                                        fullWidth
                                        multiline
                                        rows={3}
                                    />
                                </Box>
                                <Box
                                    flex={1}
                                    ml={{xs: 0, lg: '1em'}}
                                    mt={{xs: '1em', lg: 0}}
                                >
                                    <Typography variant="body1" gutterBottom>
                                        {translate(
                                            'resources.vehicles.car_teck'
                                        )}
                                    </Typography>
                                    <div>
                                        <TypeVehicleInput fullWidth/>
                                    </div>
                                    <div>
                                        <DriverVehicleInput record={props.record} fullWidth/>
                                    </div>
                                    <div>
                                        <TextInput
                                            label="resources.vehicles.fields.num_imm"
                                            source="numImm"
                                            fullWidth
                                            validate={requiredValidate}
                                        />
                                    </div>

                                    <Typography variant="body1" gutterBottom>
                                        {translate(
                                            'resources.vehicles.car_time_intr'
                                        )}
                                    </Typography>
                                    <TextInput
                                        label="resources.vehicles.interval.min"
                                        type="number"
                                        source="timeIntr.timeMin"
                                        fullWidth
                                        validate={requiredValidate}
                                    />
                                    <TextInput
                                        label="resources.vehicles.interval.moy"
                                        type="number"
                                        source="timeIntr.timeMoy"
                                        fullWidth
                                        validate={requiredValidate}
                                    />
                                    <TextInput
                                        label="resources.vehicles.interval.max"
                                        type="number"
                                        source="timeIntr.timeMax"
                                        fullWidth
                                        validate={requiredValidate}
                                    />
                                </Box>
                            </Box>
                        </CardContent>
                        <Toolbar
                            record={formProps.record}
                            basePath={formProps.basePath}
                            undoable={true}
                            invalid={formProps.invalid}
                            handleSubmit={formProps.handleSubmit}
                            saving={formProps.saving}
                            resource="customers"
                        />
                    </form>
                </Card>
            )}
        />
    );
};

const requiredValidate = [required()];

export default VehicleEditor;
