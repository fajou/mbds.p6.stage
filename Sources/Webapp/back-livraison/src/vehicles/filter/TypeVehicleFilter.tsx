import * as React from 'react';
import {FC, useCallback, useEffect, useState} from 'react';
import {FilterList, FilterListItem, useVersion} from 'react-admin';
import {InputProps} from 'ra-core';
import {TypeVehicule} from "../../types";
import {apiUrlData} from "../../dataProvider/ConstUrl";
import MotorcycleTwoToneIcon from "@material-ui/icons/MotorcycleTwoTone";

interface Props extends Omit<InputProps, 'source'> {
    source?: string;
    formClassName?: string;
}

interface State {
    typeVehicles?: TypeVehicule[];
}


const TypeVehicleFilter: FC<Props> = props => {
    const version = useVersion()
    const [state, setState] = useState<State>({});

    const fetchTypes = useCallback(async () => {
        fetch(apiUrlData + "/types_vehicle")
            .then(res => res.json())
            .then((result) => {
                setState(state => ({
                    ...state,
                    typeVehicles: result,
                }));
            })
    }, [state]);

    useEffect(() => {
        fetchTypes().then();
    }, [version])

    return (
        <FilterList
            label="resources.vehicles.filters.type"
            icon={<MotorcycleTwoToneIcon/>}>
            {state.typeVehicles?.map(type => (
                <FilterListItem
                    label={type.name}
                    key={type.id}
                    value={{type: type.id}}
                />
            ))}
        </FilterList>
    );
};

export default TypeVehicleFilter;
