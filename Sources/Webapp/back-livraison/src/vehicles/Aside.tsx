import * as React from 'react';
import {FC} from 'react';
import {DateField, TextField, useGetOne, useQuery, useTranslate,} from 'react-admin';
import PropTypes from 'prop-types';
import {Avatar, Box, Card, CardContent, CardHeader, Typography,} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import EmojiTransportationTwoToneIcon from '@material-ui/icons/EmojiTransportationTwoTone';
import NotificationsIcon from '@material-ui/icons/Notifications';
import order from '../orders';
import {makeStyles} from '@material-ui/core/styles';
import {Record} from 'ra-core';
import {Delivery} from '../types';

const useAsideStyles = makeStyles(theme => ({
    root: {
        width: 400,
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
    },
}));

interface AsideProps {
    record?: Record;
    basePath?: string;
}

const Aside: FC<AsideProps> = ({record, basePath}) => {
    const classes = useAsideStyles();
    return (
        <div className={classes.root}>
            {record && <EventList record={record} basePath={basePath}/>}
        </div>
    );
};

Aside.propTypes = {
    record: PropTypes.any,
    basePath: PropTypes.string,
};

interface EventListProps {
    record?: Record;
    basePath?: string;
}

const EventList: FC<EventListProps> = ({record, basePath}) => {
    const translate = useTranslate();

    const {loaded, data: deliveries} = useQuery({
        type: 'getList',
        resource: 'deliveries',
        payload: {
            filter: {id: record && record.id, target: "vehicle_id"},
            sort: {field: 'date', order: 'DESC'},
            pagination: {page: 1, perPage: 100},
        },
    });

    if (!loaded)
        return <></>;
    const events = deliveriesList(deliveries);
    let delivery = undefined;
    if (deliveries.length)
        delivery = deliveries[0];

    return (
        <>
            <Box m="0 0 1em 1em">
                <Card>
                    <CardContent>
                        <Typography variant="h6" gutterBottom>
                            {translate('resources.customers.fieldGroups.history')}
                        </Typography>
                        <Box display="flex">
                            <Box flexGrow={1}>
                                {deliveries.length > 0 && (
                                    <Box display="flex">
                                        <Box mr=".5em">
                                            <EmojiTransportationTwoToneIcon fontSize="small" color="primary"/>
                                        </Box>
                                        <Box flexGrow={1}>
                                            <Typography>
                                                {translate('resources.deliveries.amount', {smart_count: deliveries.length,})}
                                            </Typography>
                                        </Box>
                                    </Box>
                                )}
                            </Box>
                            <Box flexGrow={1}>
                                {deliveries.length > 0 && (
                                    <Box display="flex">
                                        <Box mr=".5em">
                                            <NotificationsIcon fontSize="small" color="primary"/>
                                        </Box>
                                        <Box flexGrow={1}>
                                            <Typography>
                                                {translate('resources.deliveries.notif', {smart_count: 0})}
                                            </Typography>
                                        </Box>
                                    </Box>
                                )}
                            </Box>
                        </Box>
                    </CardContent>
                </Card>
            </Box>

            {events.map(event =>
                <DeliveryInfo record={event.data as Delivery}
                              key={`delvr_${event.data.id}`}
                              basePath={basePath}
                />
            )}
        </>
    );
};

interface AsideEvent {
    type: string;
    date: Date;
    data: Delivery;
}

const deliveriesList = (deliveries: Delivery[]): AsideEvent[] => {
    return deliveries.map<AsideEvent>(delivery => ({
        type: 'order',
        date: delivery.departDate,
        data: delivery,
    }));
};

const useEventStyles = makeStyles({
    card: {
        margin: '0 0 1em 1em',
        minWidth: 350,
    },
    cardHeader: {
        alignItems: 'flex-start',
    },
    clamp: {
        display: '-webkit-box',
        '-webkit-line-clamp': 3,
        '-webkit-box-orient': 'vertical',
        overflow: 'hidden',
    },
});

interface OrderProps {
    record?: Delivery;
    basePath?: string;
}

const DeliveryInfo: FC<OrderProps> = ({record, basePath}) => {
    const translate = useTranslate();
    const classes = useEventStyles();

    return record ? (
        <Card className={classes.card}>
            <CardHeader
                className={classes.cardHeader}
                avatar={
                    <Avatar>
                        <order.icon/>
                    </Avatar>
                }
                title={`${translate('resources.deliveries.name', {smart_count: 1,})} #${record.id}`}
                subheader={
                    <>
                        <Typography variant="body2">{record.updatedDate}</Typography>
                        <Typography variant="body2" color="primary">
                            {translate('resources.commands.nb_items', {
                                smart_count: record.totalItems,
                                _: '1 article |||| %{smart_count} articles',
                            })}
                            &nbsp;-&nbsp;
                            {translate('resources.commands.nb_items', {
                                smart_count: record.totalWeight, _: '%{smart_count} Kg',
                            })}
                            &nbsp;-&nbsp;
                            <TextField
                                source="status"
                                record={record}
                                basePath={basePath}
                            />
                        </Typography>
                    </>
                }
            />
        </Card>
    ) : null;
};

export default Aside;
