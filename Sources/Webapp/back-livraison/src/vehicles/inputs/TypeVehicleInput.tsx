import * as React from 'react';
import {FC, useCallback, useEffect, useState} from 'react';
import {SelectInput} from 'react-admin';
import {InputProps} from 'ra-core';
import {TypeVehicule} from "../../types";
import {apiUrlData} from "../../dataProvider/ConstUrl";

interface Props extends Omit<InputProps, 'source'> {
    source?: string;
    formClassName?: string;
}

interface State {
    typeVehicles?: TypeVehicule[];
}


const TypeVehicleInput: FC<Props> = props => {
    const [state, setState] = useState<State>({});

    const fetchTypes = useCallback(async () => {
        fetch(apiUrlData + "/types_vehicle")
            .then(res => res.json())
            .then((result) => {
                setState(state => ({
                    ...state,
                    typeVehicles: result,
                }));
            })
    }, [state]);

    useEffect(() => {
        fetchTypes().then();
    }, [])

    return (
        <SelectInput
            {...props}
            choices={state.typeVehicles?.map(type => ({
                id: type.id,
                name: type.name,
            }))}
            fullWidth
        />
    );
};

TypeVehicleInput.defaultProps = {
    label: 'Type du vehicule',
    source: 'type.id',
};

export default TypeVehicleInput;
