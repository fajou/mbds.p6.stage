import * as React from 'react';
import {FC, useCallback, useEffect, useState} from 'react';
import {SelectInput, useVersion} from 'react-admin';
import {InputProps} from 'ra-core';
import {Driver, Vehicle} from "../../types";
import {apiUrlData} from "../../dataProvider/ConstUrl";

interface Props extends Omit<InputProps, 'source'> {
    source?: string;
    formClassName?: string;
    record: Vehicle
}

interface State {
    drivers?: Driver[];
}


const DriverVehicleInput: FC<Props> = props => {
    const version = useVersion()
    const [state, setState] = useState<State>({});

    const fetchDrivers = useCallback(async () => {
        const curr = props.record.driver?.id ? `?id=${props.record.driver?.id}` : "";
        fetch(`${apiUrlData}/drivers_free${curr}`)
            .then(res => res.json())
            .then((result) => {
                setState(state => ({
                    ...state,
                    drivers: result,
                }));
            })
    }, [state]);

    useEffect(() => {
        fetchDrivers().then();
    }, [version]);

    const changeDriver = (data: any) => {
        state.drivers?.forEach((item) => {
            if (item.id == data.target.value) {
                props.record.driver = item;
                return;
            }
        })
    }

    return (
        <SelectInput
            {...props}
            choices={state.drivers?.map(driver => ({
                id: driver.id,
                name: `${driver.name} - ${driver.license}`,
            }))}
            fullWidth
            onChange={changeDriver}
        />
    );
};

DriverVehicleInput.defaultProps = {
    label: 'Conducteur disponible',
    source: 'driver.id',
};

export default DriverVehicleInput;
