import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface SpeedMoyFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{`${record.speedMoy} KmH`}</span>
}

function SpeedMoyField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

SpeedMoyField.defaultProps = {
    label: 'Vitesse Moy.',
    textAlign: 'left',
};

export default SpeedMoyField;
