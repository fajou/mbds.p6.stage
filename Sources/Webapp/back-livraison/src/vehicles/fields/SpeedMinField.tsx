import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface SpeedMinFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{`${record.speedMin} KmH`}</span>
}

function SpeedMinField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

SpeedMinField.defaultProps = {
    label: 'Vitesse Min.',
    textAlign: 'left',
};

export default SpeedMinField;
