import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface WeightMaxFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{`${record.weightMax} Kg`}</span>
}

function WeightMaxField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

WeightMaxField.defaultProps = {
    label: 'Poids Max.',
    textAlign: 'left',
};

export default WeightMaxField;
