import * as React from 'react';
import {FC} from 'react';
import {ReferenceField} from 'react-admin';

import FullNameField from './DrivierNameField';
import {ReferenceFieldProps} from '../../types';

const DriverReferenceField: FC<Omit<ReferenceFieldProps, 'reference' | 'children'>> = props => (
    <ReferenceField label="Client Cn" source="id" reference="customer" {...props}>
        <FullNameField/>
    </ReferenceField>
);

DriverReferenceField.defaultProps = {
    source: 'driver',
    addLabel: true,
};

export default DriverReferenceField;
