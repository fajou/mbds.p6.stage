import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface WeightMinFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{`${record.weightMin} Kg`}</span>
}

function WeightMinField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

WeightMinField.defaultProps = {
    label: 'Poids Min.',
    textAlign: 'left',
};

export default WeightMinField;
