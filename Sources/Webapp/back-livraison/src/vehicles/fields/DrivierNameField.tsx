import * as React from 'react';
import {FC, memo} from 'react';
import {makeStyles} from '@material-ui/core/styles';

import DriverAvatarField from './DriverAvatarField';
import {FieldProps, Vehicle} from '../../types';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'nowrap',
        alignItems: 'center',
    },
    avatar: {
        marginRight: theme.spacing(1),
        marginTop: -theme.spacing(0.5),
        marginBottom: -theme.spacing(0.5),
    },
}));

interface Props extends FieldProps<Vehicle> {
    size?: string;
}

const DrivierNameField: FC<Props> = ({record, size}) => {
    const classes = useStyles();
    return record && record.driver ? (
        <div className={classes.root}>
            <DriverAvatarField className={classes.avatar}
                               record={record.driver}
                               size={size}/>
            {record.driver.name}
        </div>
    ) : null;
};

DrivierNameField.defaultProps = {
    source: 'driver',
    label: 'resources.customers.fields.name',
};

export default memo<Props>(DrivierNameField);
