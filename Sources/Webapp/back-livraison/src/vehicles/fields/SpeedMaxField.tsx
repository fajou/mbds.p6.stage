import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface SpeedMaxFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{`${record.speedMax} KmH`}</span>
}

function SpeedMaxField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

SpeedMaxField.defaultProps = {
    label: 'Vitesse Max.',
    textAlign: 'left',
};

export default SpeedMaxField;
