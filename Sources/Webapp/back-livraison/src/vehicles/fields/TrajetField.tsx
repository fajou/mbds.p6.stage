import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Vehicle} from '../../types';

interface TrajetFieldProps extends FieldProps {
    textAlign?: string;
}

function render(record: Vehicle) {
    return <span>{record.trajetMax ? `${record.trajetMax} Km` : '-'}</span>
}

function TrajetField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

TrajetField.defaultProps = {
    textAlign: 'left',
};

export default TrajetField;
