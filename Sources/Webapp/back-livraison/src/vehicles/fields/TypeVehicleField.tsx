import * as React from 'react';
import {FC} from 'react';
import Chip from '@material-ui/core/Chip';
import {FieldProps, Vehicle} from '../../types';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    main: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: -8,
        marginBottom: -8,
    },
    chip: {margin: 4},
});

const TypeVehicleField: FC<FieldProps<Vehicle>> = ({record}) => {
    const classes = useStyles();
    return record ? (
        <span className={classes.main}>
            <Chip
                size="small"
                key={record.type.id}
                className={classes.chip}
                label={record.type.name}
            />
        </span>
    ) : null;
};

TypeVehicleField.defaultProps = {
    label: 'Type Vehicule',
    addLabel: true,
};

export default TypeVehicleField;
