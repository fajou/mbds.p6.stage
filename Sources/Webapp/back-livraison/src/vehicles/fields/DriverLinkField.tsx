import * as React from 'react';
import {FC} from 'react';
import {Link} from 'react-admin';

import DrivierNameField from './DrivierNameField';
import {FieldProps, Vehicle} from '../../types';

const DriverLinkField: FC<FieldProps<Vehicle>> = props => {
    const driver = props.record?.driver;
    return driver ? (
        <Link to={`/driveres/${driver.id}`}>
            <DrivierNameField {...props} />
        </Link>
    ) : null;
};

DriverLinkField.defaultProps = {
    label: "Conducteur",
    addLabel: true
};

export default DriverLinkField;
