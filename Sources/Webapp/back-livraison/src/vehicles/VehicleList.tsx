import * as React from 'react';
import {Datagrid, Filter, List, SearchInput, TextField,} from 'react-admin';

import TypeVehicleField from './fields/TypeVehicleField';
import DriverLinkField from './fields/DriverLinkField';
import VehicleListAside from './VehicleListAside';
import WeightMinField from "./fields/WeightMinField";
import WeightMaxField from "./fields/WeightMaxField";
import SpeedMoyField from "./fields/SpeedMoyField";
import StatusVehicleField from "./fields/StatusVehicleField";
import TrajetField from "./fields/TrajetField";

const VisitorFilter = (props: any) => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn/>
    </Filter>
);

const VehicleList = (props: any) => {

    return (
        <>
            <List {...props}
                  title="Vehicules"
                  filters={<VisitorFilter/>}
                  sort={{field: 'updatedDate', order: 'DESC'}}
                  perPage={25}
                  aside={<VehicleListAside/>}>
                <Datagrid optimized rowClick="edit">
                    <DriverLinkField source="driver.name"/>
                    <StatusVehicleField source="status" label="Statut"/>
                    <TypeVehicleField source="type.id"/>
                    {/* <TextField source="numImm" label="Num. Imm" type="string"/>*/}
                    <TrajetField source="trajetMax" label="Trajet max." type="string"/>
                    <SpeedMoyField source="speedMoy"/>
                    <WeightMinField source="weightMin"/>
                    <WeightMaxField source="weightMax"/>
                </Datagrid>
            </List>
        </>

    );
};

export default VehicleList;
