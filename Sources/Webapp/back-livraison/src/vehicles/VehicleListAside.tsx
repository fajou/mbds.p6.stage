import * as React from 'react';
import {FC} from 'react';
import {Card as MuiCard, CardContent} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import EmojiTransportationTwoToneIcon from '@material-ui/icons/EmojiTransportationTwoTone';
import {FilterList, FilterListItem} from 'react-admin';
import {endOfYesterday, startOfMonth, startOfWeek, subMonths, subWeeks,} from 'date-fns';
import TypeVehicleFilter from "./filter/TypeVehicleFilter";
import DriveEtaTwoToneIcon from "@material-ui/icons/DriveEtaTwoTone";

const Card = withStyles(theme => ({
    root: {
        [theme.breakpoints.up('sm')]: {
            order: -1,
            width: '15em',
            marginRight: '1em',
        },
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
}))(MuiCard);

const date = (date: Date) => {
    return date.toISOString().replace("T", " ").replace(".000Z", "");
}

const Aside: FC = () => (
    <Card>
        <CardContent>
            <FilterList
                label="resources.vehicles.filters.dernier_livraison"
                icon={<AccessTimeIcon/>}
            >
                <FilterListItem
                    label="resources.vehicles.filters.today"
                    value={{
                        dateMin: date(endOfYesterday()),
                        dateMax: undefined,
                    }}
                />
                <FilterListItem
                    label="resources.vehicles.filters.this_week"
                    value={{
                        dateMin: date(startOfWeek(new Date())),
                        dateMax: undefined,
                    }}
                />
                <FilterListItem
                    label="resources.vehicles.filters.last_week"
                    value={{
                        dateMin: date(subWeeks(startOfWeek(new Date()), 1)),
                        dateMax: date(startOfWeek(new Date()))
                    }}
                />
                <FilterListItem
                    label="resources.vehicles.filters.this_month"
                    value={{
                        dateMin: date(startOfMonth(new Date())),
                        dateMax: undefined,
                    }}
                />
                <FilterListItem
                    label="resources.vehicles.filters.last_month"
                    value={{
                        dateMin: date(subMonths(startOfMonth(new Date()), 1)),
                        dateMax: date(startOfMonth(new Date()))
                    }}
                />
                <FilterListItem
                    label="resources.vehicles.filters.earlier"
                    value={{
                        dateMin: undefined,
                        dateMax: date(subMonths(startOfMonth(new Date()), 1))
                    }}
                />
            </FilterList>

            <FilterList
                label="resources.vehicles.filters.status"
                icon={<EmojiTransportationTwoToneIcon/>}
            >
                <FilterListItem
                    label="resources.vehicles.filters.libre"
                    value={{status: 'FREE'}}
                />
                <FilterListItem
                    label="resources.vehicles.filters.encours"
                    value={{status: 'DELIVERING'}}
                />
            </FilterList>
            <FilterList
                label="resources.vehicles.filters.etat"
                icon={<DriveEtaTwoToneIcon/>}
            >
                <FilterListItem
                    label="resources.vehicles.filters.entretien"
                    value={{status: 'MAINTENANCE'}}
                />
                <FilterListItem
                    label="resources.vehicles.filters.reserver"
                    value={{status: 'RESERVED'}}
                />
            </FilterList>
            <TypeVehicleFilter/>
        </CardContent>
    </Card>
);

export default Aside;
