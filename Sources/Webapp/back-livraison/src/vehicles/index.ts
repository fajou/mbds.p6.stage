import VisitorIcon from '@material-ui/icons/People';

import VehicleList from './VehicleList';
import VehicleCreate from './VehicleCreate';
import VehicleEditor from './VehicleEditor';

export default {
    list: VehicleList,
    create: VehicleCreate,
    edit: VehicleEditor,
    icon: VisitorIcon,
};
