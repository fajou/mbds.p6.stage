import * as React from 'react';
import {FC} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Article, FieldProps} from '../types';

const useStyles = makeStyles({
    root: {width: 40, maxWidth: 40, maxHeight: 40},
});

const ThumbnailField: FC<FieldProps<Article>> = ({record}) => {
    const classes = useStyles();
    return record ? (
        <img src={record.image.secure_url} className={classes.root} alt=""/>
    ) : null;
};

export default ThumbnailField;
