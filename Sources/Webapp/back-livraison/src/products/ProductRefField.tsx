import * as React from 'react';
import {FC} from 'react';
import {Article, FieldProps} from '../types';

const ProductRefField: FC<FieldProps<Article>> = ({record}) => {
    const cls = record?.classifier.substring(1, record?.classifier.length - 1).split(",");

    return record ? (
        <div>{cls?.join(" • ")}</div>
    ) : null;
};

ProductRefField.defaultProps = {
    source: 'id',
    label: 'Reference',
};

export default ProductRefField;
