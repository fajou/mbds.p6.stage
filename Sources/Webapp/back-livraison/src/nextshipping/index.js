import BarChartIcon from '@material-ui/icons/BarChart';

import Shipping from './Nextshipping';

export default {
    list: Shipping,
    icon: BarChartIcon,
};