import "./State.css";
import NextshipingChart from "./NextshipingChart";
import {Button, Card, CardContent, CardHeader, Grid, MuiThemeProvider, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslate} from "react-admin";
import TotalCommandeAnnuler from "./total/TotalCommandeAnnuler";
import TotalNouveauCommande from "./total/TotalNouveauCommande";
import TotalCommandeGrouper from "./total/TotalCommandeGrouper";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import React, {FC, useEffect, useState} from "react";
import TotalLivraisonEnattente from "./total/TotalLivraisonEnattente";
import {apiUrl, apiUrlLivrs} from "../dataProvider/ConstUrl";
import {Vehicle, VehicleDeliveryInfo} from "../types";
import Chip from "@material-ui/core/Chip";

const useStyles = makeStyles({
    card: {
        maxWidth: '100%',
        marginTop: 20,
        minHeight: 52,
        '& a': {
            textDecoration: 'none',
            color: 'inherit',
        },
    },
    chips: {
        fontSize: 10
    }
});

const Nextshipping: FC = () => {
    const classes = useStyles();
    const translate = useTranslate();
    const theme = createMuiTheme();
    const [vehicles, setVehicles] = useState([]);

    function fetchVehicle() {
        fetch(`${apiUrlLivrs}/vehicleDeliveryInfo`)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    console.log(data);
                    setVehicles(data);
                }
            });
    }

    useEffect(fetchVehicle, [setVehicles]);
    if (!vehicles)
        return <p className="text-center">Chargement en cours...</p>;

    const body = (data: VehicleDeliveryInfo, index: number) => {
        return (
            <tr key={index}>
                <th>
                    {data.vehicle.brand}
                    <Typography variant="caption" display="block" gutterBottom>
                        {data.vehicle.numImm}
                    </Typography>
                </th>
                <td>{data?.vehicle?.status}</td>
                <td>{data?.delivery?.id}</td>
                <td>{data?.delivery?.orders?.length}</td>
                <td>{data?.delivery?.departDate}</td>
                <td>{data?.histoItem?.estimArrivalDate ?
                    <Chip size="small" label={data?.histoItem?.estimArrivalDate} variant="default"/> : ""}
                </td>
                <td>
                    {data?.histoItem?.estimDuration?.distText}
                    <Typography variant="caption" color="primary" display="block" gutterBottom>
                        {data?.histoItem?.estimDuration?.duraText}
                    </Typography>
                </td>
                <td>{data?.histoItem?.estimDepartDate}</td>
                <td>
                    {data?.delivery?.status ?
                        <Chip size="small" label={data?.delivery?.status} variant="outlined"/> : ""}
                </td>
            </tr>
        );
    }

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <TotalNouveauCommande value={12}/>
                </Grid>
                <Grid item xs={3}>
                    <TotalCommandeGrouper value={73}/>
                </Grid>
                <Grid item xs={3}>
                    <TotalLivraisonEnattente value={4}/>
                </Grid>
                <Grid item xs={3}>
                    <TotalCommandeAnnuler value={2}/>
                </Grid>
            </Grid>
            <Card className={classes.card}>
                <CardHeader title={translate('pos.dashboard.prediction_date_shipping')}/>
                <CardContent className="table-prediction">
                    <table>
                        <thead>
                        <tr>
                            <th>Vehicule</th>
                            <th>
                                Statut
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    Vehicule
                                </Typography>
                            </th>
                            <th>ID.
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    Livraison
                                </Typography>
                            </th>
                            <th>Nombre
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    de commandes
                                </Typography>
                            </th>
                            <th>Date de départ</th>
                            <th>Date d'arrivée
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    estimée
                                </Typography>
                            </th>
                            <th color="primary">
                                Distance &
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    Durée estimée
                                </Typography></th>
                            <th>
                                Prochaine date de
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    livraison disponible
                                </Typography>
                            </th>
                            <th>Statut
                                <Typography variant="caption" color="primary" display="block" gutterBottom>
                                    Livraison
                                </Typography>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {vehicles.map(body)}
                        </tbody>
                    </table>
                    <NextshipingChart/>
                </CardContent>
            </Card>
        </div>
    );
};

export default Nextshipping;
