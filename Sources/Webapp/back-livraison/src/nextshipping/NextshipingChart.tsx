import * as React from 'react';
import {FC, useEffect, useState} from 'react';
import {Card, CardContent, CardHeader} from '@material-ui/core';
import {initChart} from './chart.js'

import {Order} from '../types';


const NextshipingChart: FC<{ orders?: Order[] }> = ({orders}) => {
    function init() {
        initChart([]);
    }

    useEffect(init, [])

    return (
        <Card>
            <CardContent>
                <div className="chart-container">
                    <canvas id="nextshippingChart" height={90}/>
                </div>
            </CardContent>
        </Card>
    );
};

export default NextshipingChart;
