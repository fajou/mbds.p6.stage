import React from "react";

import {createStyles} from "@material-ui/styles";
import {withStyles, Toolbar, Button} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import moment from "moment";

export const styles = theme =>
    createStyles({
        toolbar: {
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
            justifyContent: "center",
            height: 100,
            backgroundColor:
                theme.palette.type === "light"
                    ? theme.palette.primary.main
                    : theme.palette.background.default
        },
        label: {
            color:
                theme.palette.type === "light"
                    ? theme.palette.primary.contrastText
                    : theme.palette.background.default
        }
    });

const DateToolbar = ({setOpenView, date}) => {
    const today = moment(date);

    let weeknumber = today.week();
    const year = today.year();
    const month = today.format("MMM");

    const fromDate = today.startOf("week").date();
    const toDate = today.endOf("week").date();

    return (
        <Toolbar>
            <Button onClick={e => setOpenView("year")}>
                <Typography variant="body2" component="span" gutterBottom>
                    Semaine {weeknumber} - {year}
                </Typography>
            </Button>
            <Button onClick={e => setOpenView("date")}>
                <Typography variant="h6" component="span" gutterBottom>
                    {fromDate} au {toDate} {month}
                </Typography>
            </Button>
        </Toolbar>
    );
};

export default withStyles(styles, {name: "MuiPickersToolbar"})(DateToolbar);
