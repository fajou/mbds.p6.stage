import Chart from 'chart.js';
import moment from "moment";

export function initChart(data) {
    const ctx = document.getElementById('nextshippingChart').getContext('2d');
    const timeFormat = 'MM/DD/YYYY HH:mm';

    function newDate(days) {
        return moment().add(days, 'd').toDate();
    }

    function newDateString(days) {
        return moment().add(days, 'd').format(timeFormat);
    }


    function randomScalingFactor(min, max) {
        const seed = Date.now();
        min = min === undefined ? 0 : min;
        max = max === undefined ? 1 : max;
        const res = (seed * 9301 + 49297) % 233280;
        return min + (res / 233280) * (max - min);
    }

    const color = Chart.helpers.color;
    const config = {
        type: 'line',
        data: {
            min: 0,
            labels: [
                newDateString(0),
                newDateString(1),
                newDateString(2),
                newDateString(3),
                newDateString(4),
                newDateString(5),
                newDateString(6)
            ],
            datasets: [{
                label: 'Articles',
                backgroundColor: 'rgb(153, 102, 255)',
                borderColor: 'rgb(54, 162, 235)',
                fill: false,    
                data: [6, 9, 3, 5, 16, 60, 13],
            }, {
                label: 'Commandes',
                backgroundColor: 'rgb(75, 192, 192)',
                borderColor: 'rgb(57,140,3)',
                fill: false,
                data: [2, 3, 2, 2, 6, 10, 3],
            }]
        },
        options: {
            title: {
                text: 'Totale commande & article de la journée',
                display: true,
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'linear',
                    unitStepSize: 30,
                    time: {
                        unit: 'minute',
                        displayFormats: {
                            minute: 'HH:mm'
                        }
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Date de commandes'
                    }
                }],
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Nombre commande & article'
                    }
                }]
            },
        }
    };
    new Chart(ctx, config);
}
