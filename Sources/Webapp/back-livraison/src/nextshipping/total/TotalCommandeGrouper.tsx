import * as React from 'react';
import {FC} from 'react';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import {useTranslate} from 'react-admin';

import CardWithIcon from './CardWithIcon';

interface Props {
    value?: number;
}

const TotalCommandeGrouper: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={GroupWorkIcon}
            title={translate('pos.dashboard.total_cmd_grouper')}
            subtitle={value}
        />
    );
};

export default TotalCommandeGrouper;
