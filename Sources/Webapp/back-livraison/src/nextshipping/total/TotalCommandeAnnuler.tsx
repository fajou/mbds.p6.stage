import * as React from 'react';
import {FC} from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import {useTranslate} from 'react-admin';
import CardWithIcon from "./CardWithIcon";

interface Props {
    value?: number;
}

const TotalCommandeAnnuler: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={CancelIcon}
            title={translate('pos.dashboard.total_cmd_annuler')}
            subtitle={value}
        />
    );
};

export default TotalCommandeAnnuler;
