import * as React from 'react';
import {FC} from 'react';
import {useTranslate} from 'react-admin';
import DepartureBoardIcon from '@material-ui/icons/DepartureBoard';
import CardWithIcon from './CardWithIcon';

interface Props {
    value?: number;
}

const TotalLivraisonEnattente: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={DepartureBoardIcon}
            title={translate('pos.dashboard.total_ship_pedding')}
            subtitle={value}
        />
    );
};

export default TotalLivraisonEnattente;
