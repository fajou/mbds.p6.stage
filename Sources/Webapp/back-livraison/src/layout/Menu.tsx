import * as React from 'react';
import {FC, useState} from 'react';
import {useSelector} from 'react-redux';
import SettingsIcon from '@material-ui/icons/Settings';
import LabelIcon from '@material-ui/icons/Label';
import CommuteTwoToneIcon from '@material-ui/icons/CommuteTwoTone';
import {Theme, useMediaQuery} from '@material-ui/core';
import {DashboardMenuItem, MenuItemLink, useTranslate} from 'react-admin';
import EmojiTransportationIcon from '@material-ui/icons/EmojiTransportation';
import FlagIcon from '@material-ui/icons/Flag';
import visitors from '../customers';
import orders from '../orders';
import invoices from '../invoices';
import products from '../products';
import categories from '../categories';
import DepartureBoardIcon from '@material-ui/icons/DepartureBoard';
import SubMenu from './SubMenu';
import {AppState} from '../types';
import {makeStyles} from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import {BarChart, LocalShipping} from "@material-ui/icons";
import RoomIcon from '@material-ui/icons/Room';

type MenuName = 'menuCatalog' | 'menuSales' | 'menuCustomers' | 'menuStatistics' | 'menuVehicules';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: '1em'
    },
    divider: {
        margin: theme.spacing(2, 0, 2, 0),
    },
    space: {
        margin: theme.spacing(2, 0, 0, 0),
    }
}));

interface Props {
    dense: boolean;
    logout: () => void;
    onMenuClick: () => void;
}

const Menu: FC<Props> = ({onMenuClick, dense, logout}) => {
    const classes = useStyles();
    const [state, setState] = useState({
        menuCatalog: false,
        menuSales: false,
        menuCustomers: false,
        menuVehicules: false,
        menuStatistics: false

    });
    const translate = useTranslate();
    const isXSmall = useMediaQuery((theme: Theme) =>
        theme.breakpoints.down('xs')
    );
    const open = useSelector((state: AppState) => state.admin.ui.sidebarOpen);
    useSelector((state: AppState) => state.theme); // force rerender on theme change

    const handleToggle = (menu: MenuName) => {
        setState(state => ({...state, [menu]: !state[menu]}));
    };

    return (
        <div className={classes.root}>
            {' '}
            <DashboardMenuItem onClick={onMenuClick} sidebarIsOpen={open}/>
            <MenuItemLink
                to={`/commands`}
                primaryText={translate(`resources.commands.name`, {
                    smart_count: 2,
                })}
                leftIcon={<orders.icon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <MenuItemLink
                to={`/deliveryItems`}
                primaryText={translate('resources.deliveries.name', {smart_count: 2,})}
                leftIcon={<EmojiTransportationIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <MenuItemLink
                to={`/nextshipping`}
                primaryText={translate('pos.menu.nextshipping')}
                leftIcon={<DepartureBoardIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <MenuItemLink
                to={`/categories`}
                primaryText={translate(`resources.categories.name`, {
                    smart_count: 2,
                })}
                leftIcon={<categories.icon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <Divider className={classes.divider} variant="middle"/>
            <MenuItemLink
                to={`/vehicles`}
                primaryText={translate('pos.menu.vehicles')}
                leftIcon={<CommuteTwoToneIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <MenuItemLink
                to={`/relaies`}
                primaryText={translate(`resources.relaies.name`, {
                    smart_count: 2,
                })}
                leftIcon={<FlagIcon/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            <MenuItemLink
                to={`/statistique`}
                primaryText={translate('pos.menu.statistics', {
                    smart_count: 2,
                })}
                leftIcon={<BarChart/>}
                onClick={onMenuClick}
                sidebarIsOpen={open}
                dense={dense}
            />
            {isXSmall && (
                <MenuItemLink
                    to="/configuration"
                    primaryText={translate('pos.configuration')}
                    leftIcon={<SettingsIcon/>}
                    onClick={onMenuClick}
                    sidebarIsOpen={open}
                    dense={dense}
                />
            )}
            {isXSmall && logout}
        </div>
    );
};

export default Menu;
