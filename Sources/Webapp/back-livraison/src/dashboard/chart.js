import Chart from 'chart.js';

export default function initChart(data) {
    const colors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    const config = {
        type: 'line',
        data: {
            labels: [
                'Janvier',
                'Février',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juillet',
                'Août',
                'Septembre',
                'Octobre',
                'Novembre',
                'Décembre'],
            datasets: [{
                label: 'Articles',
                backgroundColor: colors.orange,
                borderColor: colors.green,
                data: [2450, 2600, 2550, 2475, 2620, 2740, 2515],
                fill: false,
            }, {
                label: 'Commandes',
                backgroundColor: colors.red,
                borderColor: colors.blue,
                data: [1350, 2500, 2450, 2375, 1420, 2340, 2405],
                fill: false,
            }, {
                label: 'Livraisons',
                fill: false,
                backgroundColor: colors.blue,
                borderColor: colors.red,
                data: [230, 233, 322, 419, 511, 439, 430],
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Statistique annuelle des livraisons'
            },
            scales: {
                yAxes: [{
                    gridLines: {
                        drawBorder: true,
                        color: ['rgb(201, 203, 207)']
                    }
                }]
            }
        }
    };
    const ctx = document.getElementById('livraisonAnnuelle').getContext('2d');
    let chart = new Chart(ctx, config);
}