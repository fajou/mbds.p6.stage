import * as React from 'react';
import {FC} from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {useTranslate} from 'react-admin';
import {Customer, Order} from '../types';
import {Box} from "@material-ui/core";

interface Props {
    orders?: Order[];
    customers?: { [key: string]: Customer };
}

const useStyles = makeStyles(theme => ({
    root: {
        flex: 1,
    },
    cost: {
        marginRight: '1em',
        color: theme.palette.text.primary,
    },
}));

const PendingOrders: FC<Props> = ({orders = []}) => {
    const classes = useStyles();
    const translate = useTranslate();

    return (
        <Card className={classes.root}>
            <CardHeader title={translate('pos.dashboard.pending_orders')}/>
            <List dense={true}>
                {orders.map(order => (
                    <ListItem
                        key={order.id}
                        button
                        component={Link}
                        to={`/commands/${order.id}`}>
                        <ListItemAvatar>
                            {order.customer ? <Avatar src={`${order.customer.avatar}?size=32x32`}/> : <Avatar/>}
                        </ListItemAvatar>
                        <ListItemText
                            primary={(order.updatedDate ? order.updatedDate : order.createdDate).toLocaleString('en-GB')}
                            secondary={translate('pos.dashboard.order.items', {
                                smart_count: order.items.length,
                                nb_items: order.items.length,
                                customer_name: order.customer ? `${order.customer.firstName} ${order.customer.lastName}` : '',
                            })}
                        />
                        <ListItemSecondaryAction>
                            <Box fontWeight={600} fontSize={14} m={1}>
                               <span className={classes.cost}>
                                    {order.grandTotal.toLocaleString(undefined, {
                                        style: 'currency',
                                        currency: 'MGA',
                                        minimumFractionDigits: 0,
                                        maximumFractionDigits: 0,
                                    })}
                                </span>
                            </Box>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </List>
        </Card>
    );
};

export default PendingOrders;
