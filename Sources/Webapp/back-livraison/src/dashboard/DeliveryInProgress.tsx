import * as React from 'react';
import {useEffect, useState} from 'react';
import List from '@material-ui/core/List';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import {useTranslate} from 'react-admin';

import CardWithIcon from './CardWithIcon';
import {VehicleDeliveryInfo} from '../types';
import {Button, CardContent} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import {makeStyles} from "@material-ui/core/styles";
import {apiUrlLivrs} from "../dataProvider/ConstUrl";
import {useHistory} from "react-router";

const useStyles = makeStyles({
    root: {
        margin: 8
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    details: {
        fontSize: 11,
        fontWeight: "bold"
    },
    card_content: {
        paddingBottom: 0
    }
});
const DeliveryInProgress = () => {
    const translate = useTranslate();
    const classes = useStyles();
    const [deliveries, setDeliveries] = useState([]);
    const history = useHistory();

    function fetchDeliveries() {
        fetch(`${apiUrlLivrs}/delivering`)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setDeliveries(data);
                }
            });
    }

    useEffect(fetchDeliveries, [setDeliveries]);


    if (!deliveries)
        return null;
    const bull = <span className={classes.bullet}>•</span>;

    const handleClick = (id: string) => {
        history.push("/deliveryItems/" + id);
    }

    const delivery = (item: VehicleDeliveryInfo, key: number) => {
        return (
            <Card key={key} className={classes.root} variant="outlined">
                <table style={{padding: 0, marginBottom: 0}}>
                    <thead>
                    <tr>
                        <th>
                            <Typography variant="body2">
                                Véhicule
                            </Typography>
                        </th>
                        <td>
                            <Typography variant="caption" color="secondary">
                                {item.delivery.vehicle?.brand} {bull}
                                {item.delivery.vehicle?.numImm}
                            </Typography>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>
                            <Typography variant="body2">
                                Chauffeur
                            </Typography>
                        </th>
                        <td>
                            <Typography variant="body2">
                                {item.delivery.vehicle?.driver?.name} {bull} {item.delivery.vehicle?.driver?.age} ans
                            </Typography>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <Typography variant="body2" color="primary">
                                Retours dans
                            </Typography>
                        </th>
                        <td>
                            <Typography color="primary" variant="body2">
                                {item.histoItem?.estimDuration?.duraText ?? "Non definie"} à {item.histoItem?.estimDuration?.distText}
                            </Typography>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <Typography variant="body2" color="textSecondary">
                                Articles
                            </Typography>
                        </th>
                        <td>
                            <Typography variant="body2">
                                {translate('pos.dashboard.items', {
                                    smart_count: item.delivery.totalItems,
                                    nb_items: item.delivery.totalItems
                                })}
                            </Typography>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <CardActions>
                    <Button onClick={() => handleClick(item.id)} className={classes.details} size="small"
                            variant="outlined">Détails</Button>
                </CardActions>
            </Card>
        );
    };

    const nb = deliveries ? deliveries.reduce((nb: number) => ++nb, 0) : 0;
    return (
        <CardWithIcon to="/deliveries"
                      icon={LocalShippingIcon}
                      title={translate('pos.dashboard.delivery_inprogress')}
                      subtitle={nb}>
            <List>
                {deliveries ? deliveries.map(delivery) : null}
            </List>
        </CardWithIcon>
    );
};

export default DeliveryInProgress;
