import React, {FC, useCallback, useEffect, useState,} from 'react';
import {useDataProvider, useVersion} from 'react-admin';
import {Theme, useMediaQuery} from '@material-ui/core';
import MonthlyRevenue from './MonthlyRevenue';
import NbNewOrders from './NbNewOrders';
import PendingOrders from './PendingOrders';
import PedingDelivery from './PedingDelivery';
import DeliveryInProgress from './DeliveryInProgress';
import DeliveryChart from './DeliveryChart';

import {Customer, DeliveryItem, Order, Review} from '../types';


interface OrderStats {
    revenue: number;
    nbNewOrders: number;
    pendingOrders: Order[];
}

interface CustomerData {
    [key: string]: Customer;
}

interface State {
    nbNewOrders?: number;
    nbPendingDelivery?: number;
    pendingOrders?: Order[];
    pendingOrdersCustomers?: CustomerData;
    pendingDeliveries?: DeliveryItem[];
    pendingReviewsCustomers?: CustomerData;
    recentOrders?: Order[];
    revenue?: number;
}

const styles = {
    flex: {display: 'flex'},
    flexColumn: {display: 'flex', flexDirection: 'column'},
    leftCol: {flex: 1, marginRight: '0.5em'},
    rightCol: {flex: 1, marginLeft: '0.5em'},
    singleCol: {marginTop: '1em', marginBottom: '1em'},
};
const Spacer = () => <span style={{width: '1em'}}/>;

const Dashboard: FC = () => {
    const [state, setState] = useState<State>({});
    const version = useVersion();
    const dataProvider = useDataProvider();
    useMediaQuery((theme: Theme) =>
        theme.breakpoints.down('xs')
    );
    useMediaQuery((theme: Theme) =>
        theme.breakpoints.down('md')
    );
    const fetchOrders = useCallback(async () => {
        const aMonthAgo = new Date();
        aMonthAgo.setDate(aMonthAgo.getDate() - 30); //30 days

        const {data: recentOrders} = await dataProvider.getList('commands', {
            filter: {date_gte: aMonthAgo.toISOString()},
            sort: {field: 'date', order: 'DESC'},
            pagination: {page: 1, perPage: 6},
        });

        const aggregations = recentOrders.reduce((stats: OrderStats, order: Order) => {
                if (order.status === 'ADDED') {
                    stats.revenue += order.grandTotal;
                    stats.nbNewOrders++;
                    stats.pendingOrders.push(order);
                }
                return stats;
            },
            {
                revenue: 0,
                nbNewOrders: 0,
                pendingOrders: [],
            }
        );

        setState(state => ({
            ...state,
            recentOrders,
            revenue: aggregations.revenue.toLocaleString(undefined, {
                style: 'currency',
                currency: 'MGA',
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
            }),
            nbNewOrders: aggregations.nbNewOrders,
            pendingOrders: aggregations.pendingOrders,
        }));

    }, [dataProvider]);

    useEffect(() => {
        fetchOrders().then();
    }, [version]); // eslint-disable-line react-hooks/exhaustive-deps

    const {
        nbNewOrders,
        pendingOrders,
        pendingDeliveries,
        revenue,
    } = state;

    return <>
        <div style={styles.flex}>
            <div style={styles.leftCol}>
                <div style={styles.flex}>
                    <MonthlyRevenue value={revenue}/>
                    <Spacer/>
                    <NbNewOrders value={nbNewOrders}/>
                </div>
                <div style={styles.singleCol}>
                    <PendingOrders orders={pendingOrders}/>
                </div>
            </div>
            <div style={styles.rightCol}>
                <div style={styles.flex}>
                    <PedingDelivery/>
                    <Spacer/>
                    <DeliveryInProgress/>
                </div>
            </div>
        </div>
        <div style={styles.singleCol}>
            <DeliveryChart orders={pendingDeliveries}/>
        </div>
    </>
};

export default Dashboard;
