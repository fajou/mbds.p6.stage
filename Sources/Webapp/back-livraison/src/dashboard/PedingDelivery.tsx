import * as React from 'react';
import {FC, useEffect, useState} from 'react';
import {Button, CardContent, List,} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import DepartureBoardIcon from '@material-ui/icons/DepartureBoard';
import {useTranslate} from 'react-admin';
import AccessTimeIcon from '@material-ui/icons/AccessTime';

import CardWithIcon from './CardWithIcon';
import {Delivery, DeliveryItem, VehicleDeliveryInfo} from '../types';
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import {apiUrlLivrs} from "../dataProvider/ConstUrl";
import {useHistory} from "react-router-dom";

interface Props {
    items?: DeliveryItem[];
    nbs?: number;
}

const useStyles = makeStyles(theme => ({
    avatar: {
        background: theme.palette.background.paper,
    },
    listItemText: {
        overflowY: 'hidden',
        height: '4em',
        display: '-webkit-box',
        WebkitLineClamp: 2,
        WebkitBoxOrient: 'vertical',
    },
    root: {
        margin: 8
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    details: {
        fontSize: 11,
        fontWeight: "bold"
    },
    card_content: {
        paddingBottom: 0
    }
}));

const PedingDelivery: FC<Props> = () => {
    const classes = useStyles();
    const translate = useTranslate();
    const [pedding, setPedding] = useState([]);
    const history = useHistory();

    function fetchDeliveries() {
        fetch(`${apiUrlLivrs}/pedding`)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setPedding(data);
                }
            });
    }

    useEffect(fetchDeliveries, [setPedding]);
    if (!pedding)
        return null;

    const handleClick = (id: string) => {
        history.push("/nextshipping");
    }

    const bull = <span className={classes.bullet}>•</span>;
    const delivery = (item: VehicleDeliveryInfo, key: number) => (
        <Card key={key} className={classes.root} variant="outlined">
            <table style={{padding: 0, marginBottom: 0}}>
                <thead>
                <tr>
                    <th>
                        <Typography variant="body2">
                            Véhicule
                        </Typography>
                    </th>
                    <td>
                        <Typography variant="caption" color="secondary">
                            {item.delivery.vehicle?.brand} {bull}
                            {item.delivery.vehicle?.numImm}
                        </Typography>
                    </td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>
                        <Typography variant="body2">
                            Chauffeur
                        </Typography>
                    </th>
                    <td>
                        <Typography variant="body2">
                            {item.delivery.vehicle?.driver?.name} {bull} {item.delivery.vehicle?.driver?.age} ans
                        </Typography>
                    </td>
                </tr>
                <tr>
                    <th>
                        <Typography variant="body2" color="primary">
                            Depart environs
                        </Typography>
                    </th>
                    <td>
                        <Typography color="primary" variant="body2">
                            {item?.histoItem?.estimDepartDate ?? "Non definie"} pour {item.histoItem?.estimDuration?.distText}
                        </Typography>
                    </td>
                </tr>
                <tr>
                    <th>
                        <Typography variant="body2" color="textSecondary">
                            Articles
                        </Typography>
                    </th>
                    <td>
                        <Typography variant="body2">
                            {translate('pos.dashboard.items', {
                                smart_count: item.delivery.totalItems,
                                nb_items: item.delivery.totalItems
                            })}
                        </Typography>
                    </td>
                </tr>
                </tbody>
            </table>
            <CardActions>
                <Button onClick={() => handleClick(item.id)} className={classes.details} size="small"
                        variant="outlined">Details</Button>
            </CardActions>
        </Card>
    );

    return (
        <CardWithIcon to="/nextshipping"
                      icon={DepartureBoardIcon}
                      title={translate('pos.dashboard.pending_delivery')}
                      subtitle={pedding.length}>
            <List>
                {pedding ? pedding.map(delivery) : null}
            </List>
        </CardWithIcon>
    );
};

export default PedingDelivery;
