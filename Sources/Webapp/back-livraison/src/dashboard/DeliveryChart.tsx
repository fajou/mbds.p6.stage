import * as React from 'react';
import {FC, useEffect} from 'react';
import {Button, Card, CardContent, CardHeader, Grid} from '@material-ui/core';
import {useTranslate} from 'react-admin';

import {DeliveryItem} from '../types';
import initChart from "./chart";
import YearRange from "./YearRange";
import {GetApp} from "@material-ui/icons";

const lastDay = new Date(new Date().toDateString()).getTime();
const oneDay = 24 * 60 * 60 * 1000;
const lastMonthDays = Array.from(
    {length: 30},
    (_, i) => lastDay - i * oneDay
).reverse();
const aMonthAgo = new Date();
aMonthAgo.setDate(aMonthAgo.getDate() - 30);

const dateFormatter = (date: number): string => new Date(date).toLocaleDateString();

const aggregateOrdersByDay = (orders: DeliveryItem[]): { [key: number]: number } =>
    orders.filter((order: DeliveryItem) => order.delivery.status !== 'UPDATED' && order.delivery.status !== 'CREATED')
        .reduce(
            (acc, curr) => {
                const day = new Date(new Date(curr.delivery.createdDate).toDateString()).getTime();
                if (!acc[day]) {
                    acc[day] = 0;
                }
                acc[day] += curr.delivery.grandTotal;
                return acc;
            },
            {} as { [key: string]: number }
        );

const getRevenuePerDay = (orders: DeliveryItem[]): TotalByDay[] => {
    const daysWithRevenue = aggregateOrdersByDay(orders);
    return lastMonthDays.map(date => ({
        date,
        total: daysWithRevenue[date] || 0,
    }));
};

const DeliveryChart: FC<{ orders?: DeliveryItem[] }> = ({orders}) => {
    const translate = useTranslate();
    useEffect(initState, []);

    function initState() {
        initChart(orders);
    }

    return (
        <Card>
            <CardHeader title={translate('pos.dashboard.month_history')}/>
            <CardContent>
                <Grid container>
                    <Grid item xs={11}>
                        <YearRange/>
                    </Grid>
                    <Grid item xs={1}>
                        <Button startIcon={<GetApp/>}
                                variant="text" size="small" color="primary">
                            Exporter
                        </Button>
                    </Grid>
                </Grid>
                <table className="table-state">
                    <thead>
                    <tr>
                        <th>Libellé / Mois</th>
                        <th>Janvier</th>
                        <th>Février</th>
                        <th>Mars</th>
                        <th>Avril</th>
                        <th>Mai</th>
                        <th>Juin</th>
                        <th>Juillet</th>
                        <th>Août</th>
                        <th>Septembre</th>
                        <th>Octobre</th>
                        <th>Novembre</th>
                        <th>Décembre</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Livraisons</th>
                        <td>230</td>
                        <td>233</td>
                        <td>322</td>
                        <td>419</td>
                        <td>511</td>
                        <td>439</td>
                        <td>430</td>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <th>2584</th>
                    </tr>
                    <tr>
                        <th>Commandes</th>
                        <td>1350</td>
                        <td>2500</td>
                        <td>2450</td>
                        <td>2375</td>
                        <td>1420</td>
                        <td>2340</td>
                        <td>2405</td>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <th>14840</th>
                    </tr>
                    <tr>
                        <th>Articles</th>
                        <td>2450</td>
                        <td>2600</td>
                        <td>2550</td>
                        <td>2475</td>
                        <td>2620</td>
                        <td>2740</td>
                        <td>2515</td>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <td/>
                        <th>17950</th>
                    </tr>
                    </tbody>
                </table>
                <div className="chart-container">
                    <canvas id="livraisonAnnuelle" height={90}/>
                </div>
            </CardContent>
        </Card>
    );
};

interface TotalByDay {
    date: number;
    total: number;
}

export default DeliveryChart;
