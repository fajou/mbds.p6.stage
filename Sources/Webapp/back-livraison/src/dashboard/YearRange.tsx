import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            marginBottom: theme.spacing(2),
            minWidth: 150,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        },
    }),
);

export default function YearRange() {
    const classes = useStyles();
    const [state, setState] = React.useState<{ age: string | number; name: string }>({
        age: '',
        name: 'hai',
    });

    const handleChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
        const name = event.target.name as keyof typeof state;
        setState({
            ...state,
            [name]: event.target.value,
        });
    };

    const years = (min: number, max: number) => {
        const ranges = [];
        for (let i = max; min <= i; i--) {
            ranges.push(i);
        }
        return ranges;
    }

    return (
        <div>
            <FormControl size="small" variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="filled-year-native-simple">Année</InputLabel>
                <Select native
                        value={state.age}
                        onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'filled-year-native-simple',
                        }}>
                    {years(2015, 2020).map(((value, index) => <option key={index} value={value}>{value}</option>))}
                </Select>
            </FormControl>
        </div>
    );
}
