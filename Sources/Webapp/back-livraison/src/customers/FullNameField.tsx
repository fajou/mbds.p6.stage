import * as React from 'react';
import {FC, memo} from 'react';
import {makeStyles} from '@material-ui/core/styles';

import AvatarField from './AvatarField';
import {FieldProps} from '../types';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'nowrap',
        alignItems: 'center',
    },
    avatar: {
        marginRight: theme.spacing(1),
        marginTop: -theme.spacing(0.5),
        marginBottom: -theme.spacing(0.5),
    },
}));

interface Props extends FieldProps<any> {
    size?: string;
}

const FullNameField: FC<Props> = ({record, size}) => {
    const classes = useStyles();
    const customer = record?.customer ?? {};
    return record ? (
        <div className={classes.root}>
            <AvatarField className={classes.avatar}
                         record={customer}
                         size={size}/>
            {customer.firstName} {customer.lastName}
        </div>
    ) : null;
};

FullNameField.defaultProps = {
    addLabel: true,
};

export default memo<Props>(FullNameField);
