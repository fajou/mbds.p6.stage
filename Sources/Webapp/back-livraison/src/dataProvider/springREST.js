import {fetchUtils} from 'react-admin';
import {stringify} from 'query-string';
import {apiUrl, apiUrlData} from "./ConstUrl";

const httpClient = fetchUtils.fetchJson;

let headers = new Headers();
headers.append("Content-Type", "application/json");
headers.append("Access-Control-Allow-Origin", "*");

const path = (resource, params) => {
    if (resource === "deliveryItems" && params.filter.target === "vehicle_id")
        return "deliveryItems/search/vehicle";

    if (resource === "deliveryItems" && params.filter.target === "delv_status")
        return "deliveryItems/search/findAllByDelivery_Status";

    console.log(params);
    if (resource === "commands" && params.filter.status)
        return "commands/search/status";

    if (resource === "articles")
        return "articles/search/categorie";

    if (resource === "articles" && params.target === "categorie_id")
        return "articles/search/by-catg-id";

    return resource;
}

export default {
    getList: (resource, params) => {
        const {page, perPage} = params.pagination;
        const {field, order} = params.sort;
        let url;
        if ("vehicles" === resource && params.filter)
            url = `${apiUrlData}/${path(resource, params)}?page=${page - 1}&size=${perPage}`;
        else
            url = `${apiUrl}/${path(resource, params)}?page=${page - 1}&size=${perPage}`;

        if (params?.filter) for (const [key, value] of Object.entries(params.filter)) {
            url += `&${key}=${value}`;
        }
        if (params?.sort) {
            url += `&sort=${field},${order}`;
        }

        return httpClient(url).then(({json}) => {
            const res = resource.split('/');
            if (json.content)
                return ({
                    data: json.content,
                    total: json.totalElements
                })
            return ({
                data: json._embedded[res[0]] ?? [],
                total: json.page['totalElements'],
            });
        }).catch(err => {
            console.log(err);
        });
    },

    getOne: (resource, params) =>
        httpClient(`${apiUrl}/${resource}/${params.id}`).then(({json}) => ({
            data: json,
        })),

    getMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({id: params.ids}),
        };
        const url = `${apiUrl}/${resource}?${stringify(query)}`;
        return httpClient(url).then(({json}) => ({data: json}));
    },

    getManyReference: (resource, params) => {
        const {page, perPage} = params.pagination;
        const {field, order} = params.sort;
        const query = {
            sort: JSON.stringify([field, order]),
            range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
            filter: JSON.stringify({
                ...params.filter,
                [params.target]: params.id,
            }),
        };

        let url = `${apiUrl}/${path(resource, params)}?page=${page - 1}&size=${perPage}`;
        if (params.target === "categorie_id") {
            url += `&id=${params.id}`;
        }

        if (params?.sort) {
            url += `&sort=${field},${order}`;
        }

        return httpClient(url).then(({headers, json}) => ({
            data: json._embedded[resource],
            total: json.page['totalElements'],
        }));
    },

    update: (resource, params) => {
        return httpClient(`${apiUrl}/${resource}`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params.data)
        }).then(({json}) => {
            console.log(json);
            return ({data: json});
        });
    },

    updateMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({id: params.ids}),
        };
        return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params.data),
        }).then(({json}) => ({data: json}));
    },

    create: (resource, params) => httpClient(`${apiUrl}/${resource}`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(params.data),
    }).then(({json}) => ({
        data: {...params.data, id: json.id},
    })),

    delete: (resource, params) => httpClient(`${apiUrl}/${resource}/${params.id}`, {
        method: 'DELETE',
    }).then(({json}) => ({data: json})),

    deleteMany: (resource, params) => {
        const query = {
            filter: JSON.stringify({id: params.ids}),
        };
        return httpClient(`${apiUrl}/${resource}?${stringify(query)}`, {
            method: 'DELETE',
            headers: headers,
            body: JSON.stringify(params.data),
        }).then(({json}) => ({data: json}));
    }
};