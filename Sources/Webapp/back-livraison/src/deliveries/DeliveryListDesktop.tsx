import * as React from 'react';
import {FC} from 'react';
import {Datagrid, TextField} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';

import {DatagridProps} from '../types';
import {Identifier} from 'ra-core';
import DriverLinkField from "./fields/DriverLinkField";
import ArticleField from "./fields/ArticleField";
import WeightField from "./fields/WeightField";

const useListStyles = makeStyles({
    headerRow: {
        borderLeftColor: 'white',
        borderLeftWidth: 5,
        borderLeftStyle: 'solid',
    },
    headerCell: {
        padding: '6px 8px 6px 8px',
    },
    rowCell: {
        padding: '6px 8px 6px 8px',
    },
    comment: {
        maxWidth: '18em',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    },
});

export interface DeliveryListDesktopProps extends DatagridProps {
    selectedRow?: Identifier;
}

const DeliveryListDesktop: FC<DeliveryListDesktopProps> = ({selectedRow, ...props}) => {
    const classes = useListStyles();
    return (
        <Datagrid rowClick="edit"
                  classes={{
                      headerRow: classes.headerRow,
                      headerCell: classes.headerCell,
                      rowCell: classes.rowCell,
                  }}
                  optimized
                  {...props}>
            <TextField label="Départ" source="delivery.departDate"/>
            <TextField label="Date d'arrivée" source="delivery.arrivalDate"/>
            <DriverLinkField source="delivery.vehicle"/>
            <ArticleField source="vehicle.id" label="Articles"/>
            <WeightField source="totalWeight" label="Poids"/>
            <TextField source="delivery.status" label="Statut"/>
        </Datagrid>
    );
};

export default DeliveryListDesktop;
