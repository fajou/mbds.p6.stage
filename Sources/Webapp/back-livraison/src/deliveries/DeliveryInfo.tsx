import * as React from 'react';
import {FC} from 'react';
import {SimpleForm, useEditController, useTranslate,} from 'react-admin';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

import DeliveryEditToolbar from './DeliveryEditToolbar';
import {EditComponentProps} from '../types';
import DeliveryCommands from "./DeliveryCommands";
import DriverInfo from "./DriverInfo";
import Button from "@material-ui/core/Button";
import MapIcon from "@material-ui/icons/Map";
import ViewListIcon from "@material-ui/icons/ViewList";
import {Grid} from "@material-ui/core";
import GpsInfo from "../gps/GpsInfo";

const useStyles = makeStyles(theme => ({
    root: {
        paddingTop: 40,
    },
    btnWidth: {
        width: 68,
        height: 30
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: '1em',
    },
    form: {
        [theme.breakpoints.up('xs')]: {
            width: 400,
        },
        [theme.breakpoints.down('xs')]: {
            width: '100vw',
            marginTop: -30,
        },
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    inlineField: {
        display: 'inline-block',
        width: '50%',
    },
    card_content: {
        paddingBottom: 0
    },
    details: {
        fontSize: 11,
        fontWeight: "bold"
    }
}));

interface Props extends EditComponentProps {
    onCancel: () => void;
    click: () => void;
    show: boolean;
    id: string;
}

const DeliveryInfo: FC<Props> = ({onCancel, ...props}) => {
    const classes = useStyles();
    const edit = useEditController(props);
    const translate = useTranslate();

    if (!edit.record) {
        return null;
    }

    return (
        <div className={classes.root}>
            <div className={classes.title}>
                {edit.record.delivery.status === "GROUPED" ?
                    <Grid item sm={1}>
                        <Button className={classes.btnWidth} onClick={props.click} variant="contained" size="small"
                                color="primary">
                            {props.show ? <><MapIcon/> MAP </> : <><ViewListIcon/> LISTE </>}
                        </Button>
                    </Grid> : ""}
                <Typography variant="h6">
                    {translate('resources.deliveries.details')}
                </Typography>
                <IconButton onClick={onCancel}>
                    <CloseIcon/>
                </IconButton>
            </div>
            <SimpleForm
                className={classes.form}
                basePath={edit.basePath}
                record={edit.record}
                save={edit.save}
                version={edit.version}
                redirect="list"
                resource="deliveryItems"
                toolbar={<DeliveryEditToolbar/>}>
                <DriverInfo />
                {props.show ? <DeliveryCommands/> : <GpsInfo/>}
            </SimpleForm>
        </div>
    );
};

export default DeliveryInfo;
