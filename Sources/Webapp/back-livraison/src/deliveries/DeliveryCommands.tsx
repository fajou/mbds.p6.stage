import {FC} from "react";
import {Command, DeliveryItem, FieldProps, VehicleDeliveryInfo} from "../types";
import {useTranslate} from "react-admin";
import * as React from "react";
import {Button, CardContent, Chip, Grid, List} from "@material-ui/core";
import CardWithIcon from "../dashboard/CardWithIcon";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import ListAltIcon from '@material-ui/icons/ListAlt';
import {makeStyles} from "@material-ui/core/styles";
import FullNameField from "./fields/CustomerInfoField";

const useStyles = makeStyles(theme => ({
    avatar: {
        background: theme.palette.background.paper,
    },
    listItemText: {
        overflowY: 'hidden',
        height: '4em',
        display: '-webkit-box',
        WebkitLineClamp: 2,
        WebkitBoxOrient: 'vertical',
    },
    root: {
        marginBottom: 8,
        marginTop: 8
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    details: {
        fontSize: 11,
        fontWeight: "bold"
    },
    card_content: {
        paddingBottom: 0
    },
    btnWidth: {
        width: 35,
        height: 25,
        fontSize: 12
    },
}));

const DeliveryCommands: FC<FieldProps<VehicleDeliveryInfo>> = ({record}) => {
    const translate = useTranslate();
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;

    const command = (item: Command, key: number) => {
        const numbers = item.customer.mobileNumbers;
        return (
            <Card key={key} className={classes.root} variant="outlined">
                <CardContent className={classes.card_content}>
                    <FullNameField record={item}/>
                    <br/>
                    <Typography className={classes.title} color="primary" gutterBottom>
                        Distance {item.customer.distance.distText} {bull} Durée {item.customer.distance.duraText} {bull} {translate('pos.dashboard.items', {
                        smart_count: item.totalItems,
                        nb_items: item.totalItems
                    })}
                    </Typography>
                    <Typography className={classes.title} gutterBottom>
                        Tel: <br/>
                        {numbers?.airtel} {bull} {numbers?.orange} {bull} {numbers?.telma}
                    </Typography>
                    <Typography className={classes.title} variant="caption" component="p" color="inherit" gutterBottom>
                        Commandé le {item.dateCommand}.
                    </Typography>
                </CardContent>
                <CardActions>
                    <Chip size="small" className={classes.details} variant={"default"} label={item.status}/>
                </CardActions>
            </Card>
        );
    };

    return record ? (
        <>
            <CardWithIcon to="/commands"
                          icon={ListAltIcon}
                          title={translate('pos.dashboard.command_nbs')}
                          subtitle={record.delivery.orders.length}/>
            <List>
                {record ? record.delivery.orders.map(command) : <span>Chargement...</span>}
            </List>
        </>
    ) : null;
};

export default DeliveryCommands;