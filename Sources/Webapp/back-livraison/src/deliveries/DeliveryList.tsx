import * as React from 'react';
import {FC, Fragment, useCallback, useState} from 'react';
import classnames from 'classnames';
import {BulkDeleteButton, List} from 'react-admin';
import {Route, RouteChildrenProps, useHistory} from 'react-router-dom';
import {Drawer, Grid} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import BulkAcceptButton from './BulkAcceptButton';
import BulkRejectButton from './BulkRejectButton';
import DeliveryListDesktop from './DeliveryListDesktop';
import DeliveryFilter from './DeliveryFilter';
import DeliveryInfo from './DeliveryInfo';
import {BulkActionProps, ListComponentProps} from '../types';
import Gps from "../gps/Gps";
import ViewListIcon from "@material-ui/icons/ViewList";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const ReviewsBulkActionButtons: FC<BulkActionProps> = props => (
    <Fragment>
        <BulkAcceptButton {...props} />
        <BulkRejectButton {...props} />
        <BulkDeleteButton {...props} />
    </Fragment>
);

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    btnWidth: {
        width: 68,
        height: 30
    },
    textCenter: {
        textAlign: "center"
    },
    list: {
        flexGrow: 1,
        transition: theme.transitions.create(['all'], {
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginRight: 0,
    },
    listWithDrawer: {
        marginRight: 400,
    },
    drawerPaper: {
        zIndex: 100,
    },
}));

const DeliveryList: FC<ListComponentProps<{ id: string }>> = props => {
    const classes = useStyles();
    const history = useHistory();
    const [show, setShow] = useState(true);

    const handleClose = useCallback(() => {
        history.push('/deliveryItems');
    }, [history]);

    const click = function () {
        setShow(!show);
    };

    return (
        <>
            <Grid container>
                {!show ? <Grid item sm={1}>
                    <Button className={classes.btnWidth} onClick={click} variant="contained" size="small"
                            color="primary">
                        <><ViewListIcon/> LISTE</>
                    </Button>
                </Grid> : ""}
                <Grid item sm={11}>
                    <Typography variant="h5" component="h5">
                        {!show ? `Traçage du véhicule de livraison` : `Suivi et rapport des livraisons`}
                    </Typography>
                </Grid>
            </Grid>
            <div className={classes.root}>
                <Route id="1" path="/deliveryItems/:id">
                    {({match}: RouteChildrenProps<{ id: string }>) => {
                        const isMatch = !!(match && match.params && match.params.id !== 'create');
                        return (
                            <Fragment>
                                {show ?
                                    <List
                                        {...props}
                                        className={classnames(classes.list, {
                                            [classes.listWithDrawer]: isMatch,
                                        })}
                                        bulkActionButtons={<ReviewsBulkActionButtons/>}
                                        filters={<DeliveryFilter/>}
                                        perPage={10}
                                        filter={{status: 'GROUPED', target: 'delv_status'}}
                                        sort={{field: 'createdDate', order: 'DESC'}}
                                    >
                                        <DeliveryListDesktop/>
                                    </List> : <Gps id={match?.params.id}/>}
                                <Drawer
                                    variant="persistent"
                                    open={isMatch}
                                    anchor="right"
                                    onClose={handleClose}
                                    classes={{
                                        paper: classes.drawerPaper,
                                    }}
                                >
                                    {isMatch ? (<DeliveryInfo id={(match as any).params.id}
                                                              onCancel={handleClose}
                                                              click={click}
                                                              show={show}
                                                              {...props}/>
                                    ) : null}
                                </Drawer>
                            </Fragment>
                        );
                    }}
                </Route>
            </div>
        </>
    );
};

export default DeliveryList;
