import * as React from 'react';
import {FC, memo} from 'react';
import {makeStyles} from '@material-ui/core/styles';

import DriverAvatarField from './DriverAvatarField';
import {DeliveryItem, Driver, FieldProps, Vehicle} from '../../types';
import {Typography} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'nowrap',
        alignItems: 'center',
    },
    avatar: {
        marginRight: theme.spacing(1),
        marginTop: -theme.spacing(0.5),
        marginBottom: -theme.spacing(0.5),
    },
}));

interface Props extends FieldProps<Driver> {
    size?: string;
}

const DrivierNameField: FC<Props> = ({record, size}) => {
    const classes = useStyles();
    return record ? (
        <div className={classes.root}>
            <DriverAvatarField className={classes.avatar}
                               record={record}
                               size={size}/>
            {record.name}
        </div>
    ) : null;
};

DrivierNameField.defaultProps = {
    source: 'driver',
    label: 'resources.customers.fields.name',
};

export default memo<Props>(DrivierNameField);
