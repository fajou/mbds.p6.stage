import * as React from 'react';
import {FC} from 'react';
import {Link} from 'react-admin';

import DrivierNameField from './DrivierNameField';
import {DeliveryItem, FieldProps} from '../../types';

const DriverLinkField: FC<FieldProps<DeliveryItem>> = props => {
    const driver = props.record?.delivery?.vehicle?.driver;
    return driver ? (
        <Link to={`/driveres/${driver.id}`}>
            <DrivierNameField record={driver} />
        </Link>
    ) : null;
};

DriverLinkField.defaultProps = {
    label: "Conducteur",
    addLabel: true
};

export default DriverLinkField;
