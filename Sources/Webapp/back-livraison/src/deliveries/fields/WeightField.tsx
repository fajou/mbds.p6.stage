import * as React from 'react';
import {useTranslate} from 'react-admin';
import {DeliveryItem, FieldProps} from '../../types';
import {FC} from "react";

function render(record: DeliveryItem) {
    return <span>{`${record.delivery.totalWeight} Kg`}</span>
}

const WeightField: FC<FieldProps<DeliveryItem>> = ({record}) => {
    const translate = useTranslate();
    return record ? (
        <span>
            {translate('pos.dashboard.weight', {
                smart_count: record.delivery.totalWeight,
                nb_items: record.delivery.totalWeight
            })}
        </span>
    ) : null;
};

export default WeightField;
