import * as React from 'react';
import {FC} from 'react';
import {DeliveryItem, FieldProps} from "../../types";
import {useTranslate} from "react-admin";

const ArticleField: FC<FieldProps<DeliveryItem>> = ({record}) => {
    const translate = useTranslate();
    return record ? (
        <span>
            {translate('pos.dashboard.items', {
                smart_count: record.delivery.totalItems,
                nb_items: record.delivery.totalItems
            })}
        </span>
    ) : null;
};

export default ArticleField;
