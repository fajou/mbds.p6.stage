import * as React from 'react';
import {FC} from 'react';
import Avatar from '@material-ui/core/Avatar';
import {Driver, FieldProps} from '../../types';

interface Props extends FieldProps<Driver> {
    className?: string;
    size?: string;
}

const DriverAvatarField: FC<Props> = ({record, size = '25', className}) => record ? (
    <Avatar /*src={`${record.avatar}?size=${size}x${size}`}*/
        style={{width: parseInt(size, 10), height: parseInt(size, 10)}}
        className={className}
    />
) : null;

export default DriverAvatarField;
