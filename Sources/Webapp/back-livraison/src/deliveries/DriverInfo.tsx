import {FC, useEffect, useState} from "react";
import {DeliveryItem, FieldProps, VehicleDeliveryInfo, VehicleHistoItem} from "../types";
import * as React from "react";
import {CardContent, Divider} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import DrivierNameField from "./fields/DrivierNameField";
import {apiUrl} from "../dataProvider/ConstUrl";

const useStyles = makeStyles(theme => ({
    avatar: {
        background: theme.palette.background.paper,
    },
    listItemText: {
        overflowY: 'hidden',
        height: '4em',
        display: '-webkit-box',
        WebkitLineClamp: 2,
        WebkitBoxOrient: 'vertical',
    },
    root: {
        marginBottom: 16
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    details: {
        fontSize: 11,
        fontWeight: "bold"
    },
    card_content: {
        paddingBottom: 0
    },
    spacer: {
        marginTop: 8,
        marginBottom: 8
    },
    pos: {
        marginBottom: 12,
    },
}));

const DriverInfo: FC<FieldProps<VehicleDeliveryInfo>> = ({record}) => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    const driver = record?.delivery.vehicle.driver;
    const vehicle = record?.delivery.vehicle;
    const [histoItem, setHistoItem] = useState<VehicleHistoItem>();

    function fetchDeliveries() {
        fetch(`${apiUrl}/vehicleHistos/search/findByVehicleIdAndDeliveryId?vehicleId=${record?.delivery?.vehicle?.id}&deliveryId=${record?.delivery?.id}`)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    setHistoItem(data);
                }
            });
    }

    useEffect(() => {
        fetchDeliveries();
    }, [record]);


    return driver ? (
        <Card key="driver_info" className={classes.root} variant="outlined">
            <CardContent className={classes.card_content}>
                <DrivierNameField record={driver}/>
                <br/>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Conducteur {bull} {driver.license} {bull} {driver.sexe == "M" ? "Homme" : "Femme"} {bull} {driver?.age} ans
                </Typography>
                <Typography className={classes.title} color="secondary" gutterBottom>
                    Tel: {driver.numbers?.orange} {bull} {driver.numbers?.telma}
                </Typography>
                <Divider className={classes.spacer}/>
                <Typography variant="body2" component="span">
                    {vehicle?.brand} - {vehicle?.numImm}
                </Typography>
                <Typography variant="h6" color="secondary" component="p">
                    <span>Retour environs dans</span> {histoItem?.estimDuration.duraText}
                </Typography>
                <Typography variant="body2" component="p">
                    Parcours {histoItem?.estimDuration.distText}
                </Typography>
            </CardContent>
        </Card>
    ) : null;
};

export default DriverInfo;