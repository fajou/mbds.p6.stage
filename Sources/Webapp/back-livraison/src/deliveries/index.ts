import ReviewIcon from '@material-ui/icons/Comment';
import DeliveryList from './DeliveryList';

export default {
    icon: ReviewIcon,
    list: DeliveryList,
};
