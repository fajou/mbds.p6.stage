import {ReactChildren} from 'react';
import {Identifier, Record, RedirectionSideEffect, ReduxState, useListController, usePermissions,} from 'ra-core';
import {RouteComponentProps} from 'react-router-dom';
import {StaticContext} from 'react-router';
import * as H from 'history';
import {ClassNameMap} from '@material-ui/core/styles/withStyles';
import {ListControllerProps} from 'ra-core/esm/controller/useListController';
import {FormRenderProps} from 'react-final-form';

export type ThemeName = 'light' | 'dark';

export interface AppState extends ReduxState {
    theme: ThemeName;
}

/*
*
* New types
* */
export interface BaseType {
    id: number,
    version: number,
    updatedDate: Date,
    createdDate: Date
}

export interface PointRelay extends BaseType {
    rayon: number;
    address: string;
    currPosition: string;
    unit: string,
}

export interface TimeInterval extends BaseType {
    timeMin: Date;
    timeMoy: Date;
    timeMax: Date;
}

export interface TypeVehicule extends BaseType {
    name: string;
    icon: string;
}

export interface Categorie extends BaseType {
    name: string;
    icon: string;
    price: string;
}

export interface Distance extends BaseType {
    distValue: number;
    distText: string;
    duraValue: number;
    duraText: string;
}

export interface Customer1 extends BaseType {
    firstName: string;
    lastName: string;
    currPosition: string;
    address: string;
    email: string;
    city: string;
    zipcode: string;
    avatar: string;
    mobileNumbers: MobileNumbers;
    distance: Distance;
}

export interface Driver extends BaseType {
    age: number;
    name: string;
    license: string;
    occuped: boolean;
    sexe: string;
    avatar: string;
    type: string;
    numbers: MobileNumbers,
}

export interface MobileNumbers extends BaseType {
    telma: string;
    orange: string;
    airtel: string;
    others: string;
}

export interface Carburant extends BaseType {
    carburantMax: number;
    carburantCurr: number;
    carburantType: string;
}

export type VehicleStatus = 'CREATED' | 'UPDATED' | "MAINTENANCE" | "DELIVERING" | "RESERVED" | "FREE";

export interface Vehicle extends BaseType {
    brand: any;
    status: VehicleStatus;
    weightMin: number;
    weightMax: number;
    speedMin: number;
    speedMoy: number;
    speedMax: number;
    trajetMin: number | undefined;
    trajetMax: number | undefined;
    numImm: number;
    colorVeh: string;
    descVeh: string;
    driver: Driver;
    carburant: Carburant;
    type: TypeVehicule;
    timeIntr: TimeInterval;
}

export type DeliveryStatus = 'CREATED' | 'UPDATED' | 'ADDED' | 'PREPARED' | 'PENDING' | 'DELIVERING' | 'DELIVERED';

export interface Delivery extends BaseType {
    departDate: Date;
    arrivalDate: Date;
    status: DeliveryStatus,
    duration: any;
    totalWeight: number;
    totalItems: number;
    subTotal: number;
    totalTax: number;
    grandTotal: number;
    vehicle: Vehicle;
    orders: Command[];
}

export interface DeliveryItem {
    id: string,
    delivery: Delivery
}

export interface GeoDelivery {
    vehicle: Vehicle,
    relay: PointRelay,
    customers: Customer1[]
}

export interface Command extends BaseType {
    code: string;
    status: string;
    totalItems: number;
    totalWeight: number;
    dateCommand: string,
    dateDelivery: string,
    subTotal: number;
    totalTax: number;
    totalDelivery: number;
    grandTotal: number;
    customer: Customer1;
    items: Item[];
    deliveries: Delivery;
    payementInfo: PayementInfo;
}

export interface PaimentMethod extends BaseType {
    libelle: string;
    code: string;
    type: string;
    purlExt: string;
}

export interface PayementInfo extends BaseType {
    total: number;
    advance: number;
    paimentDate: Date;
    method: PaimentMethod;
    container: Command;
}

export interface Item extends BaseType {
    articleRef: string;
    itemCount: number;
    unitePrice: number;
    priceTotal: number;
    uniteWeight: number;
    totalWeight: number;
    article: Article;
    order: Command | {};
}

export interface MediaCloud extends BaseType {
    filepath: string;
    resource_type: string;
    original_filename: string;
    signature: string;
    format: string;
    secure_url: string;
    url: string;
    public_id: string;
    placeholder: string;
    height: number;
    width: number;
}

export interface Article extends BaseType {
    name: string;
    price: number;
    promo: number;
    prating: number;
    outstock: string;
    reduction: string;
    classifier: string;
    image: MediaCloud;
    categorie: Categorie;
}

/*
*
* */

export interface Category extends Record {
    name: string;
    icon: string;
    price: string;
    media: MediaCloud
}

export interface Product extends Record {
    category_id: Identifier;
    description: string;
    height: number;
    image: string;
    price: number;
    reference: string;
    stock: number;
    thumbnail: string;
    width: number;
}

export interface Customer extends Record {
    first_name: string;
    last_name: string;
    address: string;
    city: string;
    zipcode: string;
    avatar: string;
    birthday: string;
    first_seen: string;
    last_seen: string;
    has_ordered: boolean;
    latest_purchase: string;
    has_newsletter: boolean;
    groups: string[];
    nb_commands: number;
    total_spent: number;
}

export type OrderStatus = 'ADDED' | 'DELIVERING' | 'PENDING' | 'DELIVERED' | 'CANCELLED' | 'GROUPED';

export interface Order extends BaseType {
    status: OrderStatus;
    code: string;
    returned: boolean;
    dateDelivery: Date;
    totalItems: number;
    totalWeight: number;
    subTotal: number;
    totalTax: number;
    totalDelivery: number;
    grandTotal: number;
    customer: Customer1;
    items: Item[];
    deliveries: Delivery;
    payementInfo: PayementInfo;
}

export interface VehicleHistoItem {
    vehicleId: number;
    deliveryId: number;
    back: boolean;
    estimDuration: Distance;
    estimDepartDate: string;
    localisation: any;
    estimArrivalDate: string;
    departDate: string;
    arrivalDate: string;
}

export interface VehicleDeliveryInfo {
    id: string,
    histoItem: VehicleHistoItem,
    delivery: Delivery,
    vehicle: Vehicle
}

/**
 * Types to eventually add in react-admin
 */
export interface FieldProps<T extends Record = Record> {
    addLabel?: boolean;
    label?: string;
    record?: T;
    source?: string;
    resource?: string;
    basePath?: string;
    formClassName?: string;
}

export interface ReferenceFieldProps<T extends Record = Record>
    extends FieldProps<T> {
    reference: string;
    children: ReactChildren;
    link?: string | false;
    sortBy?: string;
}

export type ReviewStatus = 'accepted' | 'pending' | 'rejected';

export interface Review extends Record {
    date: Date;
    status: ReviewStatus;
    customer_id: Identifier;
    product_id: Identifier;
}

export interface ResourceMatch {
    id: string;

    [k: string]: string;
}

type FilterClassKey = 'button' | 'form';

export interface ToolbarProps<T extends Record = Record> {
    handleSubmitWithRedirect?: (redirect: RedirectionSideEffect) => void;
    handleSubmit?: FormRenderProps['handleSubmit'];
    invalid?: boolean;
    pristine?: boolean;
    saving?: boolean;
    submitOnEnter?: boolean;
    redirect?: RedirectionSideEffect;
    basePath?: string;
    record?: T;
    resource?: string;
    undoable?: boolean;
}

export interface BulkActionProps<Params = {}> {
    basePath?: string;
    filterValues?: Params;
    resource?: string;
    selectedIds?: Identifier[];
}

export interface FilterProps<Params = {}> {
    classes?: ClassNameMap<FilterClassKey>;
    context?: 'form' | 'button';
    displayedFilters?: { [K in keyof Params]?: boolean };
    filterValues?: Params;
    hideFilter?: ReturnType<typeof useListController>['hideFilter'];
    setFilters?: ReturnType<typeof useListController>['setFilters'];
    showFilter?: ReturnType<typeof useListController>['showFilter'];
    resource?: string;
}

export interface DatagridProps<RecordType = Record>
    extends Partial<ListControllerProps<RecordType>> {
    hasBulkActions?: boolean;
}

export interface ResourceComponentProps<Params extends { [K in keyof Params]?: string } = {},
    C extends StaticContext = StaticContext,
    S = H.LocationState> extends RouteComponentProps<Params, C, S> {
    resource: string;
    options: object;
    hasList: boolean;
    hasEdit: boolean;
    hasShow: boolean;
    hasCreate: boolean;
    permissions: ReturnType<typeof usePermissions>['permissions'];
}

export interface ListComponentProps<Params = {}>
    extends ResourceComponentProps<Params> {
}

export interface EditComponentProps<Params extends ResourceMatch = { id: string },
    C extends StaticContext = StaticContext,
    S = H.LocationState> extends ResourceComponentProps<Params, C, S> {
    id: string;
}

export interface ShowComponentProps<Params extends ResourceMatch = { id: string },
    C extends StaticContext = StaticContext,
    S = H.LocationState> extends ResourceComponentProps<Params, C, S> {
    id: string;
}

export interface CreateComponentProps<Params extends ResourceMatch = { id: string },
    C extends StaticContext = StaticContext,
    S = H.LocationState> extends ResourceComponentProps<Params, C, S> {
    id: string;
}

declare global {
    interface Window {
        restServer: any;
    }
}
