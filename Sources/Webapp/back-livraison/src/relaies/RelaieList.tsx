import * as React from 'react';
import {Datagrid, List, TextField, NumberField} from 'react-admin';

import RelaieEdit from './RelaieEdit';

const RelaieList = (props: any) => (
    <List {...props} perPage={25}>
        <Datagrid rowClick="expand" expand={<RelaieEdit/>}>
            <TextField source="id" label="Identifiant"/>
            <TextField source="distance.distText" label="Bornes"/>
            <TextField source="address" label="Place"/>
            <NumberField source="rayon" label="Rayon"/>
            <TextField source="unit" label="Unite (Km ou m)"/>
            <TextField source="currPosition" label="Localisation"/>
        </Datagrid>
    </List>
);

export default RelaieList;
