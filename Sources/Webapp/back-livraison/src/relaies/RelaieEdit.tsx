import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {SimpleForm, Edit, TextInput, useShowController, NumberInput} from 'react-admin';

const RelaieEdit = (props: any) => {
    const {record} = useShowController(props);
    const classes = useStyles();

    if (!record) return null;
    return (
        <Edit className={classes.root} {...props} title=" ">
            <SimpleForm>
                <TextInput disabled label="Identifiant" source="id" fullWidth/>
                <TextInput source="address" label="Place" fullWidth/>
                <TextInput source="currPosition" label="Localisation" fullWidth/>
                <NumberInput source="rayon" label="Rayon" fullWidth/>
                <TextInput source="unit" label="Unite (Km ou m)" fullWidth/>
            </SimpleForm>
        </Edit>
    );
};

export default RelaieEdit;

const useStyles = makeStyles({
    root: {width: 600, margin: 'auto'},
    spacer: {height: 20},
    invoices: {margin: '10px 0'},
});
