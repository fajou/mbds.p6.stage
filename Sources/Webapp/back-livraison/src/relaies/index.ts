import FlagIcon from '@material-ui/icons/Flag';

import RelaieList from './RelaieList';

export default {
    list: RelaieList,
    icon: FlagIcon,
};
