import * as React from 'react';
import {FC} from 'react';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import {useTranslate} from 'react-admin';
import CardWithIcon from "./CardWithIcon";

interface Props {
    value?: number;
}

const TotalArticle: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={InsertDriveFileIcon}
            title={translate('pos.dashboard.week_articles')}
            subtitle={value}
        />
    );
};

export default TotalArticle;
