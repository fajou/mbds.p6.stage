import * as React from 'react';
import {FC} from 'react';
import EmojiTransportationIcon from '@material-ui/icons/EmojiTransportation';
import {useTranslate} from 'react-admin';

import CardWithIcon from './CardWithIcon';

interface Props {
    value?: number;
}

const TotalLivraison: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={EmojiTransportationIcon}
            title={translate('pos.dashboard.week_livraison')}
            subtitle={value}
        />
    );
};

export default TotalLivraison;
