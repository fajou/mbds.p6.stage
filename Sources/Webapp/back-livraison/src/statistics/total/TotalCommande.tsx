import * as React from 'react';
import {FC} from 'react';
import {useTranslate} from 'react-admin';
import ViewListIcon from '@material-ui/icons/ViewList';
import CardWithIcon from './CardWithIcon';

interface Props {
    value?: number;
}

const TotalCommande: FC<Props> = ({value}) => {
    const translate = useTranslate();
    return (
        <CardWithIcon
            to="/commands"
            icon={ViewListIcon}
            title={translate('pos.dashboard.week_commande')}
            subtitle={value}
        />
    );
};

export default TotalCommande;
