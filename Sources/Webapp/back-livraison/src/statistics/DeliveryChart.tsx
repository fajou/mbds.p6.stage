import * as React from 'react';
import {FC, useEffect} from 'react';
import {Card, CardContent, CardHeader} from '@material-ui/core';
import {initChart} from './chart.js'

import {Order} from '../types';


const DeliveryChart: FC<{ orders?: Order[] }> = ({orders}) => {

    /*if (!orders) return null;*/
    function init() {
        initChart({});
    }

    useEffect(init, [])

    return (
        <Card>
            <CardContent>
                <div className="chart-container">
                    <canvas id="deliveriesChart" height={80} />
                </div>
            </CardContent>
        </Card>
    );
};

export default DeliveryChart;
