import BarChartIcon from '@material-ui/icons/BarChart';

import State from './Statistics';

export default {
    list: State,
    icon: BarChartIcon,
};