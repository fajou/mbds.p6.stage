import Chart from 'chart.js';

export function initChart(data) {
    const ctx = document.getElementById('deliveriesChart').getContext('2d');
    const livraison = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
            datasets: [{
                label: 'Liviraisons',
                backgroundColor: 'rgb(255, 205, 86)',
                borderColor: 'rgb(146,255,99)',
                borderWidth: 1,
                data: [12, 8, 13, 11, 5, 18, 16]
            }, {
                label: 'Commandes',
                backgroundColor: 'rgb(75, 192, 192)',
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 1,
                data: [140, 90, 150, 130, 128, 111, 130]
            }, {
                label: 'Articles',
                backgroundColor: 'rgb(153, 102, 255)',
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 1,
                data: [200, 180, 160, 160, 228, 201, 230]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}