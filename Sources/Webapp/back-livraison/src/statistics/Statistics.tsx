import React, {FC,} from 'react';
import NbNewOrders from '../dashboard/NbNewOrders';
import "./State.css";
import DeliveryChart from "./DeliveryChart";
import {Button, Card, CardContent, CardHeader, Grid, MuiThemeProvider} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslate} from "react-admin";
import TotalArticle from "./total/TotalArticle";
import TotalCommande from "./total/TotalCommande";
import TotalLivraison from "./total/TotalLivraison";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";
import WeekPicker from './weekpicker';
import MomentUtils from "@date-io/moment";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {ArrowDownward, ArrowDropDown, CloudDownload, GetApp} from "@material-ui/icons";

const useStyles = makeStyles({
    card: {
        maxWidth: '100%',
        marginTop: 20,
        minHeight: 52,
        '& a': {
            textDecoration: 'none',
            color: 'inherit',
        },
    },
});

const Statistics: FC = () => {
    const classes = useStyles();
    const translate = useTranslate();
    const theme = createMuiTheme();

    return (
        <div>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <TotalArticle value={1359}/>
                </Grid>
                <Grid item xs={3}>
                    <TotalCommande value={876}/>
                </Grid>
                <Grid item xs={3}>
                    <TotalLivraison value={73}/>
                </Grid>
                <Grid item xs={3}>
                    <NbNewOrders value={52}/>
                </Grid>
            </Grid>

            <Card className={classes.card}>
                <CardHeader title={translate('pos.dashboard.week_history')}/>
                <CardContent>
                    <Grid container>
                        <Grid item xs={11}>
                            <div className="mb-15">
                                <MuiThemeProvider theme={theme}>
                                    <MuiPickersUtilsProvider utils={MomentUtils} locale="fr">
                                        <WeekPicker/>
                                    </MuiPickersUtilsProvider>
                                </MuiThemeProvider>
                            </div>
                        </Grid>
                        <Grid item xs={1}>
                            <Button startIcon={<GetApp/>}
                                    variant="text" size="small" color="primary">
                                Exporter
                            </Button>
                        </Grid>
                    </Grid>
                    <table className="table-state">
                        <thead>
                        <tr>
                            <th>Libellé/Jours</th>
                            <th>Dimanche</th>
                            <th>Lundi</th>
                            <th>Mardi</th>
                            <th>Mercredi</th>
                            <th>Jeudi</th>
                            <th>Vendredi</th>
                            <th>Samedi</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>Livraisons</th>
                            <td>12</td>
                            <td>8</td>
                            <td>13</td>
                            <td>11</td>
                            <td>5</td>
                            <td>18</td>
                            <td>16</td>
                            <th>73</th>
                        </tr>
                        <tr>
                            <th>Commandes</th>
                            <td>140</td>
                            <td>90</td>
                            <td>150</td>
                            <td>130</td>
                            <td>128</td>
                            <td>111</td>
                            <td>130</td>
                            <th>876</th>
                        </tr>
                        <tr>
                            <th>Articles</th>
                            <td>200</td>
                            <td>180</td>
                            <td>160</td>
                            <td>160</td>
                            <td>228</td>
                            <td>201</td>
                            <td>230</td>
                            <th>1 359</th>
                        </tr>
                        </tbody>
                    </table>
                    <DeliveryChart/>
                </CardContent>
            </Card>
        </div>
    );
};

export default Statistics;
