import ListAltIcon from '@material-ui/icons/ListAlt';

import OrderList from './OrderList';
import OrderEdit from './OrderEdit';

export default {
    list: OrderList,
    edit: OrderEdit,
    icon: ListAltIcon,
};
