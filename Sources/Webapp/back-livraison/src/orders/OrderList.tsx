import * as React from 'react';
import {FC, Fragment, useCallback, useEffect, useState} from 'react';
import {
    AutocompleteInput,
    Datagrid,
    DateInput,
    Filter,
    List,
    NullableBooleanInput,
    NumberField,
    ReferenceInput,
    SearchInput,
    TextField,
    TextInput,
} from 'react-admin';
import {Divider, Tab, Tabs, Theme, useMediaQuery} from '@material-ui/core';

import {makeStyles} from '@material-ui/core/styles';

import NbItemsField from './NbItemsField';
import {Customer1, DatagridProps, FilterProps, ListComponentProps, Order, OrderStatus,} from '../types';
import {Identifier} from 'ra-core';
import CustomerFullNameField from "../customers/FullNameField";

interface FilterParams {
    q?: string;
    customer_id?: string;
    date_gte?: string;
    date_lte?: string;
    total_gte?: string;
    returned?: boolean;
    status?: OrderStatus;
}

const OrderFilter: FC<FilterProps<FilterParams>> = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn/>
        <ReferenceInput source="customer_id" reference="customers">
            <AutocompleteInput
                optionText={(choice: Customer1) => choice.firstName && choice.lastName ? `${choice.firstName} ${choice.lastName}` : ''}/>
        </ReferenceInput>
        <DateInput source="createdDate"/>
        <DateInput source="dateDelivery"/>
        <TextInput source="code"/>
        <NullableBooleanInput source="returned"/>
    </Filter>
);

const useDatagridStyles = makeStyles({
    total: {fontWeight: 'bold'},
});
const tabs = [
    {id: 'ADDED', name: 'NOUVELLES'},
    {id: 'GROUPED', name: 'GROUPÉES'},
    {id: 'PREPARED', name: 'PRÉPARÉES'},
    {id: 'DELIVERED', name: 'LIVRÉES'},
    {id: 'CANCELLED', name: 'ANNULÉES'},
];

interface TabbedDatagridProps extends DatagridProps<Order> {
}

const TabbedDatagrid: FC<TabbedDatagridProps> = ({
                                                     ids,
                                                     filterValues,
                                                     setFilters,
                                                     displayedFilters,
                                                     ...rest
                                                 }) => {
    const classes = useDatagridStyles();
    const isXSmall = useMediaQuery<Theme>(theme =>
        theme.breakpoints.down('xs')
    );
    const [ADDED, setAdded] = useState<Identifier[]>([]);
    const [GROUPED, setGrouped] = useState<Identifier[]>([]);
    const [PREPARED, setPrepared] = useState<Identifier[]>([]);
    const [DELIVERED, setDelivered] = useState<Identifier[]>([]);
    const [CANCELLED, setCancelled] = useState<Identifier[]>([]);

    useEffect(() => {
        if (ids && ids !== filterValues.status) {
            switch (filterValues.status) {
                case 'ADDED':
                    setAdded(ids);
                    break;
                case 'GROUPED':
                    setGrouped(ids);
                    break;
                case 'PREPARED':
                    setPrepared(ids);
                    break;
                case 'DELIVERED':
                    setDelivered(ids);
                    break;
                case 'CANCELLED':
                    setCancelled(ids);
                    break;
            }
        }
    }, [ids, filterValues.status]);

    const handleChange = useCallback((event: React.ChangeEvent<{}>, value: any) => {
            setFilters && setFilters({...filterValues, status: value}, displayedFilters);
        },
        [displayedFilters, filterValues, setFilters]
    );

    const tabContent = (status: Identifier[]) => {
        return (
            <Datagrid {...rest} ids={status} optimized rowClick="edit">
                {status === ADDED ? (
                    <TextField label='resources.deliveries.date_cmd' source="dateCommand"/>
                ) : (
                    <TextField label='resources.deliveries.date' source="dateDelivery"/>
                )}
                <TextField source="code"/>
                <CustomerFullNameField label='resources.customers.column' source="customer.lastName"/>
                <TextField source="customer.distance.distText" label="A Livrer"/>
                <NbItemsField source="totalItems"/>
                <NumberField source="totalWeight" label="Poids (Kg)" className={classes.total}/>
                <NumberField source="grandTotal" options={{
                    style: 'currency',
                    currency: 'MGA',
                }} className={classes.total}/>
            </Datagrid>
        );
    }

    return (
        <Fragment>
            <Tabs variant="fullWidth"
                  centered
                  value={filterValues.status}
                  indicatorColor="primary"
                  onChange={handleChange}>
                {tabs.map(choice => (
                    <Tab key={choice.id}
                         label={choice.name}
                         value={choice.id}
                    />
                ))}
            </Tabs>
            <Divider/>
            <div>
                {filterValues.status === "ADDED" && (tabContent(ADDED))}
                {filterValues.status === "GROUPED" && (tabContent(GROUPED))}
                {filterValues.status === 'PREPARED' && (tabContent(PREPARED))}
                {filterValues.status === 'DELIVERED' && (tabContent(DELIVERED))}
                {filterValues.status === 'CANCELLED' && (tabContent(CANCELLED))}
            </div>
        </Fragment>
    );
};

const OrderList: FC<ListComponentProps> = props => (
    <List {...props}
          title="Commands"
        /*basePath="commands/getById"*/
          filterDefaultValues={{status: 'GROUPED'}}
          sort={{field: 'createdDate', order: 'DESC'}}
          perPage={15}
          filters={<OrderFilter/>}>
        <TabbedDatagrid/>
    </List>
);

export default OrderList;
