import * as React from 'react';
import {FunctionField} from 'react-admin';
import {FieldProps, Order} from '../types';

function render(record: Order) {
    return record.totalItems;
}

interface NbItemsFieldProps extends FieldProps {
    textAlign?: string;
}

function NbItemsField(props: any) {
    return <FunctionField {...props} render={render}/>;
}

NbItemsField.defaultProps = {
    label: 'Nb. Article(s)',
    textAlign: 'right',
};

export default NbItemsField;
