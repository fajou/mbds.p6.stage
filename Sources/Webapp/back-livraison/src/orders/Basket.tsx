import * as React from 'react';
import {FC} from 'react';
import classnames from 'classnames';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {Link, useTranslate} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';
import {FieldProps, Item, Order} from '../types';

const useStyles = makeStyles({
    container: {minWidth: '35em', marginLeft: '1em'},
    rightAlignedCell: {textAlign: 'right'},
    boldCell: {fontWeight: 'bold'},
});

const Basket: FC<FieldProps<Order>> = ({record}) => {
    const classes = useStyles();
    const translate = useTranslate();

    if (!record) return null;
    return (
        <Paper className={classes.container} elevation={2}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            {translate('resources.commands.fields.basket.reference')}
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {translate('resources.commands.fields.basket.unit_price')}
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {translate('resources.commands.fields.basket.quantity')}
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {translate('resources.commands.fields.basket.total')}
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {record.items.map((item: Item) => {
                            item.order = {id: record.id};
                            return (
                                <TableRow key={item.id}>
                                    <TableCell>
                                        <Link to={`/articles/${item.id}`}>
                                            {item.articleRef}
                                        </Link>
                                    </TableCell>
                                    <TableCell className={classes.rightAlignedCell}>
                                        {item.unitePrice.toLocaleString(undefined, {
                                            style: 'currency',
                                            currency: 'MGA',
                                        })}
                                    </TableCell>
                                    <TableCell className={classes.rightAlignedCell}>
                                        {item.itemCount}
                                    </TableCell>
                                    <TableCell className={classes.rightAlignedCell}>
                                        {item.priceTotal.toLocaleString(undefined, {
                                            style: 'currency',
                                            currency: 'MGA',
                                        })}
                                    </TableCell>
                                </TableRow>);
                        }
                    )}
                    <TableRow>
                        <TableCell colSpan={2}/>
                        <TableCell>
                            {translate('resources.commands.fields.basket.sum')}
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {record.subTotal.toLocaleString(undefined, {
                                style: 'currency',
                                currency: 'MGA',
                            })}
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell colSpan={2}/>
                        <TableCell>
                            {translate(
                                'resources.commands.fields.basket.delivery'
                            )}
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {record.totalDelivery.toLocaleString(undefined, {
                                style: 'currency',
                                currency: 'MGA',
                            })}
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell colSpan={2}/>
                        <TableCell>
                            {translate('resources.commands.fields.basket.tax_rate')}
                            ({(.2).toLocaleString(undefined, {style: 'percent',})})
                        </TableCell>
                        <TableCell className={classes.rightAlignedCell}>
                            {record.totalTax.toLocaleString(undefined, {
                                style: 'currency',
                                currency: 'MGA',
                            })}
                        </TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell colSpan={2}/>
                        <TableCell className={classes.boldCell}>
                            {translate('resources.commands.fields.basket.total')}
                        </TableCell>
                        <TableCell className={classnames(classes.boldCell, classes.rightAlignedCell)}>
                            {record.grandTotal.toLocaleString(undefined, {
                                style: 'currency',
                                currency: 'MGA',
                            })}
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </Paper>
    );
};

export default Basket;
