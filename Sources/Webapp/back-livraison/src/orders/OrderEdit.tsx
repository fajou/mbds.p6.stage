import * as React from 'react';
import {FC} from 'react';
import {Edit, SelectInput, SimpleForm, TextInput, useTranslate,} from 'react-admin';
import {makeStyles} from '@material-ui/core/styles';
import {EditComponentProps, Order} from '../types';

import Basket from './Basket';
import FullNameField from "../customers/FullNameField";

interface OrderTitleProps {
    record?: Order;
}

const OrderTitle: FC<OrderTitleProps> = ({record}) => {
    const translate = useTranslate();
    return record ? (
        <span>{translate('resources.commands.title', {reference: record.id,})}</span>
    ) : null;
};

const useEditStyles = makeStyles({
    root: {alignItems: 'flex-start'},
    main: {
        marginBottom: '2em',
        marginTop: '1em',
    },
    noActions: {
        color: '#000',
        fontWeight: 600
    }
});

const OrderEdit: FC<EditComponentProps> = props => {
    const classes = useEditStyles();

    return (
        <Edit title={<OrderTitle/>}
              aside={<Basket/>}
              classes={classes}
              {...props}>
            <SimpleForm>
                <FullNameField formClassName={classes.main}/>
                <TextInput variant="outlined" disabled={true}
                           className={classes.noActions}
                           label="resources.commands.date_delivery"
                           source="dateDelivery" fullWidth/>
                <SelectInput
                    fullWidth
                    disabled={true}
                    source="status"
                    choices={[
                        {id: 'GROUPED', name: 'GROUPÉES'},
                        {id: 'PREPARED', name: 'PRÉPARÉES'},
                        {id: 'DELIVERED', name: 'LIVRÉES'},
                        {id: 'CANCELLED', name: 'ANNULÉES'},
                        {
                            id: 'DELIVERING',
                            name: 'LIVRAISON ENCOURS',
                            disabled: true,
                        },
                    ]}
                />
            </SimpleForm>
        </Edit>
    );
};

export default OrderEdit;
