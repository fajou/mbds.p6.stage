import * as React from 'react';
import {Datagrid, DateField, DateInput, Filter, List, NumberField, ReferenceField, TextField,} from 'react-admin';

import FullNameField from '../customers/FullNameField';
import AddressField from '../customers/AddressField';
import InvoiceShow from './InvoiceShow';

const ListFilters = (props: any) => (
    <Filter {...props}>
        <DateInput source="date_gte" alwaysOn/>
        <DateInput source="date_lte" alwaysOn/>
    </Filter>
);

const InvoiceList = (props: any) => (
    <List {...props} filters={<ListFilters/>} perPage={25}>
        <Datagrid rowClick="expand" expand={<InvoiceShow/>}>
            <TextField source="id"/>
            <DateField source="date"/>
            <ReferenceField source="customer_id" reference="customers">
                <FullNameField/>
            </ReferenceField>
            <ReferenceField
                source="customer_id"
                reference="customers"
                link={false}
                label="resources.invoices.fields.address"
            >
                <AddressField/>
            </ReferenceField>
            <ReferenceField source="command_id" reference="commands">
                <TextField source="reference"/>
            </ReferenceField>
            <NumberField source="total_ex_taxes"/>
            <NumberField source="delivery_fees"/>
            <NumberField source="taxes"/>
            <NumberField source="total"/>
        </Datagrid>
    </List>
);

export default InvoiceList;
