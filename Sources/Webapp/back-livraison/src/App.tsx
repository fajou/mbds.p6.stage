import * as React from 'react';
import {useEffect, useState} from 'react';
import {Admin, Resource} from 'react-admin';
import polyglotI18nProvider from 'ra-i18n-polyglot';

import './App.css';

import authProvider from './authProvider';
import themeReducer from './themeReducer';
import {Layout, Login} from './layout';
import {Dashboard} from './dashboard';
import customRoutes from './routes';
import defaultI18nProvider from './i18n/fr';

import vehicles from './vehicles';
import customers from './customers';
import orders from './orders';
import articles from './products';
import invoices from './invoices';
import categories from './categories';
import deliveries from './deliveries';
import statistics from './statistics';
import nextshipping from './nextshipping';

import relaies from './relaies';
import {createMuiTheme} from '@material-ui/core/styles';

import dataProviderFactory from './dataProvider';

const i18nProvider = polyglotI18nProvider(locale => {
    if (locale === 'fr') {
        return import('./i18n/fr').then(messages => messages.default);
    }
    return defaultI18nProvider;
}, 'en');

const App = () => {
    const [dataProvider, setDataProvider] = useState(null);

    useEffect(() => {

        const fetchDataProvider = async () => {
            const dataProviderInstance = await dataProviderFactory(process.env.REACT_APP_DATA_PROVIDER || '');
            setDataProvider(() => dataProviderInstance);
        };

        fetchDataProvider().then(() => {
        });
    }, []);

    if (!dataProvider) {
        return (
            <div className="loader-container">
                <div className="loader">Loading...</div>
            </div>
        );
    }

    const theme = createMuiTheme({
        palette: {
            type: 'dark', // Switching the dark mode on is a single property value change.
        },
    });

    return (
        <Admin theme={theme} title="Livraison"
               dataProvider={dataProvider}
               customReducers={{theme: themeReducer}}
               customRoutes={customRoutes}
               authProvider={authProvider}
               dashboard={Dashboard}
               loginPage={Login}
               layout={Layout}
               i18nProvider={i18nProvider}>
            <Resource name="customers" {...customers} />
            <Resource name="vehicles" {...vehicles} />
            <Resource name="commands" {...orders} options={{label: 'Orders'}}/>
            <Resource name="invoices" {...invoices} />
            <Resource name="articles" {...articles} />
            <Resource name="categories" {...categories} />
            <Resource name="deliveryItems" {...deliveries} />
            <Resource name="relaies" {...relaies} />
            <Resource name="statistique" {...statistics} />
            <Resource name="nextshipping" {...nextshipping} />
        </Admin>
    );
};

export default App;
