import * as React from 'react';
import {FC} from 'react';
import Button from '@material-ui/core/Button';
import {makeStyles} from '@material-ui/core/styles';
import {Link} from 'react-router-dom';
import {useTranslate} from 'react-admin';
import {stringify} from 'query-string';
import {Category, FieldProps} from '../types';

const useStyles = makeStyles({
    icon: {paddingRight: '0.5em'},
    link: {
        display: 'inline-flex',
        alignItems: 'center',
        fontSize: 11,
        fontWeight: 500
    },
});

const LinkToRelatedProducts: FC<FieldProps<Category>> = ({record}) => {
    const translate = useTranslate();
    const classes = useStyles();
    return record ? (
        <Button
            size="small"
            variant="outlined"
            color="default"
            component={Link}
            to={{
                pathname: '/articles',
                search: stringify({
                    page: 1,
                    perPage: 20,
                    sort: 'reference',
                    order: 'ASC',
                    filter: JSON.stringify({categorie_id: record.id}),
                }),
            }}
            className={classes.link}>
            {translate('resources.categories.fields.products')}
        </Button>
    ) : null;
};

export default LinkToRelatedProducts;
