import * as React from 'react';
import {FC} from 'react';
import {
    Datagrid,
    Edit,
    EditButton,
    NumberField,
    ReferenceManyField,
    SimpleForm,
    TextField,
    TextInput,
    useTranslate,
} from 'react-admin';

import ThumbnailField from '../products/ThumbnailField';
import {Category, FieldProps} from '../types';
import ProductRefField from "../products/ProductRefField";

const CategoryTitle: FC<FieldProps<Category>> = ({record}) => {
    const translate = useTranslate();
    return record ? (
        <span>
            {translate('resources.categories.name', {smart_count: 1})} &quot;
            {record.name}&quot;
        </span>
    ) : null;
};

const CategoryEdit = (props: any) => (
    <Edit title={<CategoryTitle/>} {...props}>
        <SimpleForm>
            <TextInput source="name"/>
            <ReferenceManyField
                reference="articles"
                target="categorie_id"
                label="resources.categories.fields.products"
                perPage={10}
                fullWidth>
                <Datagrid>
                    <ThumbnailField label="Picture"/>
                    <TextField source="name"/>
                    <ProductRefField source="classifier"/>
                    <NumberField source="price" options={{style: 'currency', currency: 'MGA'}}/>
                    <NumberField source="weight" options={{minimumFractionDigits: 2}}/>
                    <NumberField source="outstock" label="Stock"/>
                    <EditButton label=""/>
                </Datagrid>
            </ReferenceManyField>
        </SimpleForm>
    </Edit>
);

export default CategoryEdit;
