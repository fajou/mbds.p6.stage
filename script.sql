create table carburant
(
    id             bigint not null
        constraint carburant_pkey
            primary key,
    created_date   timestamp,
    status         varchar(255),
    updated_date   timestamp,
    version        bigint,
    carburant_curr bigint not null,
    carburant_max  bigint not null,
    carburant_type varchar(255)
);

alter table carburant
    owner to db_livraison;

create table distance
(
    id           bigint not null
        constraint distance_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    dist_text    varchar(255),
    dist_value   bigint not null,
    dura_text    varchar(255),
    dura_value   bigint not null
);

alter table distance
    owner to db_livraison;

create table media_cloud
(
    id                bigint  not null
        constraint media_cloud_pkey
            primary key,
    created_date      timestamp,
    status            varchar(255),
    updated_date      timestamp,
    version           bigint,
    filepath          varchar(255),
    format            varchar(255),
    height            integer not null,
    original_filename varchar(255),
    placeholder       varchar(255),
    public_id         varchar(255),
    resource_type     varchar(255),
    secure_url        varchar(255),
    signature         varchar(255),
    url               varchar(255),
    width             integer not null
);

alter table media_cloud
    owner to db_livraison;

create table categorie
(
    id           bigint not null
        constraint categorie_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    icon         varchar(255),
    image        varchar(255),
    name         varchar(255),
    price        varchar(255),
    media_id     bigint
        constraint fkd0yfvhky0habwj2rpem8jy4ow
            references media_cloud
);

alter table categorie
    owner to db_livraison;

create table article
(
    id           bigint           not null
        constraint article_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    classifier   varchar(255),
    name         varchar(255),
    outstock     varchar(255),
    price        double precision not null,
    promo        double precision not null,
    rating       double precision not null,
    reduction    varchar(255),
    weight       double precision,
    categorie_id bigint
        constraint fkotglc2x758dnu4mnggdt7tav9
            references categorie,
    image_id     bigint
        constraint fk4jle0aue6pe97janqsvh6d4qd
            references media_cloud
);

alter table article
    owner to db_livraison;

create table mobile_numbers
(
    id           bigint not null
        constraint mobile_numbers_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    airtel       varchar(255),
    orange       varchar(255),
    others       varchar(255),
    telma        varchar(255)
);

alter table mobile_numbers
    owner to db_livraison;

create table customer
(
    id                bigint not null
        constraint customer_pkey
            primary key,
    created_date      timestamp,
    status            varchar(255),
    updated_date      timestamp,
    version           bigint,
    address           varchar(255),
    avatar            varchar(255),
    city              varchar(255),
    curr_position     varchar(255),
    email             varchar(255),
    first_name        varchar(255),
    last_name         varchar(255),
    zipcode           varchar(255),
    distance_id       bigint
        constraint fkhwq7cjk4jftrmtoqud6758cwm
            references distance,
    mobile_numbers_id bigint
        constraint fk3u7kearkjnhdie05bsqcvak1a
            references mobile_numbers
);

alter table customer
    owner to db_livraison;

create table driver
(
    id           bigint  not null
        constraint driver_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    age          integer not null,
    avatar       varchar(255),
    email        varchar(255),
    license      varchar(255),
    name         varchar(255),
    occuped      boolean not null,
    sexe         varchar(255),
    numbers_id   bigint
        constraint fkhiea3t6d3c848ah6tanvfqhsj
            references mobile_numbers
);

alter table driver
    owner to db_livraison;

create table payment_method
(
    id           bigint not null
        constraint payment_method_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    code         varchar(255),
    libelle      varchar(255),
    type         varchar(255),
    url_ext      varchar(255)
);

alter table payment_method
    owner to db_livraison;

create table payment_info
(
    id           bigint           not null
        constraint payment_info_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    advance      double precision not null,
    paiment_date timestamp,
    total        double precision not null,
    method_id    bigint
        constraint fkn9x8o28luqdgielddpfk0daid
            references payment_method
);

alter table payment_info
    owner to db_livraison;

create table point_relay
(
    id            bigint           not null
        constraint point_relay_pkey
            primary key,
    created_date  timestamp,
    status        varchar(255),
    updated_date  timestamp,
    version       bigint,
    address       varchar(255),
    curr_position varchar(255),
    rayon         double precision not null,
    unit          varchar(255),
    distance_id   bigint
        constraint fk12dd7wo0ew6pyirh7m4mspcnv
            references distance
);

alter table point_relay
    owner to db_livraison;

create table time_interval
(
    id           bigint           not null
        constraint time_interval_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    time_max     double precision not null,
    time_min     double precision not null,
    time_moy     double precision not null
);

alter table time_interval
    owner to db_livraison;

create table type_delivery
(
    id           bigint           not null
        constraint type_delivery_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    couts        double precision not null,
    delivery     varchar(255),
    duration     varchar(255)
);

alter table type_delivery
    owner to db_livraison;

create table type_vehicule
(
    id           bigint not null
        constraint type_vehicule_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    icon         varchar(255),
    name         varchar(255)
);

alter table type_vehicule
    owner to db_livraison;

create table vehicle
(
    id            bigint           not null
        constraint vehicle_pkey
            primary key,
    created_date  timestamp,
    status        varchar(255),
    updated_date  timestamp,
    version       bigint,
    brand         varchar(255),
    color_veh     varchar(255),
    desc_veh      varchar(255),
    last_delivery timestamp,
    num_imm       varchar(255),
    speed_max     integer          not null,
    speed_min     integer          not null,
    speed_moy     integer          not null,
    trajet_max    integer,
    trajet_min    integer,
    weight_max    double precision not null,
    weight_min    double precision not null,
    carburant_id  bigint
        constraint fk33bhn55b62ff2du2thh6stx1f
            references carburant,
    driver_id     bigint
        constraint fkeq0v35t6lcq8549u0q408af1b
            references driver,
    time_intr_id  bigint
        constraint fkac48so2r3yac3n5v7qjj9iglw
            references time_interval,
    type_id       bigint
        constraint fkp83ntfuxb7tx9n8u659sd0ak1
            references type_vehicule
);

alter table vehicle
    owner to db_livraison;

create table delivery
(
    id             bigint           not null
        constraint delivery_pkey
            primary key,
    created_date   timestamp,
    status         varchar(255),
    updated_date   timestamp,
    version        bigint,
    arrival_date   timestamp,
    depart_date    timestamp,
    duration       bigint,
    grand_total    double precision not null,
    sub_total      double precision not null,
    total_items    double precision not null,
    total_tax      double precision not null,
    total_weight   double precision not null,
    point_relay_id bigint
        constraint fklwdhvtn3bcxbodyeit7u5ofvp
            references point_relay,
    vehicle_id     bigint
        constraint fka1jqo7b478h2w2kpeo4mxd8wp
            references vehicle
);

alter table delivery
    owner to db_livraison;

create table command
(
    id              bigint           not null
        constraint command_pkey
            primary key,
    created_date    timestamp,
    status          varchar(255),
    updated_date    timestamp,
    version         bigint,
    code            varchar(255),
    date_command    timestamp,
    date_delivery   timestamp,
    grand_total     double precision not null,
    returned        boolean default false,
    sub_total       double precision not null,
    total_delivery  double precision not null,
    total_items     double precision not null,
    total_tax       double precision not null,
    total_weight    double precision not null,
    customer_id     bigint
        constraint fk3s95vxp6p1u9etf9huu70vcpo
            references customer,
    delivery_id     bigint
        constraint fknuhallx9a92b1ict000qntnvo
            references delivery,
    payment_info_id bigint
        constraint fk2xk81t16yp6pwk03n2pyfw33x
            references payment_info
);

alter table command
    owner to db_livraison;

create table item
(
    id           bigint           not null
        constraint item_pkey
            primary key,
    created_date timestamp,
    status       varchar(255),
    updated_date timestamp,
    version      bigint,
    item_count   bigint           not null,
    price_total  double precision not null,
    tax          double precision not null,
    total_weight double precision not null,
    unite_price  double precision not null,
    unite_weight double precision not null,
    article_id   bigint
        constraint fkssya4m722b8uliv5xsn69ngaq
            references article,
    order_id     bigint           not null
        constraint fkb55l5rk3t6unppl7xoiny9dmj
            references command,
    article_ref  varchar(255)
);

alter table item
    owner to db_livraison;


