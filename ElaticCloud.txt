elastic-deployment:dXMtY2VudHJhbDEuZ2NwLmNsb3VkLmVzLmlvJGFjZDE1ZTY5ZjY4MjRjZGZiNDc5Mjc3OWMxYmI4MzAxJGQxMmY3NThhMDQyNDRlMjhhZTIzNjVlYjg1ZDQyZGFk

docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.9.0
docker run --link epic_shamir:elasticsearch -p 5601:5601 -e discovery.type=single-node docker.elastic.co/kibana/kibana:7.9.0